##############################################################################
# Environment variables used by the Docker db service.                       #
#                                                                            #
# More information about the environment variables can be found here:        #
# https://hub.docker.com/_/postgres                                          #
##############################################################################
POSTGRES_DB=

POSTGRES_USER=

POSTGRES_PASSWORD=
