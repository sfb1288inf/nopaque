##############################################################################
# Environment variables used by Docker Compose config files.                 #
##############################################################################
# HINT: Use this bash command `id -u`
# NOTE: 0 (= root user) is not allowed
HOST_UID=

# HINT: Use this bash command `id -g`
# NOTE: 0 (= root group) is not allowed
HOST_GID=

# HINT: Use this bash command `getent group docker | cut -d: -f3`
HOST_DOCKER_GID=

# DEFAULT: nopaque
NOPAQUE_DOCKER_NETWORK_NAME=nopaque

# NOTE: This must be a network share and it must be available on all
#       Docker Swarm nodes, mounted to the same path.
HOST_NOPAQUE_DATA_PATH=/mnt/nopaque
