#!/bin/bash

display_help() {
    local script_name=$(basename "${0}")
    echo ""
    echo "Usage: ${script_name} [COMMAND]"
    echo ""
    echo "Run wrapper for a nopaque instance"
    echo ""
    echo "Commands:"
    echo "  flask    A general utility script for Flask applications."
    echo ""
    echo "Run '${script_name} COMMAND --help' for more information on a command."
}

if [[ "${#}" == "0" ]]; then
    if [[ "${NOPAQUE_IS_PRIMARY_INSTANCE:-True}" == "True" ]]; then
        while true; do
            flask deploy
            if [[ "${?}" == "0" ]]; then
                break
            fi
            echo "Deploy command failed, retrying in 5 secs..."
            sleep 5
        done
    fi
    python3 wsgi.py
elif [[ "${1}" == "flask" ]]; then
    flask ${@:2}
elif [[ "${1}" == "--help" || "${1}" == "-h" ]]; then
    display_help
else
    display_help
    exit 1
fi
