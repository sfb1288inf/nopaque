<h2>CQP Query Language</h2>
<p>Within the Corpus Query Language, a distinction is made between two types of annotations: positional attributes and structural attributes. Positional attributes refer to a token, e.g. the word "book" is assigned the part-of-speech tag "NN", the lemma "book" and the simplified part-of-speech tag "NOUN" within the token structure. Structural attributes refer to text structure-giving elements such as sentence and entity markup. For example, the markup of a sentence is represented in the background as follows:</p>
<pre>
  <code>
    <span class="green-text">&lt;s&gt;                                     structural attribute</span>
    <span class="blue-text">word    pos    lemma    simple_pos      positional attribute</span>
    <span class="green-text">&lt;ent type="PERSON"&gt;                     structural attribute</span>
    <span class="blue-text">word    pos    lemma    simple_pos      positional attribute</span>
    <span class="blue-text">word    pos    lemma    simple_pos      positional attribute</span>
    <span class="green-text">&lt;/ent&gt;                                  structural attribute</span>
    <span class="blue-text">word    pos    lemma    simple_pos      positional attribute</span>
    <span class="green-text">&lt;/s&gt;                                    structural attribute</span>
  </code>
</pre>

<h3>Positional attributes</h3>
<p>Before you can start searching for positional attributes (also called tokens), it is necessary to know what properties they contain.</p>
<ol>
  <li><span class="blue-text"><b>word</b></span>: The string as it is also found in the original text</li>
  <li>
    <span class="blue-text"><b>pos</b></span>: A code for the word type, also called POS tag
    <ol>
      <li><span class="red-text"><b>IMPORTANT</b></span>: POS tags are language-dependent to best reflect language-specific properties.</li>
      <li>The codes (= tagsets) can be taken from the Corpus Analysis Concordance page.</li>
    </ol>
  </li>
  <li><span class="blue-text"><b>lemma</b></span>: The lemmatized representation of the word</li>
  <li>
    <span class="blue-text"><b>simple_pos</b></span>: A simplified code for the word type that covers fewer categories than the <span class="blue-text"><b>pos</b></span> property, but is the same across languages.
    <ol>
      <li>The codes (= tagsets) can be taken from the Corpus Analysis Concordance page.</li>
    </ol>
  </li>
</ol>

<h4>Searching for positional attributes</h4>
<div>
  <p>
    <b>Token with no condition on any property (also called <span class="blue-text">wildcard token</span>)</b><br>
  </p>
  <pre><code>[];                            Each token matches this pattern</code></pre>
</div>
<div>
  <p>
    <b>Token with a condition on its <span class="blue-text">word</span> property</b>
  </p>
  <pre><code>[word="begin"];                “begin”</code></pre>
  <pre><code>[word="begin" %c];             same as above but ignores case</code></pre>
</div>
<div>
  <p>
    <b>Token with a condition on its <span class="blue-text">lemma</span> property</b>
  </p>
  <pre><code>[lemma="begin"];               “begin”, “began”, “beginning”, …</code></pre>
  <pre><code>[lemma="begin" %c];            same as above but ignores case</code></pre>
</div>
<div>
  <p>
    <b>Token with a condition on its <span class="blue-text">simple_pos</span> property</b>
  </p>
  <pre><code>[simple_pos="VERB"];           “begin”, “began”, “beginning”, …</code></pre>
</div>
<div>
  <p>
    <b>Token with a condition on its <span class="blue-text">pos</span> property</b>
  </p>
  <pre><code>[pos="VBG"];                   “begin”, “began”, “beginning”, …</code></pre>
</div>
<div>
  <p>
    <b>Look for words with a variable character (also called <span class="blue-text">wildcard character</span>)</b><br>
  </p>
  <pre style="margin-bottom: 0;"><code>[word="beg.n"];                “begin”, “began”, “begun”</code></pre>
  <pre style="margin-top: 0;"   ><code>          ^ the dot represents the wildcard character</code></pre>
</div>
<div>
  <p><b>Token with two conditions on its properties, where both must be fulfilled (<span class="blue-text">AND</span> operation)</b></p>
  <pre style="margin-bottom: 0;"><code>[lemma="be" & simple_pos="VERB"];          Lemma “be” and simple_pos is Verb</code></pre>
  <pre style="margin-top: 0;"   ><code>            ^ the ampersand represents the and operation</code></pre>
</div>
<div>
  <p><b>Token with two conditions on its properties, where at least one must be fulfilled (<span class="blue-text">OR</span> operation)</b></p>
  <pre style="margin-bottom: 0;"><code>[simple_pos="VERB" | simple_pos="ADJ"];    simple_pos VERB or simple_pos ADJ (adjective)</code></pre>
  <pre style="margin-top: 0;"><code>                   ^ the line represents the or operation</code></pre>
</div>
<div>
  <p><b>Sequences</b></p>
  <pre><code>[simple_pos="NOUN"] [simple_pos="VERB"];       NOUN -> VERB</code></pre>
  <pre><code>[simple_pos="NOUN"] [] [simple_pos="VERB"];    NOUN -> wildcard token -> VERB</code></pre>
</div>
<div>
  <p>
    <b>Incidence modifiers</b><br>
    Incidence Modifiers are special characters or patterns, that control how often a character/token that stands in front of it should occur.
  </p>
  <ol>
    <li><span class="blue-text"><b>+</b></span>: <span class="blue-text">One or more</span> occurrences of the character/token before</li>
    <li><span class="blue-text"><b>*</b></span>: <span class="blue-text">Zero or more occurrences</span> of the character/token before</li>
    <li><span class="blue-text"><b>?</b></span>: <span class="blue-text">Zero or one occurrences</span> of the character/token before</li>
    <li><span class="blue-text"><b>{n}</b></span>: <span class="blue-text">Exactly n occurrences</span> of the character/token before</li>
    <li><span class="blue-text"><b>{n,m}</b></span>: <span class="blue-text">Between n and m occurrences</span> of the character/token before</li>
  </ol>
  <pre><code>[word="beg.+"];        “begging”, “begin”, “began”, “begun”, …</code></pre>
  <pre><code>[word="beg.*"];        “beg”, “begging”, “begin”, “begun”, …</code></pre>
  <pre><code>[word="beg?"];         “be”, “beg”</code></pre>
  <pre><code>[word="beg.{2}"];      “begin”, “begun”, …</code></pre>
  <pre><code>[word="beg.{2,4}"];    “begging”, “begin”, “begun”, …</code></pre>
  <pre><code>[word="beg{2}.*"];     “begged”, “beggar”, …</code></pre>
  <pre><code>[simple_pos="NOUN"] []? [simple_pos="VERB"];    NOUN -> wildcard token (x0 or x1) -> VERB</code></pre>
  <pre><code>[simple_pos="NOUN"] []* [simple_pos="VERB"];    NOUN -> wildcard token (x0 or x1) -> VERB</code></pre>
</div>
<div>
  <p>
    <b>Option groups</b><br>
    Find character sequences from a list of options.
  </p>
  <pre style="margin-bottom: 0;"><code>[word="be(g|gin|gan|gun)"];			“beg”, “begin”, “began”, “begun”</code></pre>
  <pre style="margin-top: 0;"   ><code>         ^             ^ the braces indicate the start and end of an option group</code></pre>
</div>

<h3>Structural attributes</h3>
<p>nopaque provides several structural attributes for query. A distinction is made between attributes with and without value.</p>
<ol>
  <li><span class="green-text"><b>s</b></span>: Annotates a sentence</li>
  <li>
    <span class="green-text"><b>ent</b></span>: Annotates an entity
    <ol>
      <li>
        <span class="green-text"><b>*ent_type</b></span>: Annotates an entity and has as value a code that identifies the type of the entity.
        <ol>
          <li>The codes (= tagsets) can be taken from the Corpus Analysis Concordance page.</li>
        </ol>
      </li>
    </ol>
  </li>
  <li>
    <span class="green-text"><b>text</b></span>: Annotates a text
    <ol>
      <li>Note that all the following attributes have the data entered during the corpus creation as value.</li>
      <li><span class="green-text"><b>*text_address</b></span></li>
      <li><span class="green-text"><b>*text_author</b></span></li>
      <li><span class="green-text"><b>*text_booktitle</b></span></li>
      <li><span class="green-text"><b>*text_chapter</b></span></li>
      <li><span class="green-text"><b>*text_editor</b></span></li>
      <li><span class="green-text"><b>*text_institution</b></span></li>
      <li><span class="green-text"><b>*text_journal</b></span></li>
      <li><span class="green-text"><b>*text_pages</b></span></li>
      <li><span class="green-text"><b>*text_publisher</b></span></li>
      <li><span class="green-text"><b>*text_publishing_year</b></span></li>
      <li><span class="green-text"><b>*text_school</b></span></li>
      <li><span class="green-text"><b>*text_title</b></span></li>
    </ol>
  </li>
</ol>

<h4>Searching for structural attributes</h4>
<pre><code>&lt;ent&gt; [] &lt;/ent&gt;;                       A one token long entity of any type</code></pre>
<pre><code>&lt;ent_type="PERSON"&gt; [] &lt;/ent_type&gt;;     A one token long entity of type PERSON</code></pre>
<pre><code>&lt;ent_type="PERSON"&gt; []* &lt;/ent_type&gt;;    Entity of any length of type PERSON</code></pre>
<pre style="margin-bottom: 0;"><code>&lt;ent_type="PERSON"&gt; []* &lt;/ent_type&gt; []* [simple_pos="VERB"] :: match.text_publishing_year="1991";</code></pre>
<pre style="margin-top: 0;"><code>Arbitrarily long entity of type PERSON -> Arbitrarily many tokens -> VERB but only within texts with publication year 1991</code></pre>
