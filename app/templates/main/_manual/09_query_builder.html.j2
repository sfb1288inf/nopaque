<h2>Query Builder</h2>

<h3>Overview</h3>
<p>
  The query builder can be accessed via "My Corpora" or "Corpus Analysis" in the sidebar options. 
  Select the desired corpus and click on the "Analyze" and then "Concordance"
  buttons to open the query builder.
</p>

<p>
  The query builder uses the Corpus Query Language (CQL) to help you make a query for analyzing your texts. 
  In this way, it is possible to filter out various types of text parameters, for 
  example, a specific word, a lemma, or you can set part-of-speech 
  tags (pos) that indicate the type of word you are looking for (a noun, an 
  adjective, etc.). In addition, you can also search for structural attributes, 
  or specify your query for a token (word, lemma, pos) via entity typing. And of 
  course, the different text parameters can be combined.
</p>

<p>
  Tokens and structural attributes can be added by clicking on the "+" button
  (the "input marker") in the input field or the labeled buttons below it. Elements 
  added are shown as chips. These can be reorganized using drag and drop. The input 
  marker can also be moved in this way. Its position shows where new elements will be added.<br>
  A "translation" of your query into Corpus Query Language (CQL) is shown below.
</p>

<p>Advanced users can make direct use of the Corpus Query Language (CQL) by switching to "expert mode" via the toggle button.</p>
<p>The entire input field can be cleared using the red trash icon on the right.</p>

<div style="border: 1px solid; padding-left: 20px; margin-right: 400px; margin-bottom: 40px;">
  <h5>Content</h5>
  <ol style="list-style-type:disc">
    <li><a href="#add-new-token-tutorial">Add a new token to your query</a></li>
    <li><a href="#edit-options-tutorial">Options for editing your query</a></li>
    <li><a href="#add-structural-attribute-tutorial">Add structural attributes to your query</a></li>
    <li><a href="#general-options-query-builder">General options</a></li>
  </ol>
</div>

{# Add Token Tutorial #}
<div>
  <hr>
  <br>
  <h3 id="add-new-token-tutorial">Add new token to your Query</h3>
  <p>
    If you are only looking for a specific token, you can click on the left 
    button and select the type of token you are looking for from the drop-down menu. 
    By default "Word" is selected.
  </p>
  <br>

  <h4>Word and Lemma</h4>
  <p>
    If you want to search for a specific word or lemma and the respective 
    category is selected in the drop-down menu, you can type in the word or lemma 
    of your choice in the input field. You can confirm your entry by clicking the 
    Submit button on the right. You can also use the options below to modify your 
    token request before pressing the submit button. These options are explained 
    further here.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/word_lemma.gif') }}" alt="word and lemma explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>English pos, german pos or simple_pos</h4>
  <p>
    You can choose between the options "english pos", "german pos" and 
    "simple_pos" to search for different parts-of-speech. You can find an overview 
    of all tags under the "Tagsets" tab.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/pos.gif') }}" alt="part-of-speech-tag explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Empty Token</h4>
  <p>
    Here you can search for a token with unspecified attributes (also called wildcard token). This 
    selection should never stand alone and should always be extended with an incidence modifier or stand in a 
    larger query, because otherwise all possible tokens would be searched for and 
    the program would crash.
  </p>
  <br>
</div>

{# Edit Options Tutorial #}
<div>
  <hr>
  <br>
  <h3 id="edit-options-tutorial">Options for editing your query</h3> 
  <p>
    You have the possibility to extend or specify the token you are searching for with 
    certain factors. For this the query builder offers some fixed options. You can 
    find more information about the options in the Corpus Query Language Tutorial.
  </p>
  <br>

  <h4>Wildcard Character</h4>
  <p>A wildcard character replaces any character and is represented in the form of a dot. </p>
  <br>

  <h4>Option Group</h4>
  <p>
    With an option group you can search for different variants of a token. The 
    variants are not limited, so you can manually enter more options in the same 
    format. "Option1" and "option2" must be replaced accordingly.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/option_group.gif') }}" alt="option group explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Incidence Modifiers</h4>
  <p>
    With the Incidence Modifiers you can determine the occurrence of single 
    tokens. For example you can use "?" to indicate that the token occurs either 
    not at all or once:<br> 
    [word = "is"] [word="it"] [word="your"] [word="litte"]? [word = "dog"]<br>
    Here the word "little" should occur either once or not at all. With 
    [word="dogs?"] the search is for "dog "or "dogs".
  </p>
  <br>

  <h4>Ignore Case</h4>
  <p>
    With the check mark at Ignore Case the upper and lower case is ignored. 
    This is marked with a "%c". By default (if not checked) it is case sensitive.
  </p>
  <br>

  <h4>"or" & "and"</h4>
  <p>
    "Or" ( | ) and "and" ( & ) are conditions you can put on a token. With "or" 
    one of the two conditions must be fulfilled, with "and" both conditions must be 
    fulfilled. For example, the word must be called "will" AND be a verb, only then 
    it will be displayed. Note that "and" is not responsible for lining up tokens in 
    this case. For this you can simply string them together:<br>
    [word="I"] [word="will" & simple_pos="VERB"] [word="go"].
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/or_and.gif') }}" alt="OR/AND explanation" width="100%;" style="margin-bottom:20px;">

  <p>
    Tokens that have already been added can also be modified by clicking on the corresponding 
    pen icon. Click on the "ignore case" box, for example, and the query builder will 
    not differentiate between upper- and lower- case letters for that respective token. 
    New conditions added apply to the most recent token information.
  </p>
  <br>
</div>

{# Add Structural Attributes Tutorial #}
<div>
  <hr>
  <br>
  <h3 id="add-structural-attribute-tutorial">Add structural attributes to your query</h3>
  <p>
    You can use structural attributes to search specifically for structures in 
    the text or to further narrow down your previous search query.
  </p>
  <br>

  <h4>Sentence</h4>
  <p>
    With "Sentence" (&lt;s&gt;&lt;/s&gt;) you can search for sentences within your text. 
    This search can of course be specified if you search for particular tokens or 
    entities between the sentence tags (&lt;s&gt;&lt;/s&gt;). For example, you can search for
    sentences that contain only a noun, verb, and adjective. <br> 
    Click on Sentence to add the sentence chips: <span class="chip" style="background-color:#FD9720;">Sentence Start</span> 
    and <span class="chip" style="background-color:#FD9720;">Sentence End</span>. 
    These mark where the sentence starts and ends. Use drag-and-drop to place them accordingly. When 
    the Sentence attribute is added, the input marker will automatically be 
    moved between the sentence chips. Use drag-and-drop as needed to continue your query 
    at a different position.
  </p>
  <br>

  <h4>Entities</h4>
  <p>
    With entities, i.e. units of meaning, you can search for text sections that 
    contain more specific information, for example, persons, dates, or events. The 
    codes for these categories can be selected using the drop-down menus. You can find an explanation of 
    these abbreviations under the tab "Tagsets".<br>
    You can also search for unspecified entities by selecting "Add entity of any type".
  </p>

  <p>
    Click on the Entity button to add the entity chips <span class="chip" style="background-color:#A6E22D;">Entity Type=</span>
    and <span class="chip" style="background-color:#A6E22D;">Entity End</span>.
  </p>

  <p>
    The entity type can be changed by clicking on the pen symbol on the chip. When 
    the Entity attribute is added, the input marker will automatically be 
    moved between the entity chips. Use drag-and-drop as needed to continue your query 
    at a different position.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/entity.gif') }}" alt="entity explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Meta Data (currently unavailable)</h4>
  <p>
    The meta data function is being worked on and cannot currently be used!<br>
    With the meta data you can annotate your text and add specific conditions. 
    You can select a category on the left and enter your desired value on the right. 
    The selected metadata will apply to your entire request and will be added at the end.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/meta_data.gif') }}" alt="meta data explanation" width="100%;" style="margin-bottom:20px;">
  <br>
</div>

{# General Options Tutorial #}
<div>
  <hr>
  <br>
  <h3 id="general-options-query-builder">General Options of the query builder</h3>
  <p>You have several options to edit your query after adding it to the preview.</p>
  <br>

  <h4>Editing the elements</h4>
  <p>You can edit your query chips by clicking on the pen icon.</p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/editing_chips.gif') }}" alt="editing explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Deleting the elements</h4>
  <p>You can delete the added elements from the query by clicking the X behind the respective content.</p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/delete.gif') }}" alt="delete explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Move the elements of your query</h4>
  <p>You can drag and drop elements to customize your query.</p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/drag_and_drop.gif') }}" alt="Drag&Drop explanation" width="100%;" style="margin-bottom:20px;">
  <br>

  <h4>Setting an incidence modifier</h4>
  <p>
    With the incidence modifier option, you can specify the amount of 
    times a token should appear in your query. This is particularly relevant for empty
    tokens (tokens with unspecified attributes). Click on a token (blue chip) and 
    select the desired option from the list to add an incidence modifier. To 
    close the list without adding anything, click on the token again.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/incidence_modifier.gif') }}" alt="incidence modifier explanation" width="100%;" style="margin-bottom:20px;">
  <br>
  
  <h4>Switching between Query Builder and Expert mode</h4>
  <p>
    To work with the plain Corpus Query Language instead of using the Query Builder, click on the "expert mode" 
    switch. Your query can be entered into the input field. All elements previously added will be carried over 
    into expert mode. Click on the switch again to switch back to the Query Builder if desired. All recognized elements 
    will be parsed into chips; those not recognized will be deleted from the query.
  </p>
  <img src="{{ url_for('static', filename='images/manual/query_builder/expert_mode.gif') }}" alt="expert mode explanation" width="100%;" style="margin-bottom:20px;">
</div>


