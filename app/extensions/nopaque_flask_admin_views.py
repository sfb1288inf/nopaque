from flask import abort
from flask_admin import (
    AdminIndexView as _AdminIndexView,
    expose
)
from flask_admin.contrib.sqla import ModelView as _ModelView
from flask_login import current_user


class AdminIndexView(_AdminIndexView):
    @expose('/')
    def index(self):
        if not current_user.is_administrator:
            abort(403)
        return super().index()


class ModelView(_ModelView):
    def is_accessible(self):
        return current_user.is_administrator
