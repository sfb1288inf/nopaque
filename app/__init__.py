from apifairy import APIFairy
from config import Config
from docker import DockerClient
from flask import Flask
from flask.logging import default_handler
from flask_admin import Admin
from flask_apscheduler import APScheduler
from flask_assets import Environment
from flask_login import LoginManager
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_paranoid import Paranoid
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_hashids import Hashids
from logging import Formatter, StreamHandler
from werkzeug.middleware.proxy_fix import ProxyFix
from .extensions.nopaque_flask_admin_views import AdminIndexView, ModelView


docker_client = DockerClient.from_env()

admin = Admin()
apifairy = APIFairy()
assets = Environment()
db = SQLAlchemy()
hashids = Hashids()
login = LoginManager()
ma = Marshmallow()
mail = Mail()
migrate = Migrate(compare_type=True)
paranoid = Paranoid()
scheduler = APScheduler()
socketio = SocketIO()


def create_app(config: Config = Config) -> Flask:
    ''' Creates an initialized Flask object. '''

    app = Flask(__name__)
    app.config.from_object(config)

    # region Logging
    log_formatter = Formatter(
        fmt=app.config['NOPAQUE_LOG_FORMAT'],
        datefmt=app.config['NOPAQUE_LOG_DATE_FORMAT']
    )

    log_handler = StreamHandler()
    log_handler.setFormatter(log_formatter)
    log_handler.setLevel(app.config['NOPAQUE_LOG_LEVEL'])

    app.logger.setLevel('DEBUG')
    app.logger.removeHandler(default_handler)
    app.logger.addHandler(log_handler)
    # endregion Logging

    # region Middlewares
    if app.config['NOPAQUE_PROXY_FIX_ENABLED']:
        app.wsgi_app = ProxyFix(
            app.wsgi_app,
            x_for=app.config['NOPAQUE_PROXY_FIX_X_FOR'],
            x_host=app.config['NOPAQUE_PROXY_FIX_X_HOST'],
            x_port=app.config['NOPAQUE_PROXY_FIX_X_PORT'],
            x_prefix=app.config['NOPAQUE_PROXY_FIX_X_PREFIX'],
            x_proto=app.config['NOPAQUE_PROXY_FIX_X_PROTO']
        )
    # endregion Middlewares

    # region Extensions
    docker_client.login(
        app.config['NOPAQUE_DOCKER_REGISTRY_USERNAME'],
        password=app.config['NOPAQUE_DOCKER_REGISTRY_PASSWORD'],
        registry=app.config['NOPAQUE_DOCKER_REGISTRY']
    )

    from .models import AnonymousUser, User

    admin.init_app(app, index_view=AdminIndexView())
    apifairy.init_app(app)
    assets.init_app(app)
    db.init_app(app)
    hashids.init_app(app)
    login.init_app(app)
    login.anonymous_user = AnonymousUser
    login.login_view = 'auth.login'
    login.user_loader(lambda user_id: User.query.get(int(user_id)))
    ma.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    paranoid.init_app(app)
    paranoid.redirect_view = '/'
    scheduler.init_app(app)
    socketio.init_app(app, message_queue=app.config['NOPAQUE_SOCKETIO_MESSAGE_QUEUE_URI'])
    # endregion Extensions

    # region Blueprints
    from .blueprints.api import bp as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    from .blueprints.auth import bp as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .blueprints.contributions import bp as contributions_blueprint
    app.register_blueprint(contributions_blueprint, url_prefix='/contributions')

    from .blueprints.corpora import bp as corpora_blueprint
    app.register_blueprint(corpora_blueprint, cli_group='corpus', url_prefix='/corpora')

    from .blueprints.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from .blueprints.jobs import bp as jobs_blueprint
    app.register_blueprint(jobs_blueprint, url_prefix='/jobs')

    from .blueprints.main import bp as main_blueprint
    app.register_blueprint(main_blueprint, cli_group=None)

    from .blueprints.services import bp as services_blueprint
    app.register_blueprint(services_blueprint, url_prefix='/services')

    from .blueprints.settings import bp as settings_blueprint
    app.register_blueprint(settings_blueprint, url_prefix='/settings')

    from .blueprints.users import bp as users_blueprint
    app.register_blueprint(users_blueprint, cli_group='user', url_prefix='/users')

    from .blueprints.workshops import bp as workshops_blueprint
    app.register_blueprint(workshops_blueprint, url_prefix='/workshops')

    from .models import _models
    for model in _models:
        admin.add_view(ModelView(model, db.session, category='Database'))
    # endregion Blueprints

    # region SocketIO Namespaces
    from .namespaces.cqi_over_sio import CQiOverSocketIONamespace
    socketio.on_namespace(CQiOverSocketIONamespace('/cqi_over_sio'))
    # endregion SocketIO Namespaces

    # region Database event Listeners
    from .models.event_listeners import register_event_listeners
    register_event_listeners()
    # endregion Database event Listeners

    # region Add scheduler jobs
    if app.config['NOPAQUE_IS_PRIMARY_INSTANCE']:
        from .jobs import handle_corpora
        scheduler.add_job('handle_corpora', handle_corpora, seconds=3, trigger='interval')

        from .jobs import handle_jobs
        scheduler.add_job('handle_jobs', handle_jobs, seconds=3, trigger='interval')
    # endregion Add scheduler jobs

    return app
