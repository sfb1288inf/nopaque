import click
from . import bp
from .sandpaper import SandpaperConverter


@bp.cli.group('converter')
def converter():
    ''' Converter commands. '''
    pass

@converter.group('sandpaper')
def sandpaper_converter():
    ''' Sandpaper converter commands. '''
    pass

@sandpaper_converter.command('run')
@click.argument('json_db_file')
@click.argument('data_dir')
def run_sandpaper_converter(json_db_file, data_dir):
    ''' Run the sandpaper converter. '''
    sandpaper_converter = SandpaperConverter(json_db_file, data_dir)
    sandpaper_converter.run()
