from unittest import TestLoader, TextTestRunner
from unittest.suite import TestSuite
from . import bp

@bp.cli.command('run')
def run_test():
    ''' Run unit tests. '''
    tests: TestSuite = TestLoader().discover('tests')
    TextTestRunner(verbosity=2).run(tests)
