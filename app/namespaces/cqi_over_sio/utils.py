from cqi import CQiClient
from threading import Lock
from flask import session


class SessionManager:
    @staticmethod
    def setup():
        session['cqi_over_sio'] = {}

    @staticmethod
    def teardown():
        session.pop('cqi_over_sio')

    @staticmethod
    def set_corpus_id(corpus_id: int):
        session['cqi_over_sio']['corpus_id'] = corpus_id

    @staticmethod
    def get_corpus_id() -> int:
        return session['cqi_over_sio']['corpus_id']

    @staticmethod
    def set_cqi_client(cqi_client: CQiClient):
        session['cqi_over_sio']['cqi_client'] = cqi_client

    @staticmethod
    def get_cqi_client() -> CQiClient:
        return session['cqi_over_sio']['cqi_client']

    @staticmethod
    def set_cqi_client_lock(cqi_client_lock: Lock):
        session['cqi_over_sio']['cqi_client_lock'] = cqi_client_lock

    @staticmethod
    def get_cqi_client_lock() -> Lock:
        return session['cqi_over_sio']['cqi_client_lock']
