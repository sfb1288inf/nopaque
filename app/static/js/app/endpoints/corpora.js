nopaque.app.endpoints.Corpora = class Corpora {
  constructor(app) {
    this.app = app;

    this.socket = io('/corpora', {transports: ['websocket'], upgrade: false});
  }

  async delete(corpusId) {
    const options = {
      headers: {
        Accept: 'application/json'
      },
      method: 'DELETE'
    };

    const response = await fetch(`/corpora/${corpusId}`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async build(corpusId) {
    const options = {
      headers: {
        Accept: 'application/json'
      },
      method: 'POST'
    };

    const response = await fetch(`/corpora/${corpusId}/build`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async getStopwords(corpusId) {
    const options = {
      headers: {
        Accept: 'application/json'
      }
    };

    const response = await fetch(`/corpora/${corpusId}/analysis/stopwords`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async createShareLink(corpusId, expirationDate, roleName) {
    const options = {
      body: JSON.stringify({
        'expiration_date': expirationDate,
        'role_name': roleName
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST'
    };

    const response = await fetch(`/corpora/${corpusId}/create-share-link`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async updateIsPublic(corpusId, newValue) {
    const options = {
      body: JSON.stringify(newValue),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
    };

    const response = await fetch(`/corpora/${corpusId}/is-public`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }
}
