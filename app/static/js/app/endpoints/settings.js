nopaque.app.endpoints.Settings = class Settings {
  constructor(app) {
    this.app = app;
  }

  async updateProfileIsPublic(newValue) {
    const options = {
      body: JSON.stringify(newValue),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
    };

    const response = await fetch(`/settings/profile-is-public`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async updateProfileShowEmail(newValue) {
    const options = {
      body: JSON.stringify(newValue),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
    };

    const response = await fetch(`/settings/profile-show-email`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async updateProfileShowLastSeen(newValue) {
    const options = {
      body: JSON.stringify(newValue),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
    };

    const response = await fetch(`/settings/profile-show-last-seen`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async updateProfileShowMemberSince(newValue) {
    const options = {
      body: JSON.stringify(newValue),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
    };

    const response = await fetch(`/settings/profile-show-member-since`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }
}
