nopaque.app.endpoints.Jobs = class Jobs {
  constructor(app) {
    this.app = app;
  }

  async delete(jobId) {
    const options = {
      headers: {
        Accept: 'application/json'
      },
      method: 'DELETE'
    };

    const response = await fetch(`/jobs/${jobId}`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async log(jobId) {
    const options = {
      headers: {
        Accept: 'application/json'
      }
    };

    const response = await fetch(`/jobs/${jobId}/log`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async restart(jobId) {
    const options = {
      headers: {
        Accept: 'application/json'
      },
      method: 'POST'
    };

    const response = await fetch(`/jobs/${jobId}/restart`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }
}
