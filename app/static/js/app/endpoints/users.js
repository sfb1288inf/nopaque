nopaque.app.endpoints.Users = class Users {
  constructor(app) {
    this.app = app;
  }

  async get(userId) {
    const options = {
      headers: {
        Accept: 'application/json'
      }
    };

    const response = await fetch(`/users/${userId}`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }

  async subscribe(userId) {
    const response = await this.app.socket.emitWithAck('SUBSCRIBE User', userId);

    if (response.status != 200) {
      throw new Error(`[${response.status}] ${response.statusText}`);
    }
  }

  async unsubscribe(userId) {
    const response = await this.app.socket.emitWithAck('UNSUBSCRIBE User', userId);

    if (response.status != 200) {
      throw new Error(`[${response.status}] ${response.statusText}`);
    }
  }

  async delete(userId) {
    const options = {
      headers: {
        Accept: 'application/json'
      },
      method: 'DELETE'
    };

    const response = await fetch(`/users/${userId}`, options);
    const data = await response.json();

    if (!response.ok) {throw new Error(`${data.name}: ${data.description}`);}

    return data;
  }
}
