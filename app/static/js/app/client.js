nopaque.app.Client = class Client {
  constructor() {
    this.socket = io({transports: ['websocket'], upgrade: false});

    // Endpoints
    this.corpora = new nopaque.app.endpoints.Corpora(this);
    this.jobs = new nopaque.app.endpoints.Jobs(this);
    this.settings = new nopaque.app.endpoints.Settings(this);
    this.users = new nopaque.app.endpoints.Users(this);

    // Extensions
    this.toaster = new nopaque.app.extensions.Toaster(this);
    this.ui = new nopaque.app.extensions.UI(this);
    this.userHub = new nopaque.app.extensions.UserHub(this);
  }

  init() {
    // Initialize extensions
    this.toaster.init();
    this.ui.init();
    this.userHub.init();
  }
};
