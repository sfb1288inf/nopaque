nopaque.app.extensions.UI = class UI {
  constructor(app) {
    this.app = app;
  }

  init() {
    /* Pre-Initialization fixes */
    // #region

    // Flask-WTF sets the standard HTML maxlength Attribute on input/textarea
    // elements to specify their maximum length (in characters). Unfortunatly
    // Materialize won't recognize the maxlength Attribute, instead it uses
    // the data-length Attribute. It's conversion time :)
    for (let elem of document.querySelectorAll('input[maxlength], textarea[maxlength]')) {
      elem.dataset.length = elem.getAttribute('maxlength');
      elem.removeAttribute('maxlength');
    }

    // To work around some limitations with the Form setup of Flask-WTF.
    // HTML option elements with an empty value are considered as placeholder
    // elements. The user should not be able to actively select these options.
    // So they get the disabled attribute.
    for (let optionElement of document.querySelectorAll('option[value=""]')) {
      optionElement.disabled = true;
    }

    // TODO: Check why we are doing this.
    for (let optgroupElement of document.querySelectorAll('optgroup[label=""]')) {
      for (let c of optgroupElement.children) {
        optgroupElement.parentElement.insertAdjacentElement('afterbegin', c);
      }
      optgroupElement.remove();
    }
    // #endregion


    /* Initialize Materialize Components */
    // #region

    // Automatically initialize Materialize Components that do not require
    // additional configuration.
    M.AutoInit();

    // CharacterCounters
    // Materialize didn't include the CharacterCounter plugin within the
    // AutoInit method (maybe they forgot it?). Anyway... We do it here. :)
    M.CharacterCounter.init(document.querySelectorAll('input[data-length]:not(.no-autoinit), textarea[data-length]:not(.no-autoinit)'));

    // Header navigation processes and services Dropdown.
    M.Dropdown.init(
      document.querySelector('#navbar-data-processing-and-analysis-dropdown-trigger'),
      {
        constrainWidth: false,
        container: document.querySelector('#dropdowns'),
        coverTrigger: false
      }
    );

    // Header navigation account Dropdown.
    M.Dropdown.init(
      document.querySelector('#navbar-account-dropdown-trigger'),
      {
        alignment: 'right',
        constrainWidth: false,
        container: document.querySelector('#dropdowns'),
        coverTrigger: false
      }
    );

    // Terms of use modal
    M.Modal.init(
      document.querySelector('#terms-of-use-modal'),
      {
        dismissible: false,
        onCloseEnd: (modalElement) => {
          nopaque.requests.users.entity.acceptTermsOfUse();
        }
      }
    );
    // #endregion


    /* Initialize nopaque Components */
    // #region
    nopaque.resource_displays.AutoInit();
    nopaque.resource_lists.AutoInit();
    nopaque.forms.AutoInit();
    // #endregion
  }

  flash(message, category) {
    let iconPrefix;

    switch (category) {
      case 'corpus': {
        iconPrefix = '<i class="material-icons left">book</i>';
        break;
      }
      case 'job': {
        iconPrefix = '<i class="nopaque-icons left">J</i>';
        break;
      }
      case 'error': {
        iconPrefix = '<i class="material-icons left error-color-text">error</i>';
        break;
      }
      default: {
        iconPrefix = '<i class="material-icons left">notifications</i>';
        break;
      }
    }

    let toast = M.toast(
      {
        html: `
          <span>${iconPrefix}${message}</span>
          <button class="btn-flat toast-action white-text" data-toast-action="dismiss">
            <i class="material-icons">close</i>
          </button>
        `.trim()
      }
    );
    let dismissToastElement = toast.el.querySelector('.toast-action[data-toast-action="dismiss"]');
    dismissToastElement.addEventListener('click', () => {toast.dismiss();});
  }
}
