/*****************************************************************************
* Users                                                                      *
* Fetch requests for /users routes                                           *
*****************************************************************************/
nopaque.requests.users = {};

nopaque.requests.users.entity = {};

nopaque.requests.users.entity.delete = (userId) => {
  let input = `/users/${userId}`;
  let init = {
    method: 'DELETE'
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.users.entity.acceptTermsOfUse = () => {
  let input = `/users/accept-terms-of-use`;
  let init = {
    method: 'POST'
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.users.entity.avatar = {};

nopaque.requests.users.entity.avatar.delete = (userId) => {
  let input = `/users/${userId}/avatar`;
  let init = {
    method: 'DELETE'
  };
  return nopaque.requests.JSONfetch(input, init);
}


/*****************************************************************************
* Requests for /users/<entity>/settings routes                               *
*****************************************************************************/
nopaque.requests.users.entity.settings = {};

nopaque.requests.users.entity.settings.profilePrivacy = {};

nopaque.requests.users.entity.settings.profilePrivacy.update = (userId, profilePrivacySetting, enabled) => {
  let input = `/users/${userId}/settings/profile-privacy/${profilePrivacySetting}`;
  let init = {
    method: 'PUT',
    body: JSON.stringify(enabled)
  };
  return nopaque.requests.JSONfetch(input, init);
};

