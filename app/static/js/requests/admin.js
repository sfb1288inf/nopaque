/*****************************************************************************
 * Requests for /admin routes                                                *
*****************************************************************************/
nopaque.requests.admin = {};

nopaque.requests.admin.users = {};

nopaque.requests.admin.users.entity = {};

nopaque.requests.admin.users.entity.confirmed = {};

nopaque.requests.admin.users.entity.confirmed.update = (userId, value) => {
  let input = `/admin/users/${userId}/confirmed`;
  let init = {
    method: 'PUT',
    body: JSON.stringify(value)
  };
  return nopaque.requests.JSONfetch(input, init);
};
