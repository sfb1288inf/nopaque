/*****************************************************************************
* Requests for /contributions routes                                         *
*****************************************************************************/
nopaque.requests.contributions = {};


/*****************************************************************************
* Requests for /contributions/spacy-nlp-pipeline-models routes               *
*****************************************************************************/
nopaque.requests.contributions.spacy_nlp_pipeline_models = {};

nopaque.requests.contributions.spacy_nlp_pipeline_models.entity = {};

nopaque.requests.contributions.spacy_nlp_pipeline_models.entity.delete = (spacyNlpPipelineModelId) => {
  let input = `/contributions/spacy-nlp-pipeline-models/${spacyNlpPipelineModelId}`;
  let init = {
    method: 'DELETE'
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.contributions.spacy_nlp_pipeline_models.entity.isPublic = {};

nopaque.requests.contributions.spacy_nlp_pipeline_models.entity.isPublic.update = (spacyNlpPipelineModelId, value) => {
  let input = `/contributions/spacy-nlp-pipeline-models/${spacyNlpPipelineModelId}/is_public`;
  let init = {
    method: 'PUT',
    body: JSON.stringify(value)
  };
  return nopaque.requests.JSONfetch(input, init);
};


/*****************************************************************************
* Requests for /contributions/tesseract-ocr-pipeline-models routes           *
*****************************************************************************/
nopaque.requests.contributions.tesseract_ocr_pipeline_models = {};

nopaque.requests.contributions.tesseract_ocr_pipeline_models.entity = {};

nopaque.requests.contributions.tesseract_ocr_pipeline_models.entity.delete = (tesseractOcrPipelineModelId) => {
  let input = `/contributions/tesseract-ocr-pipeline-models/${tesseractOcrPipelineModelId}`;
  let init = {
    method: 'DELETE'
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.contributions.tesseract_ocr_pipeline_models.entity.isPublic = {};

nopaque.requests.contributions.tesseract_ocr_pipeline_models.entity.isPublic.update = (tesseractOcrPipelineModelId, value) => {
  let input = `/contributions/tesseract-ocr-pipeline-models/${tesseractOcrPipelineModelId}/is_public`;
  let init = {
    method: 'PUT',
    body: JSON.stringify(value)
  };
  return nopaque.requests.JSONfetch(input, init);
};
