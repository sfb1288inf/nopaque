/*****************************************************************************
* Requests for /corpora routes                                               *
*****************************************************************************/
nopaque.requests.corpora = {};

nopaque.requests.corpora.entity = {};


/*****************************************************************************
* Requests for /corpora/<entity>/files routes                                *
*****************************************************************************/
nopaque.requests.corpora.entity.files = {};

nopaque.requests.corpora.entity.files.ent = {};

nopaque.requests.corpora.entity.files.ent.delete = (corpusId, corpusFileId) => {
  let input = `/corpora/${corpusId}/files/${corpusFileId}`;
  let init = {
    method: 'DELETE',
  };
  return nopaque.requests.JSONfetch(input, init);
};


/*****************************************************************************
* Requests for /corpora/<entity>/followers routes                            *
*****************************************************************************/
nopaque.requests.corpora.entity.followers = {};

nopaque.requests.corpora.entity.followers.add = (corpusId, usernames) => {
  let input = `/corpora/${corpusId}/followers`;
  let init = {
    method: 'POST',
    body: JSON.stringify(usernames)
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.corpora.entity.followers.entity = {};

nopaque.requests.corpora.entity.followers.entity.delete = (corpusId, followerId) => {
  let input = `/corpora/${corpusId}/followers/${followerId}`;
  let init = {
    method: 'DELETE',
  };
  return nopaque.requests.JSONfetch(input, init);
};

nopaque.requests.corpora.entity.followers.entity.role = {};

nopaque.requests.corpora.entity.followers.entity.role.update = (corpusId, followerId, value) => {
  let input = `/corpora/${corpusId}/followers/${followerId}/role`;
  let init = {
    method: 'PUT',
    body: JSON.stringify(value)
  };
  return nopaque.requests.JSONfetch(input, init);
};
