nopaque.resource_lists.DetailedPublicCorpusList = class DetailedPublicCorpusList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'detailed-public-corpus-list';

  get item() {
    return (values) => {
      return `
        <tr class="list-item clickable hoverable">
          <td></td>
          <td><b class="title"></b><br><i class="description"></i></td>
          <td><span class="owner"></span></td>
          <td><span class="status badge new corpus-status-color corpus-status-text" data-badge-caption=""></span></td>
          <td>${values['current-user-is-following'] ? '<span><i class="left material-icons">visibility</i>Following</span>' : ''}</td>
          <td class="right-align">
            <a class="list-action-trigger btn-floating service-color darken waves-effect waves-light" data-list-action="view" data-service="corpus-analysis"><i class="material-icons">send</i></a>
          </td>
        </tr>
      `.trim();
    };
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['creation-date']},
      {name: 'status', attr: 'data-status'},
      'description',
      'title',
      'owner',
      'current-user-is-following'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search Corpus</label>
      </div>
      <table>
        <thead>
          <tr>
            <th></th>
            <th>Title and Description</th>
            <th>Owner</th>
            <th>Status</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(corpus) {
    return {
      'id': corpus.id,
      'creation-date': corpus.creation_date,
      'description': corpus.description,
      'status': corpus.status,
      'title': corpus.title,
      'owner': corpus.user.username,
      'is-owner': corpus.user.id === this.userId,
      'current-user-is-following': Object.values(corpus.corpus_follower_associations).some(association => association.follower.id === currentUserId)
    };
  }
};
