nopaque.resource_lists.JobResultList = class JobResultList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'job-result-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
    this.isInitialized = false;
    this.userId = listContainerElement.dataset.userId;
    this.jobId = listContainerElement.dataset.jobId;
    if (this.userId === undefined || this.jobId === undefined) {return;}
    app.userHub.addEventListener('patch', (event) => {
      if (this.isInitialized) {this.onPatch(event.detail);}
    });
    app.userHub.get(this.userId).then((user) => {
      this.add(Object.values(user.jobs[this.jobId].results));
      this.isInitialized = true;
    });
  }

  get item() {
    return `
      <tr class="list-item clickable hoverable">
        <td><span class="description"></span></td>
        <td><span class="filename"></span></td>
        <td class="right-align">
          <a class="list-action-trigger btn-floating waves-effect waves-light" data-list-action="download"><i class="material-icons">file_download</i></a>
        </td>
      </tr>
    `.trim();
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['creation-date']},
      'description',
      'filename'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('job-result-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search job result</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>Description</th>
            <th>Filename</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(jobResult) {
    return {
      'id': jobResult.id,
      'creation-date': jobResult.creation_date,
      'description': jobResult.description,
      'filename': jobResult.filename
    };
  }

  sort() {
    this.listjs.sort('filename', {order: 'asc'});
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'download' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'download': {
        window.location.href = `/jobs/${this.jobId}/results/${itemId}/download`;
        break;
      }
      default: {
        break;
      }
    }
  }

  onPatch(patch) {
    let re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}/results/([A-Za-z0-9]*)`);
    let filteredPatch = patch.filter(operation => re.test(operation.path));
    for (let operation of filteredPatch) {
      switch(operation.op) {
        case 'add': {
          let re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}/results/([A-Za-z0-9]*)$`);
          if (re.test(operation.path)) {this.add(operation.value);}
          break;
        }
        default: {
          break;
        }
      }
    }
  }
};
