nopaque.resource_lists.AdminUserList = class AdminUserList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'admin-user-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
  }

  get item() {
    return `
      <tr class="list-item clickable hoverable">
        <td><span class="id-1"></span></td>
        <td><span class="username"></span></td>
        <td><span class="email"></span></td>
        <td><span class="last-seen"></span></td>
        <td><span class="role"></span></td>
        <td class="right-align">
          <a class="list-action-trigger btn-floating red waves-effect waves-light" data-list-action="delete"><i class="material-icons">delete</i></a>
          <a class="list-action-trigger btn-floating waves-effect waves-light" data-list-action="edit"><i class="material-icons">edit</i></a>
          <a class="list-action-trigger btn-floating waves-effect waves-light" data-list-action="view"><i class="material-icons">send</i></a>
        </td>
      </tr>
    `.trim();
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['member-since']},
      'email',
      'id-1',
      'last-seen',
      'role',
      'username'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('user-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search user</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Last seen</th>
            <th>Role</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(user) {
    return {
      'id': user.id,
      'id-1': user.id,
      'username': user.username,
      'email': user.email,
      'last-seen': new Date(user.last_seen).toLocaleString('en-US'),
      'member-since': user.member_since,
      'role': user.role.name
    };
  }

  sort() {
    this.listjs.sort('member-since', {order: 'desc'});
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'view' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'delete': {
        nopaque.requests.users.entity.delete(itemId);
        if (itemId === currentUserId) {window.location.href = '/';}
        break;
      }
      case 'edit': {
        window.location.href = `/admin/users/${itemId}/edit`;
        break;
      }
      case 'view': {
        window.location.href = `/admin/users/${itemId}`;
        break;
      }
      default: {
        break;
      }
    }
  }
};
