nopaque.resource_lists.PublicUserList = class PublicUserList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'public-user-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
  }

  get item() {
    return `
      <tr class="list-item clickable hoverable">
        <td><img alt="user-image" class="circle responsive-img avatar" style="width:25%"></td>
        <td><b><span class="username"></span><b></td>
        <td><span class="full-name"></span></td>
        <td><span class="location"></span></td>
        <td><span class="organization"></span></td>
        <td><span class="corpora-online"></span></td>
        <td class="right-align">
          <a class="list-action-trigger btn-floating waves-effect waves-light social-area-color-darken" data-list-action="view"><i class="material-icons">send</i></a>
        </td>
      </tr>
    `.trim();
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['member-since']},
      {name: 'avatar', attr: 'src'},
      'username',
      'full-name',
      'location',
      'organization',
      'corpora-online'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('user-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search user</label>
      </div>
      <table>
        <thead>
          <tr>
            <th style="width:15%;"></th>
            <th>Username</th>
            <th>Full name</th>
            <th>Location</th>
            <th>Organization</th>
            <th>Corpora online</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(user) {
    return {
      'id': user.id,
      'member-since': user.member_since,
      'avatar': user.avatar,
      'username': user.username,
      'full-name': user.full_name ? user.full_name : '',
      'location': user.location ? user.location : '',
      'organization': user.organization ? user.organization : '',
      'corpora-online': Object.values(user.corpora).filter((corpus) => corpus.is_public).length
    };
  };

  sort() {
    this.listjs.sort('member-since', {order: 'desc'});
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'view' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'view': {
        window.location.href = `/users/${itemId}`;
        break;
      }
      default: {
        break;
      }
    }
  }
};
