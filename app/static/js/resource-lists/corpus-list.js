nopaque.resource_lists.CorpusList = class CorpusList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'corpus-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
    document.querySelectorAll('.corpus-list-selection-action-trigger[data-selection-action]').forEach((element) => {
      element.addEventListener('click', (event) => {this.onSelectionAction(event)});
    });
    this.isInitialized = false
    this.selectedItemIds = new Set();
    this.userId = listContainerElement.dataset.userId;
    if (this.userId === undefined) {return;}
    app.userHub.addEventListener('patch', (event) => {
      if (this.isInitialized) {this.onPatch(event.detail);}
    });
    app.userHub.get(this.userId).then((user) => {
      this.add(this.aggregateData(user));
      this.isInitialized = true;
    });
  }

  aggregateData(user) {
    const aggregatedData = [];
    for (let corpus of Object.values(user.corpora)) {
      aggregatedData.push(
        {
          'id': corpus.id,
          'creation-date': corpus.creation_date,
          'description': corpus.description,
          'status': corpus.status,
          'title': corpus.title,
          'owner': user.username,
          'is-owner': true,
          'current-user-is-following': false
        }
      );
    }
    for (let cfa of Object.values(user.corpus_follower_associations)) {
      aggregatedData.push(
        {
          'id': cfa.corpus.id,
          'creation-date': cfa.corpus.creation_date,
          'description': cfa.corpus.description,
          'status': cfa.corpus.status,
          'title': cfa.corpus.title,
          'owner': cfa.corpus.user.username,
          'is-owner': false,
          'current-user-is-following': true
        }
      );
    }
    return aggregatedData;
  }

  // #region Mandatory getters and methods to implement
  get item() {
    return (values) => {
      return `
        <tr class="list-item clickable hoverable">
          <td class="hide">
            <time class="creation-date"></span>
          </td>
          <td>
            <label class="list-action-trigger" data-list-action="select">
              <input class="select-checkbox" type="checkbox">
              <span class="disable-on-click"></span>
            </label>
          </td>
          <td><a class="btn-floating service-color darken" data-service="corpus-analysis"><i class="material-icons">book</i></a></td>
          <td>
            <b class="title"></b><br>
            <i class="description"></i>
          </td>
          <td>
            <span class="owner"></span>
          </td>
          <td><span class="status badge new corpus-status-color corpus-status-text" data-badge-caption=""></span></td>
          <td>${values['current-user-is-following'] ? '<span><i class="left material-icons">visibility</i>Following</span>' : ''}</td>
          <td class="right-align">
            <a class="list-action-trigger btn-floating red waves-effect waves-light" data-list-action="delete-request"><i class="material-icons">delete</i></a>
            <a class="list-action-trigger btn-floating waves-effect waves-light" data-list-action="view"><i class="material-icons">send</i></a>
          </td>
        </tr>
      `.trim();
    };
  }

  get valueNames() {
    return [
      {data: ['id']},
      {name: 'creation-date', attr: 'datetime'},
      {name: 'status', attr: 'data-corpus-status'},
      'description',
      'title',
      'owner',
      'current-user-is-following'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search Corpus</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>
              <label class="corpus-list-selection-action-trigger" data-selection-action="select-all">
                <input class="corpus-list-select-all-checkbox" type="checkbox">
                <span></span>
              </label>
            </th>
            <th></th>
            <th>Title and Description</th>
            <th>Owner</th>
            <th>Status</th>
            <th></th>
            <th class="right-align">
              <a class="corpus-list-selection-action-trigger btn-floating red waves-effect waves-light hide" data-selection-action="delete"><i class="material-icons">delete</i></a>
            </th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  sort() {
    this.listjs.sort('creation-date', {order: 'desc'});
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'view' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'delete-request': {
        let values = this.listjs.get('id', itemId)[0].values();
        let modalElement = nopaque.Utils.HTMLToElement(
          `
            <div class="modal">
              <div class="modal-content">
                <h4>Confirm Corpus deletion</h4>
                <p>Do you really want to ${values['is-owner'] ? 'delete' : 'unfollow'} the Corpus <b>${values.title}</b>? ${values['is-owner'] ? 'All files will be permanently deleted!' : ''}</p>
              </div>
              <div class="modal-footer">
                <a class="btn modal-close waves-effect waves-light">Cancel</a>
                <a class="action-button btn modal-close red waves-effect waves-light" data-action="confirm">Delete</a>
              </div>
            </div>
          `
        );
        document.querySelector('#modals').appendChild(modalElement);
        let modal = M.Modal.init(
          modalElement,
          {
            dismissible: false,
            onCloseEnd: () => {
              modal.destroy();
              modalElement.remove();
            }
          }
        );
        let confirmElement = modalElement.querySelector('.action-button[data-action="confirm"]');
        confirmElement.addEventListener('click', (event) => {
          if (!values['is-owner']) {
            nopaque.requests.corpora.entity.followers.entity.delete(itemId, currentUserId)
              .then((response) => {
                window.location.reload();
              });
          } else {
            app.corpora.delete(itemId);
          }
        });
        modal.open();
        break;
      }
      case 'view': {
        window.location.href = `/corpora/${itemId}`;
        break;
      }
      case 'select': {
        if (event.target.checked) {
          this.selectedItemIds.add(itemId);
        } else {
          this.selectedItemIds.delete(itemId);
        }
        this.renderingItemSelection();
      }
      default: {
        break;
      }
    }
  }

  onSelectionAction(event) {
    let selectionActionElement = event.target.closest('.corpus-list-selection-action-trigger[data-selection-action]');
    let selectionAction = selectionActionElement.dataset.selectionAction;
    let items = Array.from(this.listjs.items);
    let selectableItems = Array.from(items)
      .filter(item => item.elm)
      .map(item => item.elm.querySelector('.select-checkbox[type="checkbox"]'));

    switch (selectionAction) {
      case 'select-all': {
        let selectedIds = new Set(Array.from(items)
          .map(item => item.values().id))
        if (event.target.checked !== undefined) {
          if (event.target.checked) {
            selectableItems.forEach(selectableItem => selectableItem.checked = true);
            this.selectedItemIds = selectedIds;
          } else {
            selectableItems.forEach(checkbox => checkbox.checked = false);
            this.selectedItemIds = new Set([...this.selectedItemIds].filter(id => !selectedIds.has(id)));
          }
          this.renderingItemSelection();
        }
        break;
      }
      case 'delete': {
        // Saved for future use:
        // <p class="hide">Do you really want to unfollow this Corpora?</p>
        // <ul id="selected-unfollow-items-list"></ul>
        let modalElement = nopaque.Utils.HTMLToElement(
          `
            <div class="modal">
              <div class="modal-content">
                <h4>Confirm Corpus deletion</h4>
                <p>Do you really want to delete this Corpora? <i>All corpora will be permanently deleted!</i></p>
                <ul id="selected-deletion-items-list"></ul>
              </div>
              <div class="modal-footer">
                <a class="btn modal-close waves-effect waves-light">Cancel</a>
                <a class="action-button btn modal-close red waves-effect waves-light" data-action="confirm">Delete</a>
              </div>
            </div>
          `
        );
        document.querySelector('#modals').appendChild(modalElement);
        let itemDeletionList = document.querySelector('#selected-deletion-items-list');
        // let itemUnfollowList = document.querySelector('#selected-unfollow-items-list');
        this.selectedItemIds.forEach(selectedItemId => {
          let listItem = this.listjs.get('id', selectedItemId)[0].elm;
          let values = this.listjs.get('id', listItem.dataset.id)[0].values();
          let itemElement = nopaque.Utils.HTMLToElement(`<li> - ${values.title}</li>`);
          // if (!values['is-owner']) {
          //   itemUnfollowList.appendChild(itemElement);
          // } else {
          itemDeletionList.appendChild(itemElement);
          // }
        });
        let modal = M.Modal.init(
          modalElement,
          {
            dismissible: false,
            onCloseEnd: () => {
              modal.destroy();
              modalElement.remove();
            }
          }
        );
        let confirmElement = modalElement.querySelector('.action-button[data-action="confirm"]');
        confirmElement.addEventListener('click', (event) => {
          this.selectedItemIds.forEach(selectedItemId => {
            let listItem = this.listjs.get('id', selectedItemId)[0].elm;
            let values = this.listjs.get('id', listItem.dataset.id)[0].values();
            if (values['is-owner']) {
              app.corpora.delete(selectedItemId);
            } else {
              nopaque.requests.corpora.entity.followers.entity.delete(selectedItemId, currentUserId);
              setTimeout(() => {
                window.location.reload();
              }, 1000);
            }
          });
          this.selectedItemIds.clear();
          this.renderingItemSelection();

        });
        modal.open();
        break;
      }
      default: {
        break;
      }
    }
  }

  renderingItemSelection() {
    let selectionActionButtons = document.querySelectorAll('.corpus-list-selection-action-trigger:not([data-selection-action="select-all"])');
    let selectableItems = this.listjs.items;
    let actionButtons = [];

    Object.values(selectableItems).forEach(selectableItem => {
      if (selectableItem.elm) {
        let checkbox = selectableItem.elm.querySelector('.select-checkbox[type="checkbox"]');
        if (checkbox.checked) {
          selectableItem.elm.classList.add('grey', 'lighten-3');
        } else {
          selectableItem.elm.classList.remove('grey', 'lighten-3');
        }
        let itemActionButtons = selectableItem.elm.querySelectorAll('.list-action-trigger:not([data-list-action="select"])');
        itemActionButtons.forEach(itemActionButton => {
          actionButtons.push(itemActionButton);
        });
      }
    });
    // Hide item action buttons if > 0 item is selected and show selection action buttons
    if (this.selectedItemIds.size > 0) {
      selectionActionButtons.forEach(selectionActionButton => {
        selectionActionButton.classList.remove('hide');
      });
      actionButtons.forEach(actionButton => {
        actionButton.classList.add('hide');
      });
    } else {
      selectionActionButtons.forEach(selectionActionButton => {
        selectionActionButton.classList.add('hide');
      });
      actionButtons.forEach(actionButton => {
        actionButton.classList.remove('hide');
      });
    }

    // Check select all checkbox if all items are selected
    let selectAllCheckbox = document.querySelector('.corpus-list-select-all-checkbox[type="checkbox"]');
    if (selectableItems.length === this.selectedItemIds.size && selectAllCheckbox.checked === false) {
      selectAllCheckbox.checked = true;
    } else if (selectableItems.length !== this.selectedItemIds.size && selectAllCheckbox.checked === true) {
      selectAllCheckbox.checked = false;
    }
  }

  onPatch(patch) {
    let re = new RegExp(`^/users/${this.userId}/corpora/([A-Za-z0-9]*)`);
    let filteredPatch = patch.filter(operation => re.test(operation.path));
    for (let operation of filteredPatch) {
      switch(operation.op) {
        case 'add': {
          let re = new RegExp(`^/users/${this.userId}/corpora/([A-Za-z0-9]*)$`);
          if (re.test(operation.path)) {this.add(operation.value);}
          break;
        }
        case 'remove': {
          let re = new RegExp(`^/users/${this.userId}/corpora/([A-Za-z0-9]*)$`);
          if (re.test(operation.path)) {
            let [match, corpusId] = operation.path.match(re);
            this.remove(corpusId);
          }
          break;
        }
        case 'replace': {
          let re = new RegExp(`^/users/${this.userId}/corpora/([A-Za-z0-9]*)/(status|description|title)$`);
          if (re.test(operation.path)) {
            let [match, corpusId, valueName] = operation.path.match(re);
            this.replace(corpusId, valueName, operation.value);
          }
          break;
        }
        default: {
          break;
        }
      }
    }
  }
};
