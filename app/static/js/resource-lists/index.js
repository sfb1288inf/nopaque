nopaque.resource_lists = {};

nopaque.resource_lists.AutoInit = () => {
  for (let propertyName in nopaque.resource_lists) {
    let property = nopaque.resource_lists[propertyName];
    // Initialize properties that are subclasses of `nopaque.resource_lists.ResourceList`.
    // This does not include `nopaque.resource_lists.ResourceList` itself.
    if (property.prototype instanceof nopaque.resource_lists.ResourceList) {
      // Check if the static `htmlClass` property is defined.
      if (property.htmlClass === undefined) {return;}
      // Gather all HTML elements that have the `this.htmlClass` class
      // and do not have the `no-autoinit` class.
      let listElements = document.querySelectorAll(`.${property.htmlClass}:not(.no-autoinit)`);
      // Create an instance of this class for each display element.
      for (let listElement of listElements) {new property(listElement);}
    }
  }
};
