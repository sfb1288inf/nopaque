nopaque.resource_lists.JobInputList = class JobInputList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'job-input-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
    this.isInitialized = false;
    this.userId = listContainerElement.dataset.userId;
    this.jobId = listContainerElement.dataset.jobId;
    if (this.userId === undefined || this.jobId === undefined) {return;}
    // app.userHub.addEventListener('patch', (event) => {
    //   if (this.isInitialized) {this.onPatch(event.detail);}
    // });
    app.userHub.get(this.userId).then((user) => {
      this.add(Object.values(user.jobs[this.jobId].inputs));
      this.isInitialized = true;
    });
  }

  get item() {
    return `
      <tr class="list-item clickable hoverable">
        <td><span class="filename"></span></td>
        <td class="right-align">
          <a class="list-action-trigger btn-floating waves-effect waves-light" data-list-action="download"><i class="material-icons">file_download</i></a>
        </td>
      </tr>
    `.trim();
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['creation-date']},
      'filename'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('job-input-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search job input</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>Filename</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(jobInput) {
    return {
      'id': jobInput.id,
      'creation-date': jobInput.creation_date,
      'filename': jobInput.filename
    };
  }

  sort() {
    this.listjs.sort('filename', {order: 'asc'});
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'download' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'download': {
        window.location.href = `/jobs/${this.jobId}/inputs/${itemId}/download`;
        break;
      }
      default: {
        break;
      }
    }
  }
};
