nopaque.resource_lists.CorpusTokenList = class CorpusTokenList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'corpus-token-list';

  static defaultOptions = {
    page: 7
  };

  constructor(listContainerElement, options = {}) {
    let _options = nopaque.Utils.mergeObjectsDeep(
      nopaque.resource_lists.CorpusTokenList.defaultOptions,
      options
    );
    super(listContainerElement, _options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
    this.selectedItemTerms = new Set();
    this.listjs.on('sortComplete', () => {
      let listItems = Array.from(this.listjs.items).filter(item => item.elm);
      for (let item of listItems) {
        let termElement = item.elm.querySelector('.term');
        let mostFrequent = item.elm.dataset.mostfrequent === 'true';
        if (mostFrequent) {
          this.selectedItemTerms.add(termElement.textContent);
        }
      }
      corpusAnalysisApp.extensions['Static Visualization (beta)'].renderFrequenciesGraphic(this.selectedItemTerms);
    });
    
    let tokenListResetButtonElement = this.listContainerElement.querySelector('#token-list-reset-button');
    tokenListResetButtonElement.addEventListener('click', () => {
      this.selectedItemTerms.clear();
      let listItems = Array.from(this.listjs.items).filter(item => item.elm);
      for (let item of listItems) {
        let termElement = item.elm.querySelector('.term');
        let mostFrequent = item.elm.dataset.mostfrequent === 'true';
        if (mostFrequent) {
          item.elm.querySelector('.select-checkbox').checked = true;
          this.selectedItemTerms.add(termElement.textContent);
        } else {
          item.elm.querySelector('.select-checkbox').checked = false;
        }
      }
      corpusAnalysisApp.extensions['Static Visualization (beta)'].renderFrequenciesGraphic(this.selectedItemTerms);
    });
  }

  get item() {
    return (values) => {
      return `
        <tr class="list-item clickable hoverable">
          <td>
            <label class="list-action-trigger" data-list-action="select">
              <input class="select-checkbox" type="checkbox" ${values.mostFrequent ? 'checked="checked"' : ''}>
              <span class="disable-on-click"></span>
            </label>
          </td>
          <td><span class="term"></span></td>
          <td><span class="count"></span></td>
          <td><span class="frequency"></span></td>
        </tr>
      `.trim();
    }
  }

  get valueNames() {
    return [
      'term',
      'count',
      {data: ['mostFrequent']},
      'frequency'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-token-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search token</label>
      </div>
      <table>
        <thead>
          <tr>
            <th style="width:15%;">
              <span class="material-icons" style="cursor:pointer" id="token-list-reset-button">refresh</span>
            </th>
            <th>Term</th>
            <th>Count</th>
            <th>Frequency</th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(corpusTokenData) {
    return {
      term: corpusTokenData.term,
      count: corpusTokenData.count,
      mostFrequent: corpusTokenData.mostFrequent,
      frequency: '-'
    };
  }

  sort() {
    this.listjs.sort('count', {order: 'desc'});
  }

  onClick(event) {
    if (event.target.closest('.disable-on-click') !== null) {return;}
    let listItemElement = event.target.closest('.list-item');
    if (listItemElement === null) {return;}
    let item = listItemElement.querySelector('.term').textContent;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? '' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'select': {
        if (event.target.checked) {
          this.selectedItemTerms.add(item);
        } else {
          this.selectedItemTerms.delete(item);
        }
        corpusAnalysisApp.extensions['Static Visualization (beta)'].renderFrequenciesGraphic(this.selectedItemTerms);
        break;
      }
      default: {
        break;
      }
    }
  }

};
