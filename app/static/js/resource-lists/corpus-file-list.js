nopaque.resource_lists.CorpusFileList = class CorpusFileList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'corpus-file-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
    document.querySelectorAll('.selection-action-trigger[data-selection-action]').forEach((element) => {
      element.addEventListener('click', (event) => {this.onSelectionAction(event)});
    });
    this.isInitialized = false;
    this.selectedItemIds = new Set();
    this.userId = listContainerElement.dataset.userId;
    this.corpusId = listContainerElement.dataset.corpusId;
    this.hasPermissionView =  listContainerElement.dataset?.hasPermissionView == 'true' || false;
    this.hasPermissionManageFiles =  listContainerElement.dataset?.hasPermissionManageFiles == 'true' || false;
    if (this.userId === undefined || this.corpusId === undefined) {return;}
    app.userHub.addEventListener('patch', (event) => {
      if (this.isInitialized) {this.onPatch(event.detail);}
    });
    app.userHub.get(this.userId).then((user) => {
      // TODO: Make this better understandable
      this.add(Object.values(user.corpora[this.corpusId].files || user.followed_corpora[this.corpusId].files));
      this.isInitialized = true;
    });
  }

  get item() {
    return (values) => {
      return `
        <tr class="list-item clickable hoverable">
          <td>
            <label class="list-action-trigger ${this.hasPermissionView ? '' : 'hide'}" data-list-action="select">
              <input class="select-checkbox" type="checkbox">
              <span class="disable-on-click"></span>
            </label>
          </td>
          <td><span class="filename"></span></td>
          <td><span class="author"></span></td>
          <td><span class="title"></span></td>
          <td><span class="publishing-year"></span></td>
          <td class="right-align">
            <a class="list-action-trigger btn-floating red waves-effect waves-light ${this.hasPermissionManageFiles ? '' : 'hide'}" data-list-action="delete"><i class="material-icons">delete</i></a>
            <a class="list-action-trigger btn-floating waves-effect waves-light ${this.hasPermissionView ? '' : 'hide'}" data-list-action="download"><i class="material-icons">file_download</i></a>
            <a class="list-action-trigger btn-floating waves-effect waves-light ${this.hasPermissionManageFiles ? '' : 'hide'}" data-list-action="view"><i class="material-icons">send</i></a>
          </td>
        </tr>
      `.trim();
    }
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['creation-date']},
      'author',
      'filename',
      'publishing-year',
      'title'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-file-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search corpus file</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>
              <label class="disable-on-click selection-action-trigger ${this.listContainerElement.dataset?.hasPermissionView == 'true' ? '' : 'hide'}" data-selection-action="select-all">
                <input class="select-all-checkbox" type="checkbox">
                <span class="disable-on-click"></span>
              </label>
            </th>
            <th>Filename</th>
            <th>Author</th>
            <th>Title</th>
            <th>Publishing year</th>
            <th class="right-align">
              <a class="selection-action-trigger btn-floating red waves-effect waves-light hide" data-selection-action="delete"><i class="material-icons">delete</i></a>
              <a class="selection-action-trigger btn-floating service-color darken waves-effect waves-light hide" data-selection-action="download" data-service="corpus-analysis"><i class="material-icons">file_download</i></a>
            </th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(corpusFile) {
    return {
      'id': corpusFile.id,
      'author': corpusFile.author,
      'creation-date': corpusFile.creation_date,
      'filename': corpusFile.filename,
      'publishing-year': corpusFile.publishing_year,
      'title': corpusFile.title
    };
  }

  sort() {
    this.listjs.sort('creation-date', {order: 'desc'});
  }

  onClick(event) {
    if (event.target.closest('.disable-on-click') !== null) {return;}
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'view' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'delete': {
        let values = this.listjs.get('id', itemId)[0].values();
        let modalElement = nopaque.Utils.HTMLToElement(
          `
            <div class="modal">
              <div class="modal-content">
                <h4>Confirm Corpus File deletion</h4>
                <p>Do you really want to delete the Corpus File <b>${values.title}</b>? All files will be permanently deleted!</p>
              </div>
              <div class="modal-footer">
                <a class="btn modal-close waves-effect waves-light">Cancel</a>
                <a class="action-button btn modal-close red waves-effect waves-light" data-action="confirm">Delete</a>
              </div>
            </div>
          `
        );
        document.querySelector('#modals').appendChild(modalElement);
        let modal = M.Modal.init(
          modalElement,
          {
            dismissible: false,
            onCloseEnd: () => {
              modal.destroy();
              modalElement.remove();
            }
          }
        );
        let confirmElement = modalElement.querySelector('.action-button[data-action="confirm"]');
        confirmElement.addEventListener('click', (event) => {
          if (currentUserId != this.userId) {
            nopaque.requests.corpora.entity.files.ent.delete(this.corpusId, itemId)
            .then(() => {
              window.location.reload();
            });
          } else {
            nopaque.requests.corpora.entity.files.ent.delete(this.corpusId, itemId)
          }
        });
        modal.open();
        break;
      }
      case 'download': {
        window.location.href = `/corpora/${this.corpusId}/files/${itemId}/download`;
        break;
      }
      case 'view': {
        window.location.href = `/corpora/${this.corpusId}/files/${itemId}`;
        break;
      }
      case 'select': {
        if (event.target.checked) {
          this.selectedItemIds.add(itemId);
        } else {
          this.selectedItemIds.delete(itemId);
        }
        this.renderingItemSelection();
        break;
      }
      default: {
        break;
      }
    }
  }

  onSelectionAction(event) {
    let selectionActionElement = event.target.closest('.selection-action-trigger[data-selection-action]');
    let selectionAction = selectionActionElement.dataset.selectionAction;
    let items = this.listjs.items;
    let selectableItems = Array.from(items)
      .filter(item => item.elm)
      .map(item => item.elm.querySelector('.select-checkbox[type="checkbox"]'));

    switch (selectionAction) {
      case 'select-all': {
        let selectedIds = new Set(Array.from(items)
          .map(item => item.values().id))
        if (event.target.checked !== undefined) {
          if (event.target.checked) {
            selectableItems.forEach(selectableItem => selectableItem.checked = true);
            this.selectedItemIds = selectedIds;
          } else {
            selectableItems.forEach(checkbox => checkbox.checked = false);
            this.selectedItemIds = new Set([...this.selectedItemIds].filter(id => !selectedIds.has(id)));
          }
          this.renderingItemSelection();
        }
        break;
      }
      case 'delete': {
        let modalElement = nopaque.Utils.HTMLToElement(
          `
            <div class="modal">
              <div class="modal-content">
                <h4>Confirm Corpus File deletion</h4>
                <p>Do you really want to delete the Corpus Files?</p>
                  <ul id="selected-items-list"></ul>
                <p>All files will be permanently deleted!</p>
              </div>
              <div class="modal-footer">
                <a class="btn modal-close waves-effect waves-light">Cancel</a>
                <a class="action-button btn modal-close red waves-effect waves-light" data-action="confirm">Delete</a>
              </div>
            </div>
          `
        );
        document.querySelector('#modals').appendChild(modalElement);
        let itemList = document.querySelector('#selected-items-list');
        this.selectedItemIds.forEach(selectedItemId => {
          let listItem = this.listjs.get('id', selectedItemId)[0].elm;
          let values = this.listjs.get('id', listItem.dataset.id)[0].values();
          let itemElement = nopaque.Utils.HTMLToElement(`<li> - ${values.title}</li>`);
          itemList.appendChild(itemElement);
        });
        let modal = M.Modal.init(
          modalElement,
          {
            dismissible: false,
            onCloseEnd: () => {
              modal.destroy();
              modalElement.remove();
            }
          }
        );
        let confirmElement = modalElement.querySelector('.action-button[data-action="confirm"]');
        confirmElement.addEventListener('click', (event) => {
          this.selectedItemIds.forEach(selectedItemId => {
            if (currentUserId != this.userId) {
              nopaque.requests.corpora.entity.files.ent.delete(this.corpusId, selectedItemId)
              .then(() => {
                window.location.reload();
              });
            } else {
              nopaque.requests.corpora.entity.files.ent.delete(this.corpusId, selectedItemId);
            }
          });
          this.selectedItemIds.clear();
          this.renderingItemSelection();
        });
        modal.open();
        break;
      }
      case 'download': {
        this.selectedItemIds.forEach(selectedItemId => {
          let downloadLink = document.createElement('a');
          downloadLink.href = `/corpora/${this.corpusId}/files/${selectedItemId}/download`;
          downloadLink.download = '';
          downloadLink.click();
        });
        selectableItems.forEach(checkbox => checkbox.checked = false);
        this.selectedItemIds.clear();
        this.renderingItemSelection();
        break;
      }
      default: {
        break;
      }
    }
  }

  renderingItemSelection() {
    let selectionActionButtons;
    if (this.hasPermissionManageFiles) {
      selectionActionButtons = document.querySelectorAll('.selection-action-trigger:not([data-selection-action="select-all"])');
    } else if (this.hasPermissionView) {
      selectionActionButtons = document.querySelectorAll('.selection-action-trigger:not([data-selection-action="select-all"]):not([data-selection-action="delete"])');
    }
    let selectableItems = this.listjs.items;
    let actionButtons = [];

    Object.values(selectableItems).forEach(selectableItem => {
      if (selectableItem.elm) {
        let checkbox = selectableItem.elm.querySelector('.select-checkbox[type="checkbox"]');
        if (checkbox.checked) {
          selectableItem.elm.classList.add('grey', 'lighten-3');
        } else {
          selectableItem.elm.classList.remove('grey', 'lighten-3');
        }
        let itemActionButtons = [];
        if (this.hasPermissionManageFiles) {
          itemActionButtons = selectableItem.elm.querySelectorAll('.list-action-trigger:not([data-list-action="select"])');
        } else if (this.hasPermissionView) {
          itemActionButtons = selectableItem.elm.querySelectorAll('.list-action-trigger:not([data-list-action="select"]):not([data-list-action="delete"]):not([data-list-action="view"])');
        }
        itemActionButtons.forEach(itemActionButton => {
          actionButtons.push(itemActionButton);
        });
      }
    });
    // Hide item action buttons if > 0 item is selected and show selection action buttons
    if (this.selectedItemIds.size > 0) {
      selectionActionButtons.forEach(selectionActionButton => {
        selectionActionButton.classList.remove('hide');
      });
      actionButtons.forEach(actionButton => {
        actionButton.classList.add('hide');
      });
    } else {
      selectionActionButtons.forEach(selectionActionButton => {
        selectionActionButton.classList.add('hide');
      });
      actionButtons.forEach(actionButton => {
        actionButton.classList.remove('hide');
      });
    }

    // Check select all checkbox if all items are selected
    let selectAllCheckbox = document.querySelector('.select-all-checkbox[type="checkbox"]');
    if (selectableItems.length === this.selectedItemIds.size && selectAllCheckbox.checked === false) {
      selectAllCheckbox.checked = true;
    } else if (selectableItems.length !== this.selectedItemIds.size && selectAllCheckbox.checked === true) {
      selectAllCheckbox.checked = false;
    }
  }

  onPatch(patch) {
    let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/files/([A-Za-z0-9]*)`);
    let filteredPatch = patch.filter(operation => re.test(operation.path));
    for (let operation of filteredPatch) {
      switch(operation.op) {
        case 'add': {
          let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/files/([A-Za-z0-9]*)$`);
          if (re.test(operation.path)) {this.add(operation.value);}
          break;
        }
        case 'remove': {
          let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/files/([A-Za-z0-9]*)$`);
          if (re.test(operation.path)) {
            let [match, corpusFileId] = operation.path.match(re);
            this.remove(corpusFileId);
          }
          break;
        }
        case 'replace': {
          let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/files/([A-Za-z0-9]*)/(author|filename|publishing_year|title)$`);
          if (re.test(operation.path)) {
            let [match, corpusFileId, valueName] = operation.path.match(re);
            this.replace(corpusFileId, valueName.replace('_', '-'), operation.value);
          }
          break;
        }
        default: {
          break;
        }
      }
    }
  }
};
