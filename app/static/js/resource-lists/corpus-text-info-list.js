nopaque.resource_lists.CorpusTextInfoList = class CorpusTextInfoList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'corpus-text-info-list';

  static defaultOptions = {
    page: 5
  };

  constructor(listContainerElement, options = {}) {
    let _options = nopaque.Utils.mergeObjectsDeep(
      nopaque.resource_lists.CorpusTextInfoList.defaultOptions,
      options
    );
    super(listContainerElement, _options);
    this.isInitialized = false;
    let sortElements = this.listContainerElement.querySelectorAll('.sort');
    sortElements.forEach((sortElement) => {
      sortElement.addEventListener('click', (event) => {this.renderSortElement(sortElement)});
    });
  }

  get item() {
    return (values) => {
      return `
        <tr class="list-item hoverable">
          <td><span class="title"></span> (<span class="publishing_year"></span>)</td>
          <td><span class="num_tokens"></span></td>
          <td><span class="num_sentences"></span></td>
          <td><span class="num_unique_words"></span></td>
          <td><span class="num_unique_lemmas"></span></td>
          <td><span class="num_unique_pos"></span></td>
          <td><span class="num_unique_simple_pos"></span></td>
        </tr>
      `.trim();
    }
  }

  get valueNames() {
    return [
      'title',
      'publishing_year',
      'num_tokens',
      'num_sentences',
      'num_unique_words',
      'num_unique_lemmas',
      'num_unique_pos',
      'num_unique_simple_pos'
    ];
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-file-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search corpus file</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>Text<span class="sort right material-icons" data-sort="title" style="cursor:pointer; color:#aa9cc9">arrow_drop_down</span></th>
            <th>Tokens<span class="sort right material-icons" data-sort="num_tokens" style="cursor:pointer">arrow_drop_down</span></th>
            <th>Sentences<span class="sort right material-icons" data-sort="num_sentences" style="cursor:pointer">arrow_drop_down</span></th>
            <th>Unique words<span class="sort right material-icons" data-sort="num_unique_words" style="cursor:pointer">arrow_drop_down</span></th>
            <th>Unique lemmas<span class="sort right material-icons" data-sort="num_unique_lemmas" style="cursor:pointer">arrow_drop_down</span></th>
            <th>Unique pos<span class="sort right material-icons" data-sort="num_unique_pos" style="cursor:pointer">arrow_drop_down</span></th>
            <th>Unique simple pos<span class="sort right material-icons" data-sort="num_unique_simple_pos" style="cursor:pointer">arrow_drop_down</span></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  mapResourceToValue(corpusTextData) {
    return {
      title: corpusTextData.title,
      publishing_year: corpusTextData.publishing_year,
      num_tokens: corpusTextData.num_tokens,
      num_sentences: corpusTextData.num_sentences,
      num_unique_words: corpusTextData.num_unique_words,
      num_unique_lemmas: corpusTextData.num_unique_lemmas,
      num_unique_pos: corpusTextData.num_unique_pos,
      num_unique_simple_pos: corpusTextData.num_unique_simple_pos
    };
  }

  sort() {
    this.listjs.sort('title');
  }

  renderSortElement(clickedSortElement) {
    this.listContainerElement.querySelectorAll('.sort').forEach((sortElement) => {
      if (sortElement !== clickedSortElement) {
        sortElement.classList.remove('asc', 'desc');
        sortElement.style.color = 'black';
        sortElement.innerHTML = 'arrow_drop_down';
      };
    });
    clickedSortElement.style.color = '#aa9cc9';
    clickedSortElement.innerHTML = clickedSortElement.classList.contains('asc') ? 'arrow_drop_down' : 'arrow_drop_up';
  }
};
