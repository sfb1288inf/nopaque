nopaque.resource_lists.PublicCorpusList = class PublicCorpusList extends nopaque.resource_lists.ResourceList {
  static htmlClass = 'public-corpus-list';

  constructor(listContainerElement, options = {}) {
    super(listContainerElement, options);
    this.listjs.list.addEventListener('click', (event) => {this.onClick(event)});
  }

  get item() {
    return (values) => {
      return `
        <tr class="list-item clickable hoverable">
          <td><b class="title"></b><br><i class="description"></i></td>
          <td><span class="owner"></span></td>
          <td>${values['current-user-is-following'] ? '<span><i class="left material-icons">visibility</i></span>' : ''}</td>
          <td class="right-align">
            <a class="list-action-trigger btn-floating service-color darken waves-effect waves-light" data-list-action="view" data-service="corpus-analysis"><i class="material-icons">send</i></a>
          </td>
        </tr>
      `.trim();
    };
  }

  get valueNames() {
    return [
      {data: ['id']},
      {data: ['creation-date']},
      {name: 'status', attr: 'data-status'},
      'description',
      'title',
      'owner',
      'is-owner',
      'current-user-is-following'
    ];
  }

  mapResourceToValue(corpus) {
    return {
      'id': corpus.id,
      'creation-date': corpus.creation_date,
      'description': corpus.description,
      'status': corpus.status,
      'title': corpus.title,
      'owner': corpus.user.username,
      'is-owner': corpus.user.id === this.userId ? true : false,
      'current-user-is-following': Object.values(corpus.corpus_follower_associations).some(association => association.follower.id === currentUserId)
    };
  }

  initListContainerElement() {
    if (!this.listContainerElement.hasAttribute('id')) {
      this.listContainerElement.id = nopaque.Utils.generateElementId('corpus-list-');
    }
    let listSearchElementId = nopaque.Utils.generateElementId(`${this.listContainerElement.id}-search-`);
    this.listContainerElement.innerHTML = `
      <div class="input-field">
        <i class="material-icons prefix">search</i>
        <input id="${listSearchElementId}" class="search" type="text"></input>
        <label for="${listSearchElementId}">Search Corpus</label>
      </div>
      <table>
        <thead>
          <tr>
            <th>Title and Description</th>
            <th>Owner</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody class="list"></tbody>
      </table>
      <ul class="pagination"></ul>
    `.trim();
  }

  onClick(event) {
    let listItemElement = event.target.closest('.list-item[data-id]');
    if (listItemElement === null) {return;}
    let itemId = listItemElement.dataset.id;
    let listActionElement = event.target.closest('.list-action-trigger[data-list-action]');
    let listAction = listActionElement === null ? 'view' : listActionElement.dataset.listAction;
    switch (listAction) {
      case 'view': {
        window.location.href = `/corpora/${itemId}`;
        break;
      }
      default: {
        break;
      }
    }
  }
};
