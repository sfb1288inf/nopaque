nopaque.forms.CreateCorpusFileForm = class CreateCorpusFileForm extends nopaque.forms.BaseForm {
  static htmlClass = 'create-corpus-file-form';

  constructor(formElement) {
    super(formElement);

    this.addEventListener('requestLoad', (event) => {
      if (event.target.status === 201) {
        window.location.href = event.target.getResponseHeader('Location');
      }
    });
  }
};
