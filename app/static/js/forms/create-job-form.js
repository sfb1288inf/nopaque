nopaque.forms.CreateJobForm = class CreateJobForm extends nopaque.forms.BaseForm {
  static htmlClass = 'create-job-form';

  constructor(formElement) {
    super(formElement);

    let versionField = this.formElement.querySelector('#create-job-form-version');
    versionField.addEventListener('change', (event) => {
      let url = new URL(window.location.href);
      url.search = `?version=${event.target.value}`;
      window.location.href = url.toString();
    });

    this.addEventListener('requestLoad', (event) => {
      if (event.target.status === 201) {
        window.location.href = event.target.getResponseHeader('Location');
      }
    });
  }
};
