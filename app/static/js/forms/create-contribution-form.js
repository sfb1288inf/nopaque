nopaque.forms.CreateContributionForm = class CreateContributionForm extends nopaque.forms.BaseForm {
  static htmlClass = 'create-contribution-form';

  constructor(formElement) {
    super(formElement);

    this.addEventListener('requestLoad', (event) => {
      if (event.target.status === 201) {
        window.location.href = event.target.getResponseHeader('Location');
      }
    });
  }
};
