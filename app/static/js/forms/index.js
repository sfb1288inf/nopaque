nopaque.forms = {};

nopaque.forms.AutoInit = () => {
  for (let propertyName in nopaque.forms) {
    let property = nopaque.forms[propertyName];
    // Initialize properties that are subclasses of nopaque.forms.BaseForm.
    // This does not include nopaque.forms.BaseForm itself.
    if (property.prototype instanceof nopaque.forms.BaseForm) {
      // Check if the static htmlClass property is defined.
      if (property.htmlClass === undefined) {return;}
      // Gather all HTML elements that have the `this.htmlClass` class
      // and do not have the no-autoinit class.
      let formElements = document.querySelectorAll(`.${property.htmlClass}:not(.no-autoinit)`);
      // Create an instance of this class for each form element.
      for (let formElement of formElements) {new property(formElement);}
    }
  }
};
