nopaque.corpus_analysis.query_builder.ElementReferences = class ElementReferences {
  constructor() {
    // General Elements
    this.queryInputField = document.querySelector('#corpus-analysis-concordance-query-builder-input-field');
    this.queryChipElements = [];
    this.queryElementTarget = document.querySelector('.query-element-target')
    this.editingModusOn = false;
    this.editedQueryChipElementIndex = undefined;
    this.deleteQueryButton = document.querySelector('#corpus-analysis-concordance-delete-query-button');

    // Structural Attribute Builder Elements
    this.structuralAttrModal = M.Modal.getInstance(document.querySelector('#corpus-analysis-concordance-structural-attr-modal'));
    this.englishEntTypeSelection = document.querySelector('#corpus-analysis-concordance-english-ent-type-selection');
    this.germanEntTypeSelection = document.querySelector('#corpus-analysis-concordance-german-ent-type-selection');
    
    // Token Attribute Builder Elements
    this.positionalAttrModal = M.Modal.getInstance(document.querySelector('#corpus-analysis-concordance-positional-attr-modal'));
    this.positionalAttrSelection = document.querySelector('#corpus-analysis-concordance-positional-attr-selection');
    this.tokenBuilderContent = document.querySelector('#corpus-analysis-concordance-token-builder-content');
    this.tokenQuery = document.querySelector('#corpus-analysis-concordance-token-query');
    this.tokenQueryTemplate = document.querySelector('#corpus-analysis-concordance-token-query-template');
    this.tokenSubmitButton = document.querySelector('#corpus-analysis-concordance-token-submit');
    this.noValueMessage = document.querySelector('#corpus-analysis-concordance-no-value-message');
    this.isTokenQueryInvalid = false;

    this.ignoreCaseCheckbox = document.querySelector('#corpus-analysis-concordance-ignore-case-checkbox');
  }
};
