nopaque.corpus_analysis.query_builder.StructuralAttributeBuilderFunctions = class StructuralAttributeBuilderFunctions {
  constructor(app) {
    this.app = app;
    this.elements = app.elements;

    this.structuralAttrModalEventlisteners();

    this.elements.structuralAttrModal = M.Modal.init(
      document.querySelector('#corpus-analysis-concordance-structural-attr-modal'), 
      {
        onCloseStart: () => {
          this.resetStructuralAttrModal();
        }
      }
    );
  }

  structuralAttrModalEventlisteners() {
    document.querySelectorAll('[data-structural-attr-modal-action-button]').forEach(button => {
      button.addEventListener('click', () => {
        this.actionButtonInStrucAttrModalHandler(button.dataset.structuralAttrModalActionButton);
      });
    });
    document.querySelector('.ent-type-selection-action[data-ent-type="any"]').addEventListener('click', () => {
      this.app.submitQueryChipElement('start-empty-entity', 'Entity Start', '<ent>');
      this.app.submitQueryChipElement('end-entity', 'Entity End', '</ent>', null, true);
      this.elements.structuralAttrModal.close();
    });
    document.querySelector('.ent-type-selection-action[data-ent-type="english"]').addEventListener('change', (event) => {
      this.app.submitQueryChipElement('start-entity', `Entity Type=${event.target.value}`, `<ent_type="${event.target.value}">`, null, false, true);
      if (!this.elements.editingModusOn) {
        this.app.submitQueryChipElement('end-entity', 'Entity End', '</ent_type>', null, true);
      }
      this.elements.structuralAttrModal.close();
    });
    document.querySelector('.ent-type-selection-action[data-ent-type="german"]').addEventListener('change', (event) => {
      this.app.submitQueryChipElement('start-entity', `Entity Type=${event.target.value}`, `<ent_type="${event.target.value}">`, null, false, true); 
      if (!this.elements.editingModusOn) {
        this.app.submitQueryChipElement('end-entity', 'Entity End', '</ent_type>', null, true);
      }
      this.elements.structuralAttrModal.close();
    });
  }

  resetStructuralAttrModal() {
    this.app.resetMaterializeSelection([this.elements.englishEntTypeSelection, this.elements.germanEntTypeSelection]);
    this.app.toggleClass(['entity-builder'], 'hide', 'add');
    this.toggleEditingAreaStructuralAttrModal('remove');
    this.elements.editingModusOn = false;
    this.elements.editedQueryChipElementIndex = undefined;
  }

  actionButtonInStrucAttrModalHandler(action) {
    switch (action) {
      case 'sentence':
        this.app.submitQueryChipElement('start-sentence', 'Sentence Start', '<s>');
        this.app.submitQueryChipElement('end-sentence', 'Sentence End', '</s>', null, true);
        this.elements.structuralAttrModal.close();
        break;
      case 'entity':
        this.app.toggleClass(['entity-builder'], 'hide', 'toggle');
        break;
      default:
        break;
    }
  }

  toggleEditingAreaStructuralAttrModal(action) {
    // If the user edits a query chip element, the corresponding editing area is displayed and the other areas are hidden or disabled.
    this.app.toggleClass(['sentence-button', 'entity-button', 'any-type-entity-button'], 'disabled', action);
  }

  editStartEntityChipElement(queryChipElement) {
    this.elements.structuralAttrModal.open();
    this.app.toggleClass(['entity-builder'], 'hide', 'remove');
    this.toggleEditingAreaStructuralAttrModal('add');
    let entType = queryChipElement.dataset.query.replace(/<ent_type="|">/g, '');
    let isEnglishEntType = this.elements.englishEntTypeSelection.querySelector(`option[value=${entType}]`) !== null;
    let selection = isEnglishEntType ? this.elements.englishEntTypeSelection : this.elements.germanEntTypeSelection;
    this.app.resetMaterializeSelection([selection], entType);
  }
}
