nopaque.corpus_analysis.query_builder.QueryBuilder = class QueryBuilder {
  constructor() {
    this.elements = new nopaque.corpus_analysis.query_builder.ElementReferences();

    this.addEventListenersToQueryElementTarget();
    this.addEventListenersToIncidenceModifier();
    this.addEventListenersToNAndMInputSubmit();

    this.elements.deleteQueryButton.addEventListener('click', () => {this.resetQueryInputField()});
    this.expertModeQueryBuilderSwitchHandler();

    this.extensions = {
      structuralAttributeBuilderFunctions: new nopaque.corpus_analysis.query_builder.StructuralAttributeBuilderFunctions(this),
      tokenAttributeBuilderFunctions: new nopaque.corpus_analysis.query_builder.TokenAttributeBuilderFunctions(this),
    };

    this.dropdown = M.Dropdown.init(
      document.querySelector('.dropdown-trigger[data-toggle-area="token-incidence-modifiers"]'), 
      {
        onCloseStart: () => {
          this.unselectChipElement(this.elements.queryInputField.querySelector('.chip.teal'));
        }
      }
    )
  }

  addEventListenersToQueryElementTarget() {
    this.elements.queryElementTarget.addEventListener('click', () => {
      this.elements.positionalAttrModal.open();
    });
    this.elements.queryElementTarget.addEventListener('dragstart', this.handleDragStart.bind(this, this.elements.queryElementTarget));
    this.elements.queryElementTarget.addEventListener('dragend', this.handleDragEnd);
  }

  addEventListenersToIncidenceModifier() {
    // Eventlisteners for the incidence modifiers. There are two different types of incidence modifiers: token and character incidence modifiers.
    document.querySelectorAll('.incidence-modifier-selection').forEach(button => {
      let dropdownId = button.parentNode.parentNode.id;
      if (dropdownId === 'corpus-analysis-concordance-token-incidence-modifiers-dropdown') {
        button.addEventListener('click', () => this.tokenIncidenceModifierHandler(button.dataset.token, button.innerHTML));
      } else if (dropdownId === 'corpus-analysis-concordance-character-incidence-modifiers-dropdown') {
        button.addEventListener('click', () => this.extensions.tokenAttributeBuilderFunctions.characterIncidenceModifierHandler(button));
      }
    });
  }

  addEventListenersToNAndMInputSubmit() {
    // Eventlisteners for the submit of n- and m-values of the incidence modifier modal for "exactly n" or "between n and m".
    document.querySelectorAll('.n-m-submit-button').forEach(button => {
      let modalId = button.dataset.modalId;
      if (modalId === 'corpus-analysis-concordance-exactly-n-token-modal' || modalId === 'corpus-analysis-concordance-between-nm-token-modal') {
        button.addEventListener('click', () => this.tokenNMSubmitHandler(modalId));
      } else if (modalId === 'corpus-analysis-concordance-exactly-n-character-modal' || modalId === 'corpus-analysis-concordance-between-nm-character-modal') {
        button.addEventListener('click', () => this.extensions.tokenAttributeBuilderFunctions.characterNMSubmitHandler(modalId));
      }
    });
  }

  toggleClass(elements, className, action) {
    elements.forEach(element => {
      document.querySelector(`[data-toggle-area="${element}"]`).classList[action](className);
    });
  }

  resetQueryInputField() {
    this.elements.queryInputField.innerHTML = '';
    this.addQueryElementTarget();
    this.updateChipList();
    this.queryPreviewBuilder();
  }

  addQueryElementTarget() {
    let queryElementTarget = nopaque.Utils.HTMLToElement(
      `
      <a class="query-element-target btn-floating btn-small blue-grey lighten-4 waves-effect waves-light tooltipped" style="margin-bottom:10px; margin-right:5px;" draggable="true" data-position="bottom" data-tooltip="Add an Element to your query">
        <i class="material-icons">add</i>
      </a>
      `
    );
    this.elements.queryInputField.appendChild(queryElementTarget);
    this.elements.queryElementTarget = queryElementTarget;
    this.addEventListenersToQueryElementTarget();
  }

  updateChipList() {
    this.elements.queryChipElements = this.elements.queryInputField.querySelectorAll('.query-component');
  }
  
  resetMaterializeSelection(selectionElements, value = "default") {
    selectionElements.forEach(selectionElement => {
      if (selectionElement.querySelector(`option[value=${value}]`) !== null) {
        selectionElement.querySelector(`option[value=${value}]`).selected = true;
      }
      let instance = M.FormSelect.getInstance(selectionElement);
      instance.destroy();
      M.FormSelect.init(selectionElement);
    })
  }

  submitQueryChipElement(dataType=undefined, prettyQueryText=undefined, queryText=undefined, index=null, isClosingTag=false, isEditable=false) {
    if (this.elements.editingModusOn) {
      let editedQueryChipElement = this.elements.queryChipElements[this.elements.editedQueryChipElementIndex];
      editedQueryChipElement.dataset.type = dataType;
      editedQueryChipElement.dataset.query = queryText;
      editedQueryChipElement.firstChild.textContent = prettyQueryText;
      this.updateChipList();
      this.queryPreviewBuilder();
    } else {
      this.queryChipFactory(dataType, prettyQueryText, queryText, index, isClosingTag, isEditable);
    }
  }

  queryChipFactory(dataType, prettyQueryText, queryText, index=null, isClosingTag=false, isEditable=false) {
    // Creates a new query chip element, adds Eventlisteners for selection, deletion and drag and drop and appends it to the query input field.
    queryText = nopaque.Utils.escape(queryText);
    prettyQueryText = nopaque.Utils.escape(prettyQueryText);
    let queryChipElement = nopaque.Utils.HTMLToElement(
      `
        <span class="chip query-component" data-type="${dataType}" data-query="${queryText}" draggable="true"">
          ${prettyQueryText}${isEditable ? '<i class="material-icons chip-action-button" data-chip-action="edit" style="padding-left:5px; font-size:18px; cursor:pointer;">edit</i>': ''}
          ${isClosingTag ? '' : '<i class="material-icons close chip-action-button" data-chip-action="delete">close</i>'}
        </span>
      `
    );
    this.addActionListeners(queryChipElement);
    queryChipElement.addEventListener('dragstart', this.handleDragStart.bind(this, queryChipElement));
    queryChipElement.addEventListener('dragend', this.handleDragEnd);
    // If an index is given, inserts the query chip after the given index (only relevant for Incidence Modifier) and if there is a closing tag, inserts the query chip before the closing tag.
    if (index !== null) {
      this.updateChipList();
      this.elements.queryChipElements[index].after(queryChipElement);
    } else {
      this.elements.queryInputField.insertBefore(queryChipElement, this.elements.queryElementTarget);
    }
    if (isClosingTag) {
      this.moveQueryElementTarget(queryChipElement);
    }

    this.updateChipList();
    this.queryPreviewBuilder();
  }

  moveQueryElementTarget(element) {
    this.elements.queryInputField.insertBefore(this.elements.queryElementTarget, element);
  }

  addActionListeners(queryChipElement) {
    let notQuantifiableDataTypes = ['start-sentence', 'end-sentence', 'start-entity', 'start-empty-entity', 'end-entity', 'token-incidence-modifier'];
    queryChipElement.addEventListener('click', (event) => {
      if (event.target.classList.contains('chip')) {
        if (!notQuantifiableDataTypes.includes(queryChipElement.dataset.type)) {
          this.selectChipElement(queryChipElement);
        }
      }
    });
    let chipActionButtons = queryChipElement.querySelectorAll('.chip-action-button');
    chipActionButtons.forEach(button => {
      button.addEventListener('click', (event) => {
      if (event.target.dataset.chipAction === 'delete') {
        this.deleteChipElement(queryChipElement);
      } else if (event.target.dataset.chipAction === 'edit') {
        this.editChipElement(queryChipElement);
      }
      });
    });
  }

  editChipElement(queryChipElement) {
    this.elements.editingModusOn = true;
    this.elements.editedQueryChipElementIndex = Array.from(this.elements.queryInputField.children).indexOf(queryChipElement);
    switch (queryChipElement.dataset.type) {
      case 'start-entity':
        this.extensions.structuralAttributeBuilderFunctions.editStartEntityChipElement(queryChipElement);
        break;
      case 'token':
        let queryElementsContent = this.extensions.tokenAttributeBuilderFunctions.prepareTokenQueryElementsContent(queryChipElement);
        this.extensions.tokenAttributeBuilderFunctions.editTokenChipElement(queryElementsContent);
        break;
      default:
        break;
    }
  }

  deleteChipElement(attr) {
    let elementIndex = Array.from(this.elements.queryInputField.children).indexOf(attr);
    switch (attr.dataset.type) {
      case 'start-sentence':
        this.deleteClosingTagHandler(elementIndex, 'end-sentence');
        break;
      case 'start-empty-entity':
      case 'start-entity':
        this.deleteClosingTagHandler(elementIndex, 'end-entity');
        break;
      case 'token':
        let nextElement = Array.from(this.elements.queryInputField.children)[elementIndex+1];
        if (nextElement !== undefined && nextElement.dataset.type === 'token-incidence-modifier') {
          this.deleteChipElement(nextElement);
        }
      default:
        break;
    }
    this.elements.queryInputField.removeChild(attr);
    this.updateChipList();
    this.queryPreviewBuilder();
  }

  deleteClosingTagHandler(elementIndex, closingTagType) {
    let closingTags = this.elements.queryInputField.querySelectorAll(`[data-type="${closingTagType}"]`);
    for (let i = 0; i < closingTags.length; i++) {
      let closingTag = closingTags[i];
    
      if (Array.from(this.elements.queryInputField.children).indexOf(closingTag) > elementIndex) {
        this.deleteChipElement(closingTag);
        break;
      }
    }
  }

  handleDragStart(queryChipElement) {
    // is called when a query chip is dragged. It creates a dropzone (in form of a chip) for the dragged chip and adds it to the query input field.
    let queryChips = this.elements.queryInputField.querySelectorAll('.query-component');
    if (queryChipElement.dataset.type === 'token-incidence-modifier') {
      queryChips = this.elements.queryInputField.querySelectorAll('.query-component[data-type="token"]');
    }
    setTimeout(() => {
      let targetChipElement = nopaque.Utils.HTMLToElement('<span class="chip drop-target">Drop here</span>');
      for (let element of queryChips) {
        if (element === this.elements.queryInputField.querySelectorAll('.query-component')[0]) {
          let secondTargetChipClone = targetChipElement.cloneNode(true);
          element.insertAdjacentElement('beforebegin', secondTargetChipClone);
          this.addDragDropListeners(secondTargetChipClone, queryChipElement);
        }
        if (element === queryChipElement || element.nextSibling === queryChipElement) {continue;}

        let targetChipClone = targetChipElement.cloneNode(true);
        element.insertAdjacentElement('afterend', targetChipClone);
        //TODO: Change to two different functions for drag and drop
        this.addDragDropListeners(targetChipClone, queryChipElement);
      }
    }, 0);
  }

  handleDragEnd(event) {
    // is called when a query chip is dropped. It removes the dropzones and initializes the tooltips if the dragged element is the query element target.
    if (event.target.classList.contains('query-element-target')) {
      M.Tooltip.init(event.target);
    }
    document.querySelectorAll('.drop-target').forEach(target => target.remove());
  }

  addDragDropListeners(targetChipClone, queryChipElement) {
    targetChipClone.addEventListener('dragover', (event) => {
      event.preventDefault();
    });
    targetChipClone.addEventListener('dragenter', (event) => {
      event.preventDefault();
      event.target.style.borderStyle = 'solid dotted';
    });
    targetChipClone.addEventListener('dragleave', (event) => {
      event.preventDefault();
      event.target.style.borderStyle = 'hidden';
    });
    targetChipClone.addEventListener('drop', (event) => {
      let dropzone = event.target;
      dropzone.parentElement.replaceChild(queryChipElement, dropzone);
      this.updateChipList();
      this.queryPreviewBuilder();
    });
  }

  queryPreviewBuilder() {
    // Builds the query preview in the form of pure CQL and displays it in the query preview field.
    let queryPreview = document.querySelector('#corpus-analysis-concordance-query-preview');
    let queryInputFieldContent = [];
    this.elements.queryChipElements.forEach(element => {
      let queryElement = element.dataset.query;
      if (queryElement !== undefined) {
        queryElement = nopaque.Utils.escape(queryElement);
      }
      queryInputFieldContent.push(queryElement);
    });
  
    let queryString = queryInputFieldContent.join(' ');
    let replacements = {
      ' +': '+',
      ' *': '*',
      ' ?': '?',
      ' {': '{'
    };

    for (let key in replacements) {
      queryString = queryString.replace(key, replacements[key]);
    }
    queryString += ';';
  
    queryPreview.innerHTML = queryString;
    queryPreview.parentNode.classList.toggle('hide', queryString === ';');
  }

  selectChipElement(attr) {
    if (attr.classList.contains('teal')) {
      return;
    }

    this.toggleClass(['token-incidence-modifiers'], 'disabled', 'toggle');
    attr.classList.toggle('teal');
    attr.classList.toggle('lighten-5');

    M.Dropdown.getInstance(document.querySelector('.dropdown-trigger[data-toggle-area="token-incidence-modifiers"]')).open();

  }

  unselectChipElement(attr) {
    let nModalInstance = M.Modal.getInstance(document.querySelector('#corpus-analysis-concordance-exactly-n-token-modal'));
    let nmModalInstance = M.Modal.getInstance(document.querySelector('#corpus-analysis-concordance-between-nm-token-modal'));
    if (nModalInstance.isOpen || nmModalInstance.isOpen) {
      return;
    }
    attr.classList.remove('teal', 'lighten-5');
    this.toggleClass(['token-incidence-modifiers'], 'disabled', 'add');
  }

  tokenIncidenceModifierHandler(incidenceModifier, incidenceModifierPretty, nOrNM = false) {
    // Adds a token incidence modifier to the query input field.
    let selectedChip = this.elements.queryInputField.querySelector('.chip.teal');
    let selectedChipIndex = Array.from(this.elements.queryChipElements).indexOf(selectedChip);
    if (nOrNM) {
      this.unselectChipElement(selectedChip);
    }
    this.submitQueryChipElement('token-incidence-modifier', incidenceModifierPretty, incidenceModifier, selectedChipIndex);
  }

  tokenNMSubmitHandler(modalId) {
    // Adds a token incidence modifier (exactly n or between n and m) to the query input field.
    let modal = document.querySelector(`#${modalId}`);
    let input_n = modal.querySelector('.n-m-input[data-value-type="n"]').value;
    let input_m = modal.querySelector('.n-m-input[data-value-type="m"]') || undefined;
    input_m = input_m !== undefined ? input_m.value : '';
    let input = `{${input_n}${input_m !== '' ? ',' : ''}${input_m}}`;
    let pretty_input = `between ${input_n} and ${input_m} (${input})`;
    if (input_m === '') {
     pretty_input = `exactly ${input_n} (${input})`;
    }

    let instance = M.Modal.getInstance(modal);
    instance.close();

    this.tokenIncidenceModifierHandler(input, pretty_input, true);
  }

  expertModeQueryBuilderSwitchHandler() {
    let queryBuilderDisplay = document.querySelector("#corpus-analysis-concordance-query-builder-display");
    let expertModeDisplay = document.querySelector("#corpus-analysis-concordance-expert-mode-display");
    let expertModeSwitch = document.querySelector("#corpus-analysis-concordance-expert-mode-switch");
    let submitModal = M.Modal.getInstance(document.querySelector('#corpus-analysis-concordance-switch-to-query-builder-submit-modal'));
    
    let confirmSwitchToQueryBuilderButton = document.querySelector('.switch-action[data-switch-action="confirm"]');
    confirmSwitchToQueryBuilderButton.addEventListener("click", () => {
      queryBuilderDisplay.classList.remove("hide");
      expertModeDisplay.classList.add("hide");
      this.switchToQueryBuilderParser();
    });

    expertModeSwitch.addEventListener("change", () => {
      const isChecked = expertModeSwitch.checked;
      if (isChecked) {
        queryBuilderDisplay.classList.add("hide");
        expertModeDisplay.classList.remove("hide");
        this.switchToExpertModeParser();
      } else {
        submitModal.open();
      }
    });
  }

  switchToExpertModeParser() {
    let expertModeInputField = document.querySelector('#corpus-analysis-concordance-form-query');
    expertModeInputField.value = '';
    let queryBuilderInputFieldValue = nopaque.Utils.unescape(document.querySelector('#corpus-analysis-concordance-query-preview').innerHTML.trim());
    if (queryBuilderInputFieldValue !== "" && queryBuilderInputFieldValue !== ";") {
      expertModeInputField.value = queryBuilderInputFieldValue;
    }
  }

  switchToQueryBuilderParser() {
    this.resetQueryInputField();
    let expertModeInputFieldValue = document.querySelector('#corpus-analysis-concordance-form-query').value;
    let chipElements = this.parseTextToChip(expertModeInputFieldValue);
    let editableElements = ['start-entity', 'token'];
    for (let chipElement of chipElements) {
      let isEditable = editableElements.includes(chipElement['type']);
      if (chipElement['query'] === '[]'){
        isEditable = false;
      }
      this.submitQueryChipElement(chipElement['type'], chipElement['pretty'], chipElement['query'], null, false, isEditable);
    }
  }

  parseTextToChip(query) {
    const parsingElementDict = {
      '<s>': {
        pretty: 'Sentence Start',
        type: 'start-sentence'
      },
      '<\/s>': {
        pretty: 'Sentence End',
        type: 'end-sentence'
      },
      '<ent>': {
        pretty: 'Entity Start',
        type: 'start-empty-entity'
      },
      '<ent_type="([A-Z]+)">': {
        pretty: '',
        type: 'start-entity'
      },
      '<\\\/ent(_type)?>': {
        pretty: 'Entity End',
        type: 'end-entity'
      },
      '\\[(word|lemma|pos|simple_pos)=("(?:[^"\\\\]|\\\\")*") ?(%c)? ?((\\&|\\|) ?(word|lemma|pos|simple_pos)=("(?:[^"\\\\]|\\\\")*") ?(%c)? ?)*\\]': {
        pretty: '',
        type: 'token'
      },
      '\\[\\]': {
        pretty: 'Empty Token',
        type: 'token'
      },
      '(?<!\\[) ?\\+ ?(?![^\\]]\\])': {
        pretty: ' one or more (+)',
        type: 'token-incidence-modifier'
      },
      '(?<!\\[) ?\\* ?(?![^\\]]\\])': {
        pretty: 'zero or more (*)',
        type: 'token-incidence-modifier'
      },
      '(?<!\\[) ?\\? ?(?![^\\]]\\])': {
        pretty: 'zero or one (?)',
        type: 'token-incidence-modifier'
      },
      '(?<!\\[) ?\\{[0-9]+} ?(?![^\\]]\\])': {
        pretty: '',
        type: 'token-incidence-modifier'
      },
      '(?<!\\[) ?\\{[0-9]+(,[0-9]+)?} ?(?![^\\]]\\])': {
        pretty: '',
        type: 'token-incidence-modifier'
      }
    }
  
    let chipElements = [];
    let regexPattern = Object.keys(parsingElementDict).map(pattern => `(${pattern})`).join('|');
    const regex = new RegExp(regexPattern, 'gi');
    let match;
  
    while ((match = regex.exec(query)) !== null) {
      // this is necessary to avoid infinite loops with zero-width matches
      if (match.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      let stringElement = match[0];
      for (let [pattern, chipElement] of Object.entries(parsingElementDict)) {
        const parsingRegex = new RegExp(pattern, 'gi');
        if (parsingRegex.exec(stringElement)) {
          // Creating the pretty text for the chip element
          let prettyText;
          switch (pattern) {
            case '<ent_type="([A-Z]+)">':
              prettyText = `Entity Type=${stringElement.replace(/<ent_type="|">/g, '')}`;
              break;
            case ':: ?match\\.text_[A-Za-z]+="[^"]+"':
              prettyText = stringElement.replace(/:: ?match\.text_|"|"/g, '');
              break;
            case '\\[(word|lemma|pos|simple_pos)=("(?:[^"\\\\]|\\\\")*") ?(%c)? ?((\\&|\\|) ?(word|lemma|pos|simple_pos)=("(?:[^"\\\\]|\\\\")*") ?(%c)? ?)*\\]':
              prettyText = stringElement.replace(/^\[|\]$|(?<!\\)"/g, '');
              prettyText = prettyText.replace(/\&/g, ' and ').replace(/\|/g, ' or ');
              break;
            case '(?<!\\[) ?\\{[0-9]+} ?(?![^\\]]\\])':
              prettyText = `exactly ${stringElement.replace(/{|}/g, '')} (${stringElement})`;
              break;
            case '(?<!\\[) ?\\{[0-9]+(,[0-9]+)?} ?(?![^\\]]\\])':
              prettyText = `between${stringElement.replace(/{|}/g, ' ').replace(',', ' and ')}(${stringElement})`;
              break;
            default:
              prettyText = chipElement.pretty;
              break;
          }
          chipElements.push({
            type: chipElement.type,
            pretty: prettyText,
            query: stringElement
          });
          break;
        }
      }
    }
  
    return chipElements;
  }
};
