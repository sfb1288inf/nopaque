nopaque.corpus_analysis.cqi.Client = class Client {
  /**
   * @param {string} host
   * @param {number} [timeout=60] timeout
   * @param {string} [version=0.1] version
   */
  constructor(host, timeout = 60, version = '0.1') {
     /** @type {nopaque.corpus_analysis.cqi.api.Client} */
    this.api = new nopaque.corpus_analysis.cqi.api.Client(host, timeout, version);
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.models.corpora.CorpusCollection}
   */
  get corpora() {
    return new nopaque.corpus_analysis.cqi.models.corpora.CorpusCollection(this);
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusByeOk>}
   */
  async bye() {
    return await this.api.ctrl_bye();
  }

  /**
   * @param {string} username
   * @param {string} password
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusConnectOk>}
   */
  async connect(username, password) {
    return await this.api.ctrl_connect(username, password);
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusPingOk>}
   */
  async ping() {
    return await this.api.ctrl_ping();
  }

  /**
   * @returns {Promise<null>}
   */
  async userAbort() {
    return await this.api.ctrl_user_abort();
  }

  /**
   * Alias for "bye" method
   * 
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusByeOk>}
   */
  async disconnect() {
    return await this.api.ctrl_bye();
  }
};
