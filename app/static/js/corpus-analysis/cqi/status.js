nopaque.corpus_analysis.cqi.status = {};


/**
 * A base class from which all other status inherit.
 */
nopaque.corpus_analysis.cqi.status.CQiStatus = class CQiStatus {
  constructor() {
    this.code = undefined;
  }
};


nopaque.corpus_analysis.cqi.status.StatusOk = class StatusOk extends nopaque.corpus_analysis.cqi.status.CQiStatus {
  constructor() {
    super();
    this.code = 257;
  }
};


nopaque.corpus_analysis.cqi.status.StatusConnectOk = class StatusConnectOk extends nopaque.corpus_analysis.cqi.status.CQiStatus {
  constructor() {
    super();
    this.code = 258;
  }
};


nopaque.corpus_analysis.cqi.status.StatusByeOk = class StatusByeOk extends nopaque.corpus_analysis.cqi.status.CQiStatus {
  constructor() {
    super();
    this.code = 259;
  }
};


nopaque.corpus_analysis.cqi.status.StatusPingOk = class StatusPingOk extends nopaque.corpus_analysis.cqi.status.CQiStatus {
  constructor() {
    super();
    this.code = 260;
  }
};


nopaque.corpus_analysis.cqi.status.lookup = {
  257: nopaque.corpus_analysis.cqi.status.StatusOk,
  258: nopaque.corpus_analysis.cqi.status.StatusConnectOk,
  259: nopaque.corpus_analysis.cqi.status.StatusByeOk,
  260: nopaque.corpus_analysis.cqi.status.StatusPingOk
};
