nopaque.corpus_analysis.cqi.errors = {};


/**
 * A base class from which all other errors inherit.
 * If you want to catch all errors that the CQi package might throw,
 * catch this base error.
 */
nopaque.corpus_analysis.cqi.errors.CQiError = class CQiError extends Error {
  constructor(message) {
    super(message);
    this.code = undefined;
    this.description = undefined;
  }
};


nopaque.corpus_analysis.cqi.errors.Error = class Error extends nopaque.corpus_analysis.cqi.errors.CQiError {
  constructor(message) {
    super(message);
    this.code = 2;
  }
};


nopaque.corpus_analysis.cqi.errors.ErrorGeneralError = class ErrorGeneralError extends nopaque.corpus_analysis.cqi.errors.Error {
  constructor(message) {
    super(message);
    this.code = 513;
  }
};


nopaque.corpus_analysis.cqi.errors.ErrorConnectRefused = class ErrorConnectRefused extends nopaque.corpus_analysis.cqi.errors.Error {
  constructor(message) {
    super(message);
    this.code = 514;
  }
};


nopaque.corpus_analysis.cqi.errors.ErrorUserAbort = class ErrorUserAbort extends nopaque.corpus_analysis.cqi.errors.Error {
  constructor(message) {
    super(message);
    this.code = 515;
  }
};


nopaque.corpus_analysis.cqi.errors.ErrorSyntaxError = class ErrorSyntaxError extends nopaque.corpus_analysis.cqi.errors.Error {
  constructor(message) {
    super(message);
    this.code = 516;
  }
};


nopaque.corpus_analysis.cqi.errors.CLError = class Error extends nopaque.corpus_analysis.cqi.errors.CQiError {
  constructor(message) {
    super(message);
    this.code = 4;
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorNoSuchAttribute = class CLErrorNoSuchAttribute extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1025;
    this.description = "CQi server couldn't open attribute";
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorWrongAttributeType = class CLErrorWrongAttributeType extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1026;
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorOutOfRange = class CLErrorOutOfRange extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1027;
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorRegex = class CLErrorRegex extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1028;
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorCorpusAccess = class CLErrorCorpusAccess extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1029;
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorOutOfMemory = class CLErrorOutOfMemory extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1030;
    this.description = 'CQi server has run out of memory; try discarding some other corpora and/or subcorpora';
  }
};


nopaque.corpus_analysis.cqi.errors.CLErrorInternal = class CLErrorInternal extends nopaque.corpus_analysis.cqi.errors.CLError {
  constructor(message) {
    super(message);
    this.code = 1031;
    this.description = "The classical 'please contact technical support' error";
  }
};


nopaque.corpus_analysis.cqi.errors.CQPError = class Error extends nopaque.corpus_analysis.cqi.errors.CQiError {
  constructor(message) {
    super(message);
    this.code = 5;
  }
};


nopaque.corpus_analysis.cqi.errors.CQPErrorGeneral = class CQPErrorGeneral extends nopaque.corpus_analysis.cqi.errors.CQPError {
  constructor(message) {
    super(message);
    this.code = 1281;
  }
};


nopaque.corpus_analysis.cqi.errors.CQPErrorNoSuchCorpus = class CQPErrorNoSuchCorpus extends nopaque.corpus_analysis.cqi.errors.CQPError {
  constructor(message) {
    super(message);
    this.code = 1282;
  }
};


nopaque.corpus_analysis.cqi.errors.CQPErrorInvalidField = class CQPErrorInvalidField extends nopaque.corpus_analysis.cqi.errors.CQPError {
  constructor(message) {
    super(message);
    this.code = 1283;
  }
};


nopaque.corpus_analysis.cqi.errors.CQPErrorOutOfRange = class CQPErrorOutOfRange extends nopaque.corpus_analysis.cqi.errors.CQPError {
  constructor(message) {
    super(message);
    this.code = 1284;
    this.description = 'A number is out of range';
  }
};


nopaque.corpus_analysis.cqi.errors.lookup = {
  2: nopaque.corpus_analysis.cqi.errors.Error,
  513: nopaque.corpus_analysis.cqi.errors.ErrorGeneralError,
  514: nopaque.corpus_analysis.cqi.errors.ErrorConnectRefused,
  515: nopaque.corpus_analysis.cqi.errors.ErrorUserAbort,
  516: nopaque.corpus_analysis.cqi.errors.ErrorSyntaxError,
  4: nopaque.corpus_analysis.cqi.errors.CLError,
  1025: nopaque.corpus_analysis.cqi.errors.CLErrorNoSuchAttribute,
  1026: nopaque.corpus_analysis.cqi.errors.CLErrorWrongAttributeType,
  1027: nopaque.corpus_analysis.cqi.errors.CLErrorOutOfRange,
  1028: nopaque.corpus_analysis.cqi.errors.CLErrorRegex,
  1029: nopaque.corpus_analysis.cqi.errors.CLErrorCorpusAccess,
  1030: nopaque.corpus_analysis.cqi.errors.CLErrorOutOfMemory,
  1031: nopaque.corpus_analysis.cqi.errors.CLErrorInternal,
  5: nopaque.corpus_analysis.cqi.errors.CQPError,
  1281: nopaque.corpus_analysis.cqi.errors.CQPErrorGeneral,
  1282: nopaque.corpus_analysis.cqi.errors.CQPErrorNoSuchCorpus,
  1283: nopaque.corpus_analysis.cqi.errors.CQPErrorInvalidField,
  1284: nopaque.corpus_analysis.cqi.errors.CQPErrorOutOfRange
};
