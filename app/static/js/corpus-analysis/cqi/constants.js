nopaque.corpus_analysis.cqi.constants = {};

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_KEYWORD = 9;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_MATCH = 16;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_MATCHEND = 17;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET = 0;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_0 = 0;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_1 = 1;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_2 = 2;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_3 = 3;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_4 = 4;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_5 = 5;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_6 = 6;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_7 = 7;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_8 = 8;

/** @type {number} */
nopaque.corpus_analysis.cqi.constants.FIELD_TARGET_9 = 9;
