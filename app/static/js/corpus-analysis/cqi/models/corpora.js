nopaque.corpus_analysis.cqi.models.corpora = {};


nopaque.corpus_analysis.cqi.models.corpora.Corpus = class Corpus extends nopaque.corpus_analysis.cqi.models.resource.Model {
  /**
   * @returns {string}
   */
  get apiName() {
    return this.attrs.api_name;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.attrs.name;
  }

  /**
   * @returns {number}
   */
  get size() {
    return this.attrs.size;
  }

  /**
   * @returns {string}
   */
  get charset() {
    return this.attrs.charset;
  }

  /**
   * @returns {string[]}
   */
  get properties() {
    return this.attrs?.properties;
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttributeCollection}
   */
  get alignmentAttributes() {
    return new nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttributeCollection(this.client, this);
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.models.attributes.PositionalAttributeCollection}
   */
  get positionalAttributes() {
    return new nopaque.corpus_analysis.cqi.models.attributes.PositionalAttributeCollection(this.client, this);
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.models.attributes.StructuralAttributeCollection}
   */
  get structuralAttributes() {
    return new nopaque.corpus_analysis.cqi.models.attributes.StructuralAttributeCollection(this.client, this);
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.models.subcorpora.SubcorpusCollection}
   */
  get subcorpora() {
    return new nopaque.corpus_analysis.cqi.models.subcorpora.SubcorpusCollection(this.client, this);
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusOk>}
   */
  async drop() {
    return await this.client.api.corpus_drop_corpus(this.apiName);
  }

  /**
   * @param {string} subcorpusName
   * @param {string} query
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusOk>}
   */
  async query(subcorpusName, query) {
    return await this.client.api.cqp_query(this.apiName, subcorpusName, query);
  }

  /**************************************************************************
   * NOTE: The following is not included in the CQi specification.          *
   **************************************************************************/
  /**************************************************************************
   *                      Custom additions for nopaque                      *
   **************************************************************************/

  /**
   * @returns {string}
   */
  get staticData() {
    return this.attrs.static_data;
  }

  /**
   * @returns {nopaque.corpus_analysis.cqi.status.StatusOk}
   */
  async updateDb() {
    return await this.client.api.ext_corpus_update_db(this.apiName);
  }

  /**
   * @param {number=} page
   * @param {number=} per_page
   * @returns {Promise<object>}
   */
  async paginate(page, per_page) {
    return await this.client.api.ext_corpus_paginate_corpus(this.apiName, page, per_page);
  }
};


nopaque.corpus_analysis.cqi.models.corpora.CorpusCollection = class CorpusCollection extends nopaque.corpus_analysis.cqi.models.resource.Collection {
   /** @type {typeof nopaque.corpus_analysis.cqi.models.corpora.Corpus} */
  static model = nopaque.corpus_analysis.cqi.models.corpora.Corpus;

  /**
   * @param {string} corpusName
   * @returns {Promise<object>}
   */
  async _get(corpusName) {
    const returnValue = {
      api_name: corpusName,
      charset: await this.client.api.corpus_charset(corpusName),
      // full_name: await this.client.api.corpus_full_name(corpusName),
      // info: await this.client.api.corpus_info(corpusName),
      name: corpusName,
      properties: await this.client.api.corpus_properties(corpusName),
      size: await this.client.api.cl_attribute_size(`${corpusName}.word`)
    };

    /************************************************************************
     * NOTE: The following is not included in the CQi specification.        *
     ************************************************************************/
    /************************************************************************
     *                     Custom additions for nopaque                     *
     ************************************************************************/
    returnValue.static_data = await this.client.api.ext_corpus_static_data(corpusName);
    return returnValue;
  }

  /**
   * @param {string} corpusName
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.corpora.Corpus>}
   */
  async get(corpusName) {
    return this.prepareModel(await this._get(corpusName));
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.corpora.Corpus[]>}
   */
  async list() {
     /** @type {string[]} */
    let corpusNames = await this.client.api.corpus_list_corpora();
     /** @type {nopaque.corpus_analysis.cqi.models.corpora.Corpus[]} */
    let corpora = [];
    for (let corpusName of corpusNames) {
      corpora.push(await this.get(corpusName));
    }
    return corpora;
  }
};
