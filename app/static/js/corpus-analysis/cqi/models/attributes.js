nopaque.corpus_analysis.cqi.models.attributes = {};


nopaque.corpus_analysis.cqi.models.attributes.Attribute = class Attribute extends nopaque.corpus_analysis.cqi.models.resource.Model {
  /**
   * @returns {string}
   */
  get apiName() {
    return this.attrs.api_name;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.attrs.name;
  }

  /**
   * @returns {number}
   */
  get size() {
    return this.attrs.size;
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusOk>}
   */
  async drop() {
    return await this.client.api.cl_drop_attribute(this.apiName);
  }
};


nopaque.corpus_analysis.cqi.models.attributes.AttributeCollection = class AttributeCollection extends nopaque.corpus_analysis.cqi.models.resource.Collection {
   /** @type{typeof nopaque.corpus_analysis.cqi.models.attributes.Attribute} */
  static model = nopaque.corpus_analysis.cqi.models.attributes.Attribute;

  /**
   * @param {nopaque.corpus_analysis.cqi.Client} client
   * @param {nopaque.corpus_analysis.cqi.models.corpora.Corpus} corpus
   */
  constructor(client, corpus) {
    super(client);
     /** @type {nopaque.corpus_analysis.cqi.models.corpora.Corpus} */
    this.corpus = corpus;
  }

  /**
   * @param {string} attributeName
   * @returns {Promise<object>}
   */
  async _get(attributeName) {
     /** @type{string} */
    let apiName = `${this.corpus.apiName}.${attributeName}`;
    return {
      api_name: apiName,
      name: attributeName,
      size: await this.client.api.cl_attribute_size(apiName)
    }
  }

  /**
   * @param {string} attributeName
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.attributes.Attribute>}
   */
  async get(attributeName) {
    return this.prepareModel(await this._get(attributeName));
  }
};


nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttribute = class AlignmentAttribute extends nopaque.corpus_analysis.cqi.models.attributes.Attribute {
  /**
   * @param {number} id 
   * @returns {Promise<[number, number, number, number]>}
   */
  async cposById(id) {
    return await this.client.api.cl_alg2cpos(this.apiName, id);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<number[]>}
   */
  async idsByCpos(cposList) {
    return await this.client.api.cl_cpos2alg(this.apiName, cposList);
  }
};


nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttributeCollection = class AlignmentAttributeCollection extends nopaque.corpus_analysis.cqi.models.attributes.AttributeCollection {
   /** @type{typeof nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttribute} */
  static model = nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttribute;

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttribute[]>}
   */
  async list() {
     /** @type {string[]} */
     let alignmentAttributeNames = await this.client.api.corpus_alignment_attributes(this.corpus.apiName);
     /** @type {nopaque.corpus_analysis.cqi.models.attributes.AlignmentAttribute[]} */
    let alignmentAttributes = [];
    for (let alignmentAttributeName of alignmentAttributeNames) {
      alignmentAttributes.push(await this.get(alignmentAttributeName));
    }
    return alignmentAttributes;
  }
};


nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute = class PositionalAttribute extends nopaque.corpus_analysis.cqi.models.attributes.Attribute {
  /**
   * @returns {number}
   */
  get lexiconSize() {
    return this.attrs.lexicon_size;
  }

  /**
   * @param {number} id
   * @returns {Promise<number[]>}
   */
  async cposById(id) {
    return await this.client.api.cl_id2cpos(this.apiName, id);
  }

  /**
   * @param {number[]} idList
   * @returns {Promise<number[]>}
   */
  async cposByIds(idList) {
    return await this.client.api.cl_idlist2cpos(this.apiName, idList);
  }

  /**
   * @param {number[]} idList
   * @returns {Promise<number[]>}
   */
  async freqsByIds(idList) {
    return await this.client.api.cl_id2freq(this.apiName, idList);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<number[]>}
   */
  async idsByCpos(cposList) {
    return await this.client.api.cl_cpos2id(this.apiName, cposList);
  }

  /**
   * @param {string} regex
   * @returns {Promise<number[]>}
   */
  async idsByRegex(regex) {
    return await this.client.api.cl_regex2id(this.apiName, regex);
  }

  /**
   * @param {string[]} valueList
   * @returns {Promise<number[]>}
   */
  async idsByValues(valueList) {
    return await this.client.api.cl_str2id(this.apiName, valueList);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<string[]>}
   */
  async valuesByCpos(cposList) {
    return await this.client.api.cl_cpos2str(this.apiName, cposList);
  }

  /**
   * @param {number[]} idList
   * @returns {Promise<string[]>}
   */
  async valuesByIds(idList) {
    return await this.client.api.cl_id2str(this.apiName, idList);
  }
};


nopaque.corpus_analysis.cqi.models.attributes.PositionalAttributeCollection = class PositionalAttributeCollection extends nopaque.corpus_analysis.cqi.models.attributes.AttributeCollection {
   /** @type{typeof nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute} */
  static model = nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute;

  /**
   * @param {string} positionalAttributeName
   * @returns {Promise<object>}
   */
  async _get(positionalAttributeName) {
    let positionalAttribute = await super._get(positionalAttributeName);
    positionalAttribute.lexicon_size = await this.client.api.cl_lexicon_size(positionalAttribute.api_name);
    return positionalAttribute;
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute[]>}
   */
  async list() {
    let positionalAttributeNames = await this.client.api.corpus_positional_attributes(this.corpus.apiName);
    let positionalAttributes = [];
    for (let positionalAttributeName of positionalAttributeNames) {
      positionalAttributes.push(await this.get(positionalAttributeName));
    }
    return positionalAttributes;
  }
};


nopaque.corpus_analysis.cqi.models.attributes.StructuralAttribute = class StructuralAttribute extends nopaque.corpus_analysis.cqi.models.attributes.Attribute {
  /**
   * @returns {boolean}
   */
  get hasValues() {
    return this.attrs.has_values;
  }

  /**
   * @param {number} id
   * @returns {Promise<[number, number]>}
   */
  async cposById(id) {
    return await this.client.api.cl_struc2cpos(this.apiName, id);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<number[]>}
   */
  async idsByCpos(cposList) {
    return await this.client.api.cl_cpos2struc(this.apiName, cposList);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<number[]>}
   */
  async lboundByCpos(cposList) {
    return await this.client.api.cl_cpos2lbound(this.apiName, cposList);
  }

  /**
   * @param {number[]} cposList
   * @returns {Promise<number[]>}
   */
  async rboundByCpos(cposList) {
    return await this.client.api.cl_cpos2rbound(this.apiName, cposList);
  }

  /**
   * @param {number[]} idList
   * @returns {Promise<string[]>}
   */
  async valuesByIds(idList) {
    return await this.client.api.cl_struc2str(this.apiName, idList);
  }
};


nopaque.corpus_analysis.cqi.models.attributes.StructuralAttributeCollection = class StructuralAttributeCollection extends nopaque.corpus_analysis.cqi.models.attributes.AttributeCollection {
   /** @type{typeof nopaque.corpus_analysis.cqi.models.attributes.StructuralAttribute} */
  static model = nopaque.corpus_analysis.cqi.models.attributes.StructuralAttribute;

  /**
   * @param {string} structuralAttributeName
   * @returns {Promise<object>}
   */
  async _get(structuralAttributeName) {
    let structuralAttribute = await super._get(structuralAttributeName);
    structuralAttribute.has_values = await this.client.api.cl_has_values(structuralAttribute.api_name);
    return structuralAttribute;
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.attributes.StructuralAttribute[]>}
   */
  async list() {
    let structuralAttributeNames = await this.client.api.corpus_structural_attributes(this.corpus.apiName);
    let structuralAttributes = [];
    for (let structuralAttributeName of structuralAttributeNames) {
      structuralAttributes.push(await this.get(structuralAttributeName));
    }
    return structuralAttributes;
  }
};
