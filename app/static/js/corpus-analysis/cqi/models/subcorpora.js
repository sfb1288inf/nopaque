nopaque.corpus_analysis.cqi.models.subcorpora = {};


nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus = class Subcorpus extends nopaque.corpus_analysis.cqi.models.resource.Model {
  /**
   * @returns {string}
   */
  get apiName() {
    return this.attrs.api_name;
  }

  /**
   * @returns {object}
   */
  get fields() {
    return this.attrs.fields;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.attrs.name;
  }

  /**
   * @returns {number}
   */
  get size() {
    return this.attrs.size;
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.status.StatusOk>}
   */
  async drop() {
    return await this.client.api.cqp_drop_subcorpus(this.apiName);
  }

  /**
   * @param {number} field
   * @param {number} first
   * @param {number} last
   * @returns {Promise<number[]>}
   */
  async dump(field, first, last) {
    return await this.client.api.cqp_dump_subcorpus(
      this.apiName,
      field,
      first,
      last
    );
  }

  /**
   * @param {number} cutoff
   * @param {number} field
   * @param {nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute} attribute
   * @returns {Promise<number[]>}
   */
  async fdist1(cutoff, field, attribute) {
    return await this.client.api.cqp_fdist_1(
      this.apiName,
      cutoff,
      field,
      attribute.apiName
    );
  }

  /**
   * @param {number} cutoff
   * @param {number} field1
   * @param {nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute} attribute1
   * @param {number} field2
   * @param {nopaque.corpus_analysis.cqi.models.attributes.PositionalAttribute} attribute2
   * @returns {Promise<number[]>}
   */ 
  async fdist2(cutoff, field1, attribute1, field2, attribute2) {
    return await this.client.api.cqp_fdist_2(
      this.apiName,
      cutoff,
      field1,
      attribute1.apiName,
      field2,
      attribute2.apiName
    );
  }

  /**************************************************************************
   * NOTE: The following is not included in the CQi specification.          *
   **************************************************************************/
  /**************************************************************************
   *                      Custom additions for nopaque                      *
   **************************************************************************/

  /**
   * @param {number=} context
   * @param {number=} page
   * @param {number=} perPage
   * @returns {Promise<object>}
   */
  async paginate(context, page, perPage) {
    return await this.client.api.ext_cqp_paginate_subcorpus(this.apiName, context, page, perPage);
  }

  /**
   * @param {number[]} matchIdList
   * @param {number=} context
   * @returns {Promise<object>}
   */
  async partialExport(matchIdList, context) {
    return await this.client.api.ext_cqp_partial_export_subcorpus(this.apiName, matchIdList, context);
  }

  /**
   * @param {number=} context
   * @returns {Promise<object>}
   */
  async export(context) {
    return await this.client.api.ext_cqp_export_subcorpus(this.apiName, context);
  }
};


nopaque.corpus_analysis.cqi.models.subcorpora.SubcorpusCollection = class SubcorpusCollection extends nopaque.corpus_analysis.cqi.models.resource.Collection {
   /** @type {typeof nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus} */
  static model = nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus;

  /**
   * @param {nopaque.corpus_analysis.cqi.CQiClient} client
   * @param {nopaque.corpus_analysis.cqi.models.corpora.Corpus} corpus
   */
  constructor(client, corpus) {
    super(client);
     /** @type {nopaque.corpus_analysis.cqi.models.corpora.Corpus} */
    this.corpus = corpus;
  }

  /**
   * @param {string} subcorpusName
   * @returns {Promise<object>}
   */
  async _get(subcorpusName) {
     /** @type {string} */
    let apiName = `${this.corpus.apiName}:${subcorpusName}`;
     /** @type {object} */
    let fields = {};
    if (await this.client.api.cqp_subcorpus_has_field(apiName, nopaque.corpus_analysis.cqi.constants.FIELD_MATCH)) {
      fields.match = nopaque.corpus_analysis.cqi.constants.FIELD_MATCH;
    }
    if (await this.client.api.cqp_subcorpus_has_field(apiName, nopaque.corpus_analysis.cqi.constants.FIELD_MATCHEND)) {
      fields.matchend = nopaque.corpus_analysis.cqi.constants.FIELD_MATCHEND
    }
    if (await this.client.api.cqp_subcorpus_has_field(apiName, nopaque.corpus_analysis.cqi.constants.FIELD_TARGET)) {
      fields.target = nopaque.corpus_analysis.cqi.constants.FIELD_TARGET
    }
    if (await this.client.api.cqp_subcorpus_has_field(apiName, nopaque.corpus_analysis.cqi.constants.FIELD_KEYWORD)) {
      fields.keyword = nopaque.corpus_analysis.cqi.constants.FIELD_KEYWORD
    }
    return {
      api_name: apiName,
      fields: fields,
      name: subcorpusName,
      size: await this.client.api.cqp_subcorpus_size(apiName)
    }
  }

  /**
   * @param {string} subcorpusName
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus>}
   */
  async get(subcorpusName) {
    return this.prepareModel(await this._get(subcorpusName));
  }

  /**
   * @returns {Promise<nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus[]>}
   */
  async list() {
     /** @type {string[]} */
    let subcorpusNames = await this.client.api.cqp_list_subcorpora(this.corpus.apiName);
     /** @type {nopaque.corpus_analysis.cqi.models.subcorpora.Subcorpus[]} */
    let subcorpora = [];
    for (let subcorpusName of subcorpusNames) {
      subcorpora.push(await this.get(subcorpusName));
    }
    return subcorpora;
  }
};
