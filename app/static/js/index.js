/*
 * This object functions as a global namespace for nopaque.
 * All components of nopaque should be attached to this object.
 */
var nopaque = {};
