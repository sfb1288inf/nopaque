nopaque.resource_displays = {};

nopaque.resource_displays.AutoInit = () => {
  for (let propertyName in nopaque.resource_displays) {
    let property = nopaque.resource_displays[propertyName];
    // Initialize properties that are subclasses of `nopaque.resource_displays.ResourceDisplay`.
    // This does not include `nopaque.resource_displays.ResourceDisplay` itself.
    if (property.prototype instanceof nopaque.resource_displays.ResourceDisplay) {
      // Check if the static `htmlClass` property is defined.
      if (property.htmlClass === undefined) {return;}
      // Gather all HTML elements that have the `this.htmlClass` class
      // and do not have the `no-autoinit` class.
      let displayElements = document.querySelectorAll(`.${property.htmlClass}:not(.no-autoinit)`);
      // Create an instance of this class for each display element.
      for (let displayElement of displayElements) {new property(displayElement);}
    }
  }
};
