nopaque.resource_displays.CorpusDisplay = class CorpusDisplay extends nopaque.resource_displays.ResourceDisplay {
  static htmlClass = 'corpus-display';

  constructor(displayElement) {
    super(displayElement);
    this.corpusId = displayElement.dataset.corpusId;
    this.displayElement
      .querySelector('.action-button[data-action="build-request"]')
      .addEventListener('click', (event) => {
        app.corpora.build(this.corpusId);
      });
  }

  init(user) {
    let corpus = user.corpora[this.corpusId];
    this.setCreationDate(corpus.creation_date);
    this.setDescription(corpus.description);
    this.setStatus(corpus.status);
    this.setTitle(corpus.title);
    this.setNumTokens(corpus.num_tokens);
  }

  onPatch(patch) {
    let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}`);
    let filteredPatch = patch.filter(operation => re.test(operation.path));
    for (let operation of filteredPatch) {
      switch(operation.op) {
        case 'remove': {
          let re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}$`);
          if (re.test(operation.path)) {
            window.location.href = '/dashboard#corpora';
          }
          break;
        }
        case 'replace': {
          re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/num_tokens`);
          if (re.test(operation.path)) {
            this.setNumTokens(operation.value);
            break;
          }
          re = new RegExp(`^/users/${this.userId}/corpora/${this.corpusId}/status$`);
          if (re.test(operation.path)) {
            this.setStatus(operation.value);
            break;
          }
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  async setTitle(title) {
    const corpusTitleElements = this.displayElement.querySelectorAll('.corpus-title');
    this.setElements(corpusTitleElements, title);
  }

  setNumTokens(numTokens) {
    const corpusTokenRatioElements = this.displayElement.querySelectorAll('.corpus-token-ratio');
    const maxNumTokens = 2147483647;

    this.setElements(corpusTokenRatioElements, `${numTokens}/${maxNumTokens}`);
  }

  setDescription(description) {
    this.setElements(this.displayElement.querySelectorAll('.corpus-description'), description);
  }

  async setStatus(status) {
    let elements = this.displayElement.querySelectorAll('.action-button[data-action="analyze"]');
    for (let element of elements) {
      if (['BUILT', 'STARTING_ANALYSIS_SESSION', 'RUNNING_ANALYSIS_SESSION', 'CANCELING_ANALYSIS_SESSION'].includes(status)) {
        element.classList.remove('disabled');
      } else {
        element.classList.add('disabled');
      }
    }
    elements = this.displayElement.querySelectorAll('.action-button[data-action="build-request"]');
    const user = await app.userHub.get(this.userId);
    const corpusFiles = user.corpora[this.corpusId].files;
    for (let element of elements) {
      if (['UNPREPARED', 'FAILED'].includes(status) && Object.values(corpusFiles.length > 0)) {
        element.classList.remove('disabled');
      } else {
        element.classList.add('disabled');
      }
    }
    elements = this.displayElement.querySelectorAll('.corpus-status');
    for (let element of elements) {
      element.dataset.corpusStatus = status;
    }
  }

  setCreationDate(creationDate) {
    this.setElements(
      this.displayElement.querySelectorAll('.corpus-creation-date'),
      new Date(creationDate).toLocaleString("en-US")
    );
  }
};
