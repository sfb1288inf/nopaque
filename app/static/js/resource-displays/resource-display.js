nopaque.resource_displays.ResourceDisplay = class ResourceDisplay {
  static htmlClass;

  constructor(displayElement) {
    this.displayElement = displayElement;
    this.userId = this.displayElement.dataset.userId;
    this.isInitialized = false;
    if (this.userId === undefined) {return;}
    app.userHub.addEventListener('patch', (event) => {
      if (this.isInitialized) {this.onPatch(event.detail);}
    });
    app.userHub.get(this.userId).then((user) => {
      this.init(user);
      this.isInitialized = true;
    });
  }

  init(user) {throw 'Not implemented';}

  onPatch(patch) {throw 'Not implemented';}

  setElement(element, value) {
    switch (element.tagName) {
      case 'INPUT': {
        element.value = value;
        M.updateTextFields();
        break;
      }
      default: {
        element.innerText = value;
        break;
      }
    }
  }

  setElements(elements, value) {
    for (let element of elements) {
      this.setElement(element, value);
    }
  }
};
