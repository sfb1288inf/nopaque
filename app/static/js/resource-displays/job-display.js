nopaque.resource_displays.JobDisplay = class JobDisplay extends nopaque.resource_displays.ResourceDisplay {
  static htmlClass = 'job-display';

  constructor(displayElement) {
    super(displayElement);
    this.jobId = this.displayElement.dataset.jobId;
  }

  init(user) {
    let job = user.jobs[this.jobId];
    this.setCreationDate(job.creation_date);
    this.setEndDate(job.creation_date);
    this.setDescription(job.description);
    this.setService(job.service);
    this.setServiceArgs(job.service_args);
    this.setServiceVersion(job.service_version);
    this.setStatus(job.status);
    this.setTitle(job.title);
  }

  onPatch(patch) {
    let re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}`);
    let filteredPatch = patch.filter(operation => re.test(operation.path));
    for (let operation of filteredPatch) {
      switch(operation.op) {
        case 'remove': {
          let re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}$`);
          if (re.test(operation.path)) {
            window.location.href = '/dashboard#jobs';
          }
          break;
        }
        case 'replace': {
          let re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}/end_date$`);
          if (re.test(operation.path)) {
            this.setEndDate(operation.value);
            break;
          }
          re = new RegExp(`^/users/${this.userId}/jobs/${this.jobId}/status$`);
          if (re.test(operation.path)) {
            this.setStatus(operation.value);
            break;
          }
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  setTitle(title) {
    this.setElements(this.displayElement.querySelectorAll('.job-title'), title);
  }

  setDescription(description) {
    this.setElements(this.displayElement.querySelectorAll('.job-description'), description);
  }

  setStatus(status) {
    let elements = this.displayElement.querySelectorAll('.job-status');
    for (let element of elements) {
      element.dataset.jobStatus = status;
    }
    elements = this.displayElement.querySelectorAll('.job-log-trigger');
    for (let element of elements) {
      if (['COMPLETED', 'FAILED'].includes(status)) {
        element.classList.remove('hide');
      } else {
        element.classList.add('hide');
      }
    }
    elements = this.displayElement.querySelectorAll('.action-button[data-action="get-log-request"]');
    for (let element of elements) {
      if (['COMPLETED', 'FAILED'].includes(status)) {
        element.classList.remove('disabled');
      } else {
        element.classList.add('disabled');
      }
    }
    elements = this.displayElement.querySelectorAll('.action-button[data-action="restart-request"]');
    for (let element of elements) {
      if (status === 'FAILED') {
        element.classList.remove('disabled');
      } else {
        element.classList.add('disabled');
      }
    }
  }

  setCreationDate(creationDate) {
    this.setElements(
      this.displayElement.querySelectorAll('.job-creation-date'),
      new Date(creationDate).toLocaleString('en-US')
    );
  }

  setEndDate(endDate) {
    this.setElements(
      this.displayElement.querySelectorAll('.job-end-date'),
      new Date(endDate).toLocaleString('en-US')
    );
  }

  setService(service) {
    this.setElements(this.displayElement.querySelectorAll('.job-service'), service);
  }

  setServiceArgs(serviceArgs) {
    this.setElements(
      this.displayElement.querySelectorAll('.job-service-args'),
      JSON.stringify(serviceArgs)
    );
  }

  setServiceVersion(serviceVersion) {
    this.setElements(this.displayElement.querySelectorAll('.job-service-version'), serviceVersion);
  }
};
