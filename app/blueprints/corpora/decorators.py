from flask import abort
from flask_login import current_user
from functools import wraps
from app.models import Corpus, CorpusFollowerAssociation


def corpus_follower_permission_required(*permissions):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            corpus_id = kwargs.get('corpus_id')
            corpus = Corpus.query.get_or_404(corpus_id)
            if not (corpus.user == current_user or current_user.is_administrator):
                cfa = CorpusFollowerAssociation.query.filter_by(corpus_id=corpus_id, follower_id=current_user.id).first()
                if cfa is None:
                    abort(403)
                if not all([cfa.role.has_permission(p) for p in permissions]):
                    abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def corpus_owner_or_admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        corpus_id = kwargs.get('corpus_id')
        corpus = Corpus.query.get_or_404(corpus_id)
        if not (corpus.user == current_user or current_user.is_administrator):
            abort(403)
        return f(*args, **kwargs)
    return decorated_function

