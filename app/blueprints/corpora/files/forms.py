from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import (
    StringField,
    SubmitField,
    ValidationError,
    IntegerField
)
from wtforms.validators import InputRequired, Length


class CorpusFileBaseForm(FlaskForm):
    author = StringField(
        'Author',
        validators=[InputRequired(), Length(max=255)]
    )
    publishing_year = IntegerField(
        'Publishing year',
        validators=[InputRequired()]
    )
    title = StringField(
        'Title',
        validators=[InputRequired(), Length(max=255)]
    )
    address = StringField('Adress', validators=[Length(max=255)])
    booktitle = StringField('Booktitle', validators=[Length(max=255)])
    chapter = StringField('Chapter', validators=[Length(max=255)])
    editor = StringField('Editor', validators=[Length(max=255)])
    institution = StringField('Institution', validators=[Length(max=255)])
    journal = StringField('Journal', validators=[Length(max=255)])
    pages = StringField('Pages', validators=[Length(max=255)])
    publisher = StringField('Publisher', validators=[Length(max=255)])
    school = StringField('School', validators=[Length(max=255)])
    submit = SubmitField()


class CreateCorpusFileForm(CorpusFileBaseForm):
    vrt = FileField('File', validators=[FileRequired()])

    def validate_vrt(self, field):
        if not field.data.filename.lower().endswith('.vrt'):
            raise ValidationError('VRT files only!')

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'create-corpus-file-form'
        super().__init__(*args, **kwargs)


class UpdateCorpusFileForm(CorpusFileBaseForm):
    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'update-corpus-file-form'
        super().__init__(*args, **kwargs)
