from flask import (
    abort,
    flash,
    redirect,
    render_template,
    send_from_directory,
    url_for
)
from app import db
from app.models import Corpus, CorpusFile, CorpusStatus
from ..decorators import corpus_follower_permission_required
from . import bp
from .forms import CreateCorpusFileForm, UpdateCorpusFileForm


@bp.route('/<hashid:corpus_id>/files')
def corpus_files(corpus_id):
    return redirect(url_for('.corpus', _anchor='files', corpus_id=corpus_id))


@bp.route('/<hashid:corpus_id>/files/create', methods=['GET', 'POST'])
@corpus_follower_permission_required('MANAGE_FILES')
def create_corpus_file(corpus_id):
    corpus = Corpus.query.get_or_404(corpus_id)
    form = CreateCorpusFileForm()
    if form.is_submitted():
        if not form.validate():
            response = {'errors': form.errors}
            return response, 400
        try:
            corpus_file = CorpusFile.create(
                form.vrt.data,
                address=form.address.data,
                author=form.author.data,
                booktitle=form.booktitle.data,
                chapter=form.chapter.data,
                editor=form.editor.data,
                institution=form.institution.data,
                journal=form.journal.data,
                pages=form.pages.data,
                publisher=form.publisher.data,
                publishing_year=form.publishing_year.data,
                school=form.school.data,
                title=form.title.data,
                mimetype='application/vrt+xml',
                corpus=corpus
            )
        except (AttributeError, OSError):
            abort(500)
        corpus.status = CorpusStatus.UNPREPARED
        db.session.commit()
        flash(f'Corpus File "{corpus_file.filename}" added', category='corpus')
        return '', 201, {'Location': corpus.url}
    return render_template(
        'corpora/files/create.html.j2',
        title='Add corpus file',
        form=form,
        corpus=corpus
    )


@bp.route('/<hashid:corpus_id>/files/<hashid:corpus_file_id>', methods=['GET', 'POST'])
@corpus_follower_permission_required('MANAGE_FILES')
def corpus_file(corpus_id, corpus_file_id):
    corpus_file = CorpusFile.query.filter_by(corpus_id=corpus_id, id=corpus_file_id).first_or_404()
    form = UpdateCorpusFileForm(data=corpus_file.to_json_serializeable())
    if form.validate_on_submit():
        form.populate_obj(corpus_file)
        if db.session.is_modified(corpus_file):
            corpus_file.corpus.status = CorpusStatus.UNPREPARED
            db.session.commit()
            flash(f'Corpus file "{corpus_file.filename}" updated', category='corpus')
        return redirect(corpus_file.corpus.url)
    return render_template(
        'corpora/files/corpus_file.html.j2',
        title='Edit corpus file',
        form=form,
        corpus=corpus_file.corpus,
        corpus_file=corpus_file
    )


@bp.route('/<hashid:corpus_id>/files/<hashid:corpus_file_id>/download')
@corpus_follower_permission_required('VIEW')
def download_corpus_file(corpus_id, corpus_file_id):
    corpus_file = CorpusFile.query.filter_by(corpus_id=corpus_id, id=corpus_file_id).first_or_404()
    return send_from_directory(
        corpus_file.path.parent,
        corpus_file.path.name,
        as_attachment=True,
        download_name=corpus_file.filename,
        mimetype=corpus_file.mimetype
    )
