from flask import Blueprint
from flask_login import login_required


bp = Blueprint('corpora', __name__)
bp.cli.short_help = 'Corpus commands.'


@bp.before_request
@login_required
def before_request():
    '''
    Ensures that the routes in this package can only be visited by users that
    are logged in.
    '''
    pass


from . import cli, files, followers, routes
