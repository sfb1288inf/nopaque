from flask import current_app
import shutil
from app import db
from app.models import Corpus, CorpusStatus
from . import bp


@bp.cli.command('reset')
def reset():
    ''' Reset built corpora. '''
    status = [
        CorpusStatus.QUEUED,
        CorpusStatus.BUILDING,
        CorpusStatus.BUILT,
        CorpusStatus.STARTING_ANALYSIS_SESSION,
        CorpusStatus.RUNNING_ANALYSIS_SESSION,
        CorpusStatus.CANCELING_ANALYSIS_SESSION
    ]
    for corpus in [x for x in Corpus.query.all() if x.status in status]:
        print(f'Resetting corpus {corpus}')
        corpus_cwb_dir = corpus.path / 'cwb'
        corpus_cwb_data_dir = corpus_cwb_dir / 'data'
        corpus_cwb_registry_dir = corpus_cwb_dir / 'registry'
        try:
            shutil.rmtree(corpus.path / 'cwb', ignore_errors=True)
            corpus_cwb_dir.mkdir()
            corpus_cwb_data_dir.mkdir()
            corpus_cwb_registry_dir.mkdir()
        except OSError as e:
            current_app.logger.error(e)
            raise
        corpus.status = CorpusStatus.UNPREPARED
        corpus.num_analysis_sessions = 0
    db.session.commit()
