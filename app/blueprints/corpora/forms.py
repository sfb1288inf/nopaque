from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import InputRequired, Length


class CorpusBaseForm(FlaskForm):
    description = TextAreaField(
        'Description',
        validators=[InputRequired(), Length(max=255)]
    )
    title = StringField('Title', validators=[InputRequired(), Length(max=32)])
    submit = SubmitField()


class CreateCorpusForm(CorpusBaseForm):
    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'create-corpus-form'
        super().__init__(*args, **kwargs)


class UpdateCorpusForm(CorpusBaseForm):
    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'update-corpus-form'
        super().__init__(*args, **kwargs)


class ImportCorpusForm(FlaskForm):
    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'import-corpus-form'
        super().__init__(*args, **kwargs)
