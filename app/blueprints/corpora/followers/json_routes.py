from flask import abort, flash, jsonify, make_response, request
from flask_login import current_user
from app import db
from app.decorators import content_negotiation
from app.models import (
    Corpus,
    CorpusFollowerAssociation,
    CorpusFollowerRole,
    User
)
from ..decorators import corpus_follower_permission_required
from . import bp


@bp.route('/<hashid:corpus_id>/followers', methods=['POST'])
@corpus_follower_permission_required('MANAGE_FOLLOWERS')
@content_negotiation(consumes='application/json', produces='application/json')
def create_corpus_followers(corpus_id):
    usernames = request.json
    if not (isinstance(usernames, list) or all(isinstance(u, str) for u in usernames)):
        abort(400)
    corpus = Corpus.query.get_or_404(corpus_id)
    for username in usernames:
        user = User.query.filter_by(username=username, is_public=True).first_or_404()
        user.follow_corpus(corpus)
    db.session.commit()
    response_data = {
        'message': f'Users are now following "{corpus.title}"',
        'category': 'corpus'
    }
    return response_data, 200


@bp.route('/<hashid:corpus_id>/followers/<hashid:follower_id>/role', methods=['PUT'])
@corpus_follower_permission_required('MANAGE_FOLLOWERS')
@content_negotiation(consumes='application/json', produces='application/json')
def update_corpus_follower_role(corpus_id, follower_id):
    role_name = request.json
    if not isinstance(role_name, str):
        abort(400)
    cfr = CorpusFollowerRole.query.filter_by(name=role_name).first()
    if cfr is None:
        abort(400)
    cfa = CorpusFollowerAssociation.query.filter_by(corpus_id=corpus_id, follower_id=follower_id).first_or_404()
    cfa.role = cfr
    db.session.commit()
    response_data = {
        'message': f'User "{cfa.follower.username}" is now {cfa.role.name}',
        'category': 'corpus'
    }
    return response_data, 200


@bp.route('/<hashid:corpus_id>/followers/<hashid:follower_id>', methods=['DELETE'])
def delete_corpus_follower(corpus_id, follower_id):
    cfa = CorpusFollowerAssociation.query.filter_by(corpus_id=corpus_id, follower_id=follower_id).first_or_404()
    if not (
        current_user.id == follower_id
        or current_user == cfa.corpus.user 
        or CorpusFollowerAssociation.query.filter_by(corpus_id=corpus_id, follower_id=current_user.id).first().role.has_permission('MANAGE_FOLLOWERS')
        or current_user.is_administrator):
        abort(403)
    if current_user.id == follower_id:
        flash(f'You are no longer following "{cfa.corpus.title}"', 'corpus')
        response = make_response()
        response.status_code = 204
    else:
        response_data = {
            'message': f'"{cfa.follower.username}" is not following "{cfa.corpus.title}" anymore',
            'category': 'corpus'
        }
        response = jsonify(response_data)
        response.status_code = 200
    cfa.follower.unfollow_corpus(cfa.corpus)
    db.session.commit()
    return response
