from flask import Blueprint


bp = Blueprint('api', __name__)


from .tokens import bp as tokens_blueprint
bp.register_blueprint(tokens_blueprint, url_prefix='/tokens')

from .users import bp as users_blueprint
bp.register_blueprint(users_blueprint, url_prefix='/users')

from .jobs import bp as jobs_blueprint
bp.register_blueprint(jobs_blueprint, url_prefix='/jobs')
