from flask import Blueprint
from flask_login import login_required
from pathlib import Path
import yaml


services_file = Path(__file__).parent / 'services.yml'
with services_file.open('r') as f:
    SERVICES = yaml.safe_load(f)

bp = Blueprint('services', __name__)


@bp.before_request
@login_required
def before_request():
    '''
    Ensures that the routes in this package can only be visited by users that
    are logged in.
    '''
    pass


from . import routes  # noqa
