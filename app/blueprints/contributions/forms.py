from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    SelectMultipleField,
    IntegerField
)
from wtforms.validators import InputRequired, Length


class ContributionBaseForm(FlaskForm):
    title = StringField(
        'Title',
        validators=[InputRequired(), Length(max=64)]
    )
    description = StringField(
        'Description',
        validators=[InputRequired(), Length(max=255)]
    )
    version = StringField(
        'Version',
        validators=[InputRequired(), Length(max=16)]
    )
    publisher = StringField(
        'Publisher',
        validators=[InputRequired(), Length(max=128)]
    )
    publisher_url = StringField(
        'Publisher URL',
        validators=[InputRequired(), Length(max=512)]
    )
    publishing_url = StringField(
        'Publishing URL',
        validators=[InputRequired(), Length(max=512)]
    )
    publishing_year = IntegerField(
        'Publishing year',
        validators=[InputRequired()]
    )
    compatible_service_versions = SelectMultipleField(
        'Compatible service versions'
    )
    submit = SubmitField()


class UpdateContributionBaseForm(ContributionBaseForm):
    pass
