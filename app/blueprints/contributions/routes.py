from flask import render_template
from . import bp


@bp.route('')
def index():
    return render_template('contributions/index.html.j2', title='Contributions')
