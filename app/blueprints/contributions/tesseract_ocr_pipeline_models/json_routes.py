from flask import abort, current_app, request
from flask_login import current_user
from threading import Thread
from app import db
from app.decorators import content_negotiation, permission_required
from app.models import TesseractOCRPipelineModel
from . import bp


@bp.route('/<hashid:tesseract_ocr_pipeline_model_id>', methods=['DELETE'])
@content_negotiation(produces='application/json')
def delete_tesseract_model(tesseract_ocr_pipeline_model_id):
    def _delete_tesseract_ocr_pipeline_model(app, tesseract_ocr_pipeline_model_id):
        with app.app_context():
            topm = TesseractOCRPipelineModel.query.get(tesseract_ocr_pipeline_model_id)
            topm.delete()
            db.session.commit()

    topm = TesseractOCRPipelineModel.query.get_or_404(tesseract_ocr_pipeline_model_id)
    if not (topm.user == current_user or current_user.is_administrator):
        abort(403)
    thread = Thread(
        target=_delete_tesseract_ocr_pipeline_model,
        args=(current_app._get_current_object(), topm.id)
    )
    thread.start()
    response_data = {
        'message': \
            f'Tesseract OCR Pipeline Model "{topm.title}" marked for deletion'
    }
    return response_data, 202


@bp.route('/<hashid:tesseract_ocr_pipeline_model_id>/is_public', methods=['PUT'])
@permission_required('CONTRIBUTE')
@content_negotiation(consumes='application/json', produces='application/json')
def update_tesseract_ocr_pipeline_model_is_public(tesseract_ocr_pipeline_model_id):
    is_public = request.json
    if not isinstance(is_public, bool):
        abort(400)
    topm = TesseractOCRPipelineModel.query.get_or_404(tesseract_ocr_pipeline_model_id)
    if not (topm.user == current_user or current_user.is_administrator):
        abort(403)
    topm.is_public = is_public
    db.session.commit()
    response_data = {
        'message': (
            f'Tesseract OCR Pipeline Model "{topm.title}"'
            f' is now {"public" if is_public else "private"}'
        )
    }
    return response_data, 200
