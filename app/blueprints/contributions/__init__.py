from flask import Blueprint
from flask_login import login_required


bp = Blueprint('contributions', __name__)


@bp.before_request
@login_required
def before_request():
    '''
    Ensures that the routes in this package can only be visited by users that
    are logged in.
    '''
    pass


from . import routes


from .spacy_nlp_pipeline_models import bp as spacy_nlp_pipeline_models_bp
bp.register_blueprint(spacy_nlp_pipeline_models_bp, url_prefix='/spacy-nlp-pipeline-models')

from .tesseract_ocr_pipeline_models import bp as tesseract_ocr_pipeline_models_bp
bp.register_blueprint(tesseract_ocr_pipeline_models_bp, url_prefix='/tesseract-ocr-pipeline-models')
