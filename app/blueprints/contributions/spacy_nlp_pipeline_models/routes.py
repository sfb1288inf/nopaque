from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required
from app import db
from app.models import SpaCyNLPPipelineModel
from . import bp
from .forms import (
    CreateSpaCyNLPPipelineModelForm,
    UpdateSpaCyNLPPipelineModelForm
)


@bp.route('/')
@login_required
def index():
    return redirect(url_for('contributions.index', _anchor='spacy-nlp-pipeline-models'))


@bp.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    form = CreateSpaCyNLPPipelineModelForm()
    if form.is_submitted():
        if not form.validate():
            return {'errors': form.errors}, 400
        try:
            snpm = SpaCyNLPPipelineModel.create(
                form.spacy_model_file.data,
                compatible_service_versions=form.compatible_service_versions.data,
                description=form.description.data,
                pipeline_name=form.pipeline_name.data,
                publisher=form.publisher.data,
                publisher_url=form.publisher_url.data,
                publishing_url=form.publishing_url.data,
                publishing_year=form.publishing_year.data,
                is_public=False,
                title=form.title.data,
                version=form.version.data,
                user=current_user
            )
        except OSError:
            abort(500)
        db.session.commit()
        flash(f'SpaCy NLP Pipeline model "{snpm.title}" created')
        return {}, 201, {'Location': url_for('.index')}
    return render_template(
        'contributions/spacy_nlp_pipeline_models/create.html.j2',
        title='Create SpaCy NLP Pipeline Model',
        form=form
    )


@bp.route('/<hashid:spacy_nlp_pipeline_model_id>', methods=['GET', 'POST'])
@login_required
def entity(spacy_nlp_pipeline_model_id):
    snpm = SpaCyNLPPipelineModel.query.get_or_404(spacy_nlp_pipeline_model_id)
    if not (snpm.user == current_user or current_user.is_administrator):
        abort(403)
    form = UpdateSpaCyNLPPipelineModelForm(data=snpm.to_json_serializeable())
    if form.validate_on_submit():
        form.populate_obj(snpm)
        if db.session.is_modified(snpm):
            flash(f'SpaCy NLP Pipeline model "{snpm.title}" updated')
            db.session.commit()
        return redirect(url_for('.index'))
    return render_template(
        'contributions/spacy_nlp_pipeline_models/entity.html.j2',
        title=f'{snpm.title} {snpm.version}',
        form=form,
        spacy_nlp_pipeline_model=snpm
    )
