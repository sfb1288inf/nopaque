from flask import abort, current_app, request
from flask_login import current_user, login_required
from threading import Thread
from app import db
from app.decorators import content_negotiation, permission_required
from app.models import SpaCyNLPPipelineModel
from . import bp


@bp.route('/<hashid:spacy_nlp_pipeline_model_id>', methods=['DELETE'])
@login_required
@content_negotiation(produces='application/json')
def delete_spacy_model(spacy_nlp_pipeline_model_id):
    def _delete_spacy_model(app, spacy_nlp_pipeline_model_id):
        with app.app_context():
            snpm = SpaCyNLPPipelineModel.query.get(spacy_nlp_pipeline_model_id)
            snpm.delete()
            db.session.commit()

    snpm = SpaCyNLPPipelineModel.query.get_or_404(spacy_nlp_pipeline_model_id)
    if not (snpm.user == current_user or current_user.is_administrator):
        abort(403)
    thread = Thread(
        target=_delete_spacy_model,
        args=(current_app._get_current_object(), snpm.id)
    )
    thread.start()
    response_data = {
        'message': \
            f'SpaCy NLP Pipeline Model "{snpm.title}" marked for deletion'
    }
    return response_data, 202


@bp.route('/<hashid:spacy_nlp_pipeline_model_id>/is_public', methods=['PUT'])
@permission_required('CONTRIBUTE')
@content_negotiation(consumes='application/json', produces='application/json')
def update_spacy_nlp_pipeline_model_is_public(spacy_nlp_pipeline_model_id):
    is_public = request.json
    if not isinstance(is_public, bool):
        abort(400)
    snpm = SpaCyNLPPipelineModel.query.get_or_404(spacy_nlp_pipeline_model_id)
    if not (snpm.user == current_user or current_user.is_administrator):
        abort(403)
    snpm.is_public = is_public
    db.session.commit()
    response_data = {
        'message': (
            f'SpaCy NLP Pipeline Model "{snpm.title}"'
            f' is now {"public" if is_public else "private"}'
        )
    }
    return response_data, 200
