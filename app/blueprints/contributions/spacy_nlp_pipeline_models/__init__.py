from flask import current_app, Blueprint
from flask_login import login_required


bp = Blueprint('spacy_nlp_pipeline_models', __name__)


@bp.before_request
@login_required
def before_request():
    '''
    Ensures that the routes in this package can only be visited by users that
    are logged in.
    '''
    pass


from . import routes, json_routes
