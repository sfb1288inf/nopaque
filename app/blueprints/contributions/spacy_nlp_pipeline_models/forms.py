from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, ValidationError
from wtforms.validators import InputRequired, Length
from app.blueprints.services import SERVICES
from ..forms import ContributionBaseForm, UpdateContributionBaseForm


class CreateSpaCyNLPPipelineModelForm(ContributionBaseForm):
    spacy_model_file = FileField(
        'File',
        validators=[FileRequired()]
    )
    pipeline_name = StringField(
        'Pipeline name',
        validators=[InputRequired(), Length(max=64)]
    )

    def validate_spacy_model_file(self, field):
        if not field.data.filename.lower().endswith(('.tar.gz', ('.whl'))):
            raise ValidationError('.tar.gz or .whl files only!')

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'create-spacy-nlp-pipeline-model-form'
        super().__init__(*args, **kwargs)
        service_manifest = SERVICES['spacy-nlp-pipeline']
        self.compatible_service_versions.choices = [('', 'Choose your option')]
        self.compatible_service_versions.choices += [
            (x, x) for x in service_manifest['versions'].keys()
        ]
        self.compatible_service_versions.default = ''


class UpdateSpaCyNLPPipelineModelForm(UpdateContributionBaseForm):
    pipeline_name = StringField(
        'Pipeline name',
        validators=[InputRequired(), Length(max=64)]
    )
    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'edit-spacy-nlp-pipeline-model-form'
        super().__init__(*args, **kwargs)
        service_manifest = SERVICES['spacy-nlp-pipeline']
        self.compatible_service_versions.choices = [('', 'Choose your option')]
        self.compatible_service_versions.choices += [
            (x, x) for x in service_manifest['versions'].keys()
        ]
        self.compatible_service_versions.default = ''
