from flask import jsonify, render_template, request
from werkzeug.exceptions import HTTPException
from . import bp


@bp.app_errorhandler(HTTPException)
def handle_http_exception(e: HTTPException):
    ''' Generic HTTP exception handler '''
    accept_json = request.accept_mimetypes.accept_json
    accept_html = request.accept_mimetypes.accept_html

    if accept_json and not accept_html:
        error = {
            'code': e.code,
            'name': e.name,
            'description': e.description
        }
        return jsonify(error), e.code

    return render_template('errors/error.html.j2', error=e), e.code
