from flask import abort, send_from_directory
from flask_login import current_user, login_required
from app.models import JobInput
from . import bp


@bp.route('/<hashid:job_input_id>/download')
@login_required
def download_job_input(job_id: int, job_input_id: int):
    job_input = JobInput.query.filter_by(
        job_id=job_id,
        id=job_input_id
    ).first_or_404()

    if not (
        job_input.job.user == current_user
        or current_user.is_administrator
    ):
        abort(403)

    return send_from_directory(
        job_input.path.parent,
        job_input.path.name,
        as_attachment=True,
        download_name=job_input.filename,
        mimetype=job_input.mimetype
    )
