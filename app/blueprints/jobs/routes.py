from flask import (
    abort,
    current_app,
    Flask,
    jsonify,
    redirect,
    render_template,
    url_for
)
from flask_login import current_user, login_required
from threading import Thread
from app import db
from app.decorators import admin_required
from app.models import Job, JobStatus
from . import bp


@bp.route('')
@login_required
def index():
    return redirect(url_for('main.dashboard', _anchor='jobs'))


@bp.route('/<hashid:job_id>')
@login_required
def job(job_id: int):
    job = Job.query.get_or_404(job_id)

    if not (
        job.user == current_user
        or current_user.is_administrator
    ):
        abort(403)

    return render_template(
        'jobs/job.html.j2',
        title='Job',
        job=job
    )


def _delete_job(app: Flask, job_id: int):
    with app.app_context():
        job = Job.query.get(job_id)
        job.delete()
        db.session.commit()


@bp.route('/<hashid:job_id>', methods=['DELETE'])
@login_required
def delete_job(job_id: int):
    job = Job.query.get_or_404(job_id)

    if not (
        job.user == current_user
        or current_user.is_administrator
    ):
        abort(403)

    thread = Thread(
        target=_delete_job,
        args=(current_app._get_current_object(), job.id)
    )
    thread.start()

    return jsonify(f'Job "{job.title}" marked for deletion.'), 202


@bp.route('/<hashid:job_id>/log')
@admin_required
def job_log(job_id: int):
    job = Job.query.get_or_404(job_id)

    if job.status not in [JobStatus.COMPLETED, JobStatus.FAILED]:
        abort(409)

    log_file_path = job.path / 'pipeline_data' / 'logs' / 'pyflow_log.txt'
    with log_file_path.open() as log_file:
        log = log_file.read()

    return jsonify(log)


def _restart_job(app: Flask, job_id: int):
    with app.app_context():
        job = Job.query.get(job_id)
        job.restart()
        db.session.commit()


@bp.route('/<hashid:job_id>/restart', methods=['POST'])
@login_required
def restart_job(job_id: int):
    job = Job.query.get_or_404(job_id)

    if not (
        job.user == current_user
        or current_user.is_administrator
    ):
        abort(403)

    if job.status != JobStatus.FAILED:
        abort(409)

    thread = Thread(
        target=_restart_job,
        args=(current_app._get_current_object(), job.id)
    )
    thread.start()

    return jsonify(f'Job "{job.title}" marked for restarting.'), 202
