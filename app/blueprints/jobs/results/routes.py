from flask import abort, send_from_directory
from flask_login import current_user, login_required
from app.models import JobResult
from . import bp


@bp.route('/<hashid:job_result_id>/download')
@login_required
def download_job_result(job_id: int, job_result_id: int):
    job_result = JobResult.query.filter_by(
        job_id=job_id,
        id=job_result_id
    ).first_or_404()

    if not (
        job_result.job.user == current_user
        or current_user.is_administrator
    ):
        abort(403)

    return send_from_directory(
        job_result.path.parent,
        job_result.path.name,
        as_attachment=True,
        download_name=job_result.filename,
        mimetype=job_result.mimetype
    )
