from app.models import User
from app import db
from . import bp


@bp.cli.command('reset')
def reset():
    ''' Reset terms of use accept '''
    for user in [x for x in User.query.all() if x.terms_of_use_accepted]:
        print(f'Resetting user {user.username}')
        user.terms_of_use_accepted = False
    db.session.commit()
