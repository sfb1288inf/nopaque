from flask import current_app
from flask_migrate import upgrade
from pathlib import Path
from app import db
from app.models import (
    Corpus,
    CorpusFollowerRole,
    Role,
    SpaCyNLPPipelineModel,
    TesseractOCRPipelineModel,
    User
)
from . import bp


@bp.cli.command('deploy')
def deploy():
    ''' Run deployment tasks. '''

    print('Make default directories')
    base_dir = current_app.config['NOPAQUE_DATA_DIR']
    default_dirs: list[Path] = [
        base_dir / 'tmp',
        base_dir / 'users'
    ]
    for default_dir in default_dirs:
        if not default_dir.exists():
            default_dir.mkdir()
        if not default_dir.is_dir():
            raise NotADirectoryError(f'{default_dir} is not a directory')

    print('Migrate database to latest revision')
    upgrade()

    print('Insert/Update default Roles')
    Role.insert_defaults()
    print('Insert/Update default Users')
    User.insert_defaults()
    print('Insert/Update default CorpusFollowerRoles')
    CorpusFollowerRole.insert_defaults()
    print('Insert/Update default SpaCyNLPPipelineModels')
    SpaCyNLPPipelineModel.insert_defaults()
    print('Insert/Update default TesseractOCRPipelineModels')
    TesseractOCRPipelineModel.insert_defaults()

    print('Stop running analysis sessions')
    for corpus in Corpus.query.all():
        corpus.num_analysis_sessions = 0
    db.session.commit()

    # TODO: Implement checks for if the nopaque network exists
