from flask import flash, redirect, render_template, url_for
from flask_login import current_user, login_required, login_user
from app.blueprints.auth.forms import LoginForm
from app.models import Corpus, User
from . import bp


@bp.route('/', methods=['GET', 'POST'])
def index():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter((User.email == form.user.data.lower()) | (User.username == form.user.data)).first()
        if user and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            flash('You have been logged in')
            return redirect(url_for('.dashboard'))
        flash('Invalid email/username or password', category='error')
        redirect(url_for('.index'))
    return render_template(
        'main/index.html.j2',
        title='nopaque',
        form=form
    )


@bp.route('/faq')
def faq():
    return render_template(
        'main/faq.html.j2',
        title='Frequently Asked Questions'
    )


@bp.route('/dashboard')
@login_required
def dashboard():
    return render_template(
        'main/dashboard.html.j2',
        title='Dashboard'
    )


@bp.route('/manual')
def manual():
    return render_template(
        'main/manual.html.j2',
        title='Manual'
    )


@bp.route('/news')
def news():
    return render_template(
        'main/news.html.j2',
        title='News'
    )


@bp.route('/privacy_policy')
def privacy_policy():
    return render_template(
        'main/privacy_policy.html.j2',
        title='Privacy statement (GDPR)'
    )


@bp.route('/terms_of_use')
def terms_of_use():
    return render_template(
        'main/terms_of_use.html.j2',
        title='Terms of Use'
    )


@bp.route('/social')
@login_required
def social():
    corpora = Corpus.query.filter(Corpus.is_public == True, Corpus.user != current_user).all()
    users = User.query.filter(User.is_public == True, User.id != current_user.id).all()
    return render_template(
        'main/social.html.j2',
        title='Social',
        corpora=corpora,
        users=users
    )
