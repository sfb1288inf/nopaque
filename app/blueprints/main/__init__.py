from flask import Blueprint


bp = Blueprint('main', __name__, cli_group=None)
from . import cli, routes
