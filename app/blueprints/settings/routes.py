from flask import (
    abort,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for
)
from flask_login import current_user, login_required
from app import db
from app.models import Avatar
from . import bp
from .forms import (
    UpdateAvatarForm,
    UpdatePasswordForm,
    UpdateNotificationsForm,
    UpdateAccountInformationForm,
    UpdateProfileInformationForm
)


@bp.route('', methods=['GET', 'POST'])
@login_required
def index():
    update_account_information_form = UpdateAccountInformationForm(current_user)
    update_profile_information_form = UpdateProfileInformationForm(current_user)
    update_avatar_form = UpdateAvatarForm()
    update_password_form = UpdatePasswordForm(current_user)
    update_notifications_form = UpdateNotificationsForm(current_user)

    # region handle update profile information form
    if update_profile_information_form.submit.data and update_profile_information_form.validate():
        current_user.about_me = update_profile_information_form.about_me.data
        current_user.location = update_profile_information_form.location.data
        current_user.organization = update_profile_information_form.organization.data
        current_user.website = update_profile_information_form.website.data
        current_user.full_name = update_profile_information_form.full_name.data
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('.index'))
    # endregion handle update profile information form

    # region handle update avatar form
    if update_avatar_form.submit.data and update_avatar_form.validate():
        try:
            Avatar.create(
                update_avatar_form.avatar.data,
                user=current_user
            )
        except (AttributeError, OSError):
            abort(500)
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('.index'))
    # endregion handle update avatar form

    # region handle update account information form
    if update_account_information_form.submit.data and update_account_information_form.validate():
        current_user.email = update_account_information_form.email.data
        current_user.username = update_account_information_form.username.data
        db.session.commit()
        flash('Profile settings updated')
        return redirect(url_for('.index'))
    # endregion handle update account information form

    # region handle update password form
    if update_password_form.submit.data and update_password_form.validate():
        current_user.password = update_password_form.new_password.data
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('.index'))
    # endregion handle update password form

    # region handle update notifications form
    if update_notifications_form.submit.data and update_notifications_form.validate():
        current_user.setting_job_status_mail_notification_level = \
            update_notifications_form.job_status_mail_notification_level.data
        db.session.commit()
        flash('Your changes have been saved')
        return redirect(url_for('.index'))
    # endregion handle update notifications form

    return render_template(
        'settings/index.html.j2',
        title='Settings',
        update_account_information_form=update_account_information_form,
        update_avatar_form=update_avatar_form,
        update_notifications_form=update_notifications_form,
        update_password_form=update_password_form,
        update_profile_information_form=update_profile_information_form,
        user=current_user
    )


@bp.route('/profile-is-public', methods=['PUT'])
@login_required
def update_profile_is_public():
    new_value = request.json

    if not isinstance(new_value, bool):
        abort(400)

    current_user.is_public = new_value
    db.session.commit()

    return jsonify('Your changes have been saved'), 200


@bp.route('/profile-show-email', methods=['PUT'])
@login_required
def update_profile_show_email():
    new_value = request.json

    if not isinstance(new_value, bool):
        abort(400)

    if new_value:
        current_user.add_profile_privacy_setting('SHOW_EMAIL')
    else:
        current_user.remove_profile_privacy_setting('SHOW_EMAIL')
    db.session.commit()

    return jsonify('Your changes have been saved'), 200


@bp.route('/profile-show-last-seen', methods=['PUT'])
@login_required
def update_profile_show_last_seen():
    new_value = request.json

    if not isinstance(new_value, bool):
        abort(400)

    if new_value:
        current_user.add_profile_privacy_setting('SHOW_LAST_SEEN')
    else:
        current_user.remove_profile_privacy_setting('SHOW_LAST_SEEN')
    db.session.commit()

    return jsonify('Your changes have been saved'), 200


@bp.route('/profile-show-member-since', methods=['PUT'])
@login_required
def update_profile_show_member_since():
    new_value = request.json

    if not isinstance(new_value, bool):
        abort(400)

    if new_value:
        current_user.add_profile_privacy_setting('SHOW_MEMBER_SINCE')
    else:
        current_user.remove_profile_privacy_setting('SHOW_MEMBER_SINCE')
    db.session.commit()

    return jsonify('Your changes have been saved'), 200
