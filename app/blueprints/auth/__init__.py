from flask import Blueprint, redirect, request, url_for
from flask_login import current_user
from app import db


bp = Blueprint('auth', __name__)


@bp.before_app_request
def before_request():
    if not current_user.is_authenticated:
        return

    current_user.ping()
    db.session.commit()

    if (
        not current_user.confirmed
        and request.endpoint
        and request.blueprint != 'auth'
        and request.endpoint != 'static'
    ):
        return redirect(url_for('auth.unconfirmed'))

    if not current_user.terms_of_use_accepted:
        return redirect(url_for('main.terms_of_use'))


from . import routes
