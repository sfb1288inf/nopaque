from flask_wtf import FlaskForm
from wtforms import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    ValidationError
)
from wtforms.validators import InputRequired, Email, EqualTo, Length, Regexp
from app.models import User


class RegistrationForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[InputRequired(), Email(), Length(max=254)]
    )
    username = StringField(
        'Username',
        validators=[
            InputRequired(),
            Length(max=64),
            Regexp(
                User.username_pattern,
                message=(
                    'Usernames must have only letters, numbers, dots or '
                    'underscores'
                )
            )
       ]
    )
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password_2', message='Passwords must match')
        ]
    )
    password_2 = PasswordField(
        'Password confirmation',
        validators=[
            InputRequired(),
            EqualTo('password', message='Passwords must match')
        ]
    )
    terms_of_use_accepted = BooleanField(
        'I have read and accept the terms of use',
        validators=[InputRequired()]
    )
    submit = SubmitField()

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'registration-form'
        super().__init__(*args, **kwargs)

    def validate_email(self, field):
        if User.query.filter_by(email=field.data.lower()).first():
            raise ValidationError('Email already registered')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already registered')

    def validate_terms_of_use_accepted(self, field):
        if not field.data:
            raise ValidationError('Terms of Use not accepted')


class LoginForm(FlaskForm):
    user = StringField('Email or username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField()

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'login-form'
        super().__init__(*args, **kwargs)


class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired(), Email()])
    submit = SubmitField()

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'reset-password-request-form'
        super().__init__(*args, **kwargs)


class ResetPasswordForm(FlaskForm):
    password = PasswordField(
        'New password',
        validators=[
            InputRequired(),
            EqualTo('password_2', message='Passwords must match')
        ]
    )
    password_2 = PasswordField(
        'New password confirmation',
        validators=[
            InputRequired(),
            EqualTo('password', message='Passwords must match')
        ]
    )
    submit = SubmitField()

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'reset-password-form'
        super().__init__(*args, **kwargs)
