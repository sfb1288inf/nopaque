from flask import redirect, render_template, url_for
from . import bp


@bp.route('')
def workshops():
    return redirect(url_for('main.dashboard'))


@bp.route('/fgho_sommerschule_2023')
def fgho_sommerschule_2023():
    return render_template(
        'workshops/fgho_sommerschule_2023.html.j2',
        title='FGHO Sommerschule 2023',
    )
