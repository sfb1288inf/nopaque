from flask import Blueprint


bp = Blueprint('workshops', __name__)
from . import routes
