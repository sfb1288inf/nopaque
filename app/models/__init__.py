from .anonymous_user import AnonymousUser
from .avatar import Avatar
from .corpus_file import CorpusFile
from .corpus_follower_association import CorpusFollowerAssociation
from .corpus_follower_role import CorpusFollowerPermission, CorpusFollowerRole
from .corpus import CorpusStatus, Corpus
from .job_input import JobInput
from .job_result import JobResult
from .job import JobStatus, Job
from .role import Permission, Role
from .spacy_nlp_pipeline_model import SpaCyNLPPipelineModel
from .tesseract_ocr_pipeline_model import TesseractOCRPipelineModel
from .token import Token
from .user import (
    ProfilePrivacySettings,
    UserSettingJobStatusMailNotificationLevel,
    User
)


_models = [
    Avatar,
    CorpusFile,
    CorpusFollowerAssociation,
    CorpusFollowerRole,
    Corpus,
    JobInput,
    JobResult,
    Job,
    Role,
    SpaCyNLPPipelineModel,
    TesseractOCRPipelineModel,
    Token,
    User
]


_enums = [
    CorpusFollowerPermission,
    CorpusStatus,
    JobStatus,
    Permission,
    ProfilePrivacySettings,
    UserSettingJobStatusMailNotificationLevel
]
