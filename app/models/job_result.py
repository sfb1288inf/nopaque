from flask import url_for
from flask_hashids import HashidMixin
from pathlib import Path
from app import db
from .file_mixin import FileMixin


class JobResult(FileMixin, HashidMixin, db.Model):
    __tablename__ = 'job_results'
    # Primary key
    id = db.Column(db.Integer, primary_key=True)
    # Foreign keys
    job_id = db.Column(db.Integer, db.ForeignKey('jobs.id'))
    # Fields
    description = db.Column(db.String(255))
    # Relationships
    job = db.relationship(
        'Job',
        back_populates='results'
    )

    def __repr__(self):
        return f'<JobResult {self.filename}>'

    @property
    def jsonpatch_path(self):
        return f'{self.job.jsonpatch_path}/results/{self.hashid}'

    @property
    def path(self) -> Path:
        return self.job.path / 'results' / f'{self.id}'

    @property
    def url(self):
        return url_for(
            'job_results.job_result',
            job_result_id=self.id,
            _anchor=f'job-{self.job.hashid}-result-{self.hashid}'
        )

    @property
    def user_hashid(self):
        return self.job.user.hashid

    @property
    def user_id(self):
        return self.job.user.id

    def to_json_serializeable(self, backrefs=False, relationships=False):
        json_serializeable = {
            'id': self.hashid,
            'description': self.description,
            **self.file_mixin_to_json_serializeable(
                backrefs=backrefs,
                relationships=relationships
            )
        }
        if backrefs:
            json_serializeable['job'] = \
                self.job.to_json_serializeable(backrefs=True)
        if relationships:
            pass
        return json_serializeable
