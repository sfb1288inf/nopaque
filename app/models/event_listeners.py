from datetime import datetime
from enum import Enum
from app import db, mail, socketio
from app.email import create_message
from .corpus_file import CorpusFile
from .corpus_follower_association import CorpusFollowerAssociation
from .corpus import Corpus
from .job_input import JobInput
from .job_result import JobResult
from .job import Job, JobStatus
from .spacy_nlp_pipeline_model import SpaCyNLPPipelineModel
from .tesseract_ocr_pipeline_model import TesseractOCRPipelineModel
from .user import UserSettingJobStatusMailNotificationLevel


def register_event_listeners():
    resources = [
        Corpus,
        CorpusFile,
        Job,
        JobInput,
        JobResult,
        SpaCyNLPPipelineModel,
        TesseractOCRPipelineModel
    ]

    for resource in resources:
        db.event.listen(resource, 'after_delete', resource_after_delete)
        db.event.listen(resource, 'after_insert', resource_after_insert)
        db.event.listen(resource, 'after_update', resource_after_update)

    db.event.listen(CorpusFollowerAssociation, 'after_delete', cfa_after_delete)
    db.event.listen(CorpusFollowerAssociation, 'after_insert', cfa_after_insert)

    db.event.listen(Job, 'after_update', job_after_update)


def resource_after_delete(mapper, connection, resource):
    jsonpatch = [
        {
            'op': 'remove',
            'path': resource.jsonpatch_path
        }
    ]
    room = f'/users/{resource.user_hashid}'
    socketio.emit('PATCH', jsonpatch, room=room)


def cfa_after_delete(mapper, connection, cfa):
    jsonpatch_path = f'/users/{cfa.corpus.user.hashid}/corpora/{cfa.corpus.hashid}/corpus_follower_associations/{cfa.hashid}'
    jsonpatch = [
        {
            'op': 'remove',
            'path': jsonpatch_path
        }
    ]
    room = f'/users/{cfa.corpus.user.hashid}'
    socketio.emit('PATCH', jsonpatch, room=room)


def resource_after_insert(mapper, connection, resource):
    jsonpatch_value = resource.to_json_serializeable()
    for attr in mapper.relationships:
        jsonpatch_value[attr.key] = {}
    jsonpatch = [
        {
            'op': 'add',
            'path': resource.jsonpatch_path,
            'value': jsonpatch_value
        }
    ]
    room = f'/users/{resource.user_hashid}'
    socketio.emit('PATCH', jsonpatch, room=room)


def cfa_after_insert(mapper, connection, cfa):
    jsonpatch_value = cfa.to_json_serializeable()
    jsonpatch_path = f'/users/{cfa.corpus.user.hashid}/corpora/{cfa.corpus.hashid}/corpus_follower_associations/{cfa.hashid}'
    jsonpatch = [
        {
            'op': 'add',
            'path': jsonpatch_path,
            'value': jsonpatch_value
        }
    ]
    room = f'/users/{cfa.corpus.user.hashid}'
    socketio.emit('PATCH', jsonpatch, room=room)


def resource_after_update(mapper, connection, resource):
    jsonpatch = []
    for attr in db.inspect(resource).attrs:
        if attr.key in mapper.relationships:
            continue
        if not attr.load_history().has_changes():
            continue
        jsonpatch_path = f'{resource.jsonpatch_path}/{attr.key}'
        if isinstance(attr.value, datetime):
            jsonpatch_value = f'{attr.value.isoformat()}Z'
        elif isinstance(attr.value, Enum):
            jsonpatch_value = attr.value.name
        else:
            jsonpatch_value = attr.value
        jsonpatch.append(
            {
                'op': 'replace',
                'path': jsonpatch_path,
                'value': jsonpatch_value
            }
        )
    if jsonpatch:
        room = f'/users/{resource.user_hashid}'
        socketio.emit('PATCH', jsonpatch, room=room)


def job_after_update(mapper, connection, job):
    for attr in db.inspect(job).attrs:
        if attr.key != 'status':
            continue
        if not attr.load_history().has_changes():
            return
        if job.user.setting_job_status_mail_notification_level == UserSettingJobStatusMailNotificationLevel.NONE:
            return
        if job.user.setting_job_status_mail_notification_level == UserSettingJobStatusMailNotificationLevel.END:
            if job.status not in [JobStatus.COMPLETED, JobStatus.FAILED]:
                return
        msg = create_message(
            job.user.email,
            f'Status update for your Job "{job.title}"',
            'tasks/email/notification',
            job=job
        )
        mail.send(msg)
