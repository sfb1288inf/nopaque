from flask import current_app
from flask_hashids import HashidMixin
from pathlib import Path
from app import db
from .file_mixin import FileMixin


class Avatar(HashidMixin, FileMixin, db.Model):
    __tablename__ = 'avatars'
    # Primary key
    id = db.Column(db.Integer, primary_key=True)
    # Foreign keys
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    # Relationships
    user = db.relationship('User', back_populates='avatar')

    @property
    def path(self) -> Path:
        return self.user.path / 'avatar'
        # return os.path.join(self.user.path, 'avatar')

    def delete(self):
        try:
            self.path.unlink(missing_ok=True)
        except OSError as e:
            current_app.logger.error(e)
            raise
        db.session.delete(self)

    def to_json_serializeable(self, backrefs=False, relationships=False):
        json_serializeable = {
            'id': self.hashid,
            **self.file_mixin_to_json_serializeable()
        }
        if backrefs:
            json_serializeable['user'] = \
                self.user.to_json_serializeable(backrefs=True)
        if relationships:
            pass
        return json_serializeable
