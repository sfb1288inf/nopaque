from flask import current_app, url_for
from flask_hashids import HashidMixin
from pathlib import Path
from app import db
from .corpus import CorpusStatus
from .file_mixin import FileMixin


class CorpusFile(FileMixin, HashidMixin, db.Model):
    __tablename__ = 'corpus_files'
    # Primary key
    id = db.Column(db.Integer, primary_key=True)
    # Foreign keys
    corpus_id = db.Column(db.Integer, db.ForeignKey('corpora.id'))
    # Fields
    author = db.Column(db.String(255))
    description = db.Column(db.String(255))
    publishing_year = db.Column(db.Integer)
    title = db.Column(db.String(255))
    address = db.Column(db.String(255))
    booktitle = db.Column(db.String(255))
    chapter = db.Column(db.String(255))
    editor = db.Column(db.String(255))
    institution = db.Column(db.String(255))
    journal = db.Column(db.String(255))
    pages = db.Column(db.String(255))
    publisher = db.Column(db.String(255))
    school = db.Column(db.String(255))
    # Relationships
    corpus = db.relationship(
        'Corpus',
        back_populates='files'
    )

    @property
    def download_url(self):
        return url_for(
            'corpora.download_corpus_file',
            corpus_id=self.corpus_id,
            corpus_file_id=self.id
        )

    @property
    def jsonpatch_path(self):
        return f'{self.corpus.jsonpatch_path}/files/{self.hashid}'

    @property
    def path(self) -> Path:
        return self.corpus.path / 'files' / f'{self.id}'

    @property
    def url(self):
        return url_for(
            'corpora.corpus_file',
            corpus_id=self.corpus_id,
            corpus_file_id=self.id
        )

    @property
    def user_hashid(self):
        return self.corpus.user.hashid

    @property
    def user_id(self):
        return self.corpus.user_id

    def delete(self):
        try:
            self.path.unlink(missing_ok=True)
        except OSError as e:
            current_app.logger.error(e)
            raise
        db.session.delete(self)
        self.corpus.status = CorpusStatus.UNPREPARED

    def to_json_serializeable(self, backrefs=False, relationships=False):
        json_serializeable = {
            'id': self.hashid,
            'address': self.address,
            'author': self.author,
            'description': self.description,
            'booktitle': self.booktitle,
            'chapter': self.chapter,
            'editor': self.editor,
            'institution': self.institution,
            'journal': self.journal,
            'pages': self.pages,
            'publisher': self.publisher,
            'publishing_year': self.publishing_year,
            'school': self.school,
            'title': self.title,
            **self.file_mixin_to_json_serializeable(
                backrefs=backrefs,
                relationships=relationships
            )
        }
        if backrefs:
            json_serializeable['corpus'] = \
                self.corpus.to_json_serializeable(backrefs=True)
        if relationships:
            pass
        return json_serializeable
