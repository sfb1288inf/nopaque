from flask import url_for
from flask_hashids import HashidMixin
from pathlib import Path
from app import db
from .file_mixin import FileMixin


class JobInput(FileMixin, HashidMixin, db.Model):
    __tablename__ = 'job_inputs'
    # Primary key
    id = db.Column(db.Integer, primary_key=True)
    # Foreign keys
    job_id = db.Column(db.Integer, db.ForeignKey('jobs.id'))
    # Relationships
    job = db.relationship(
        'Job',
        back_populates='inputs'
    )

    def __repr__(self):
        return f'<JobInput {self.filename}>'

    @property
    def jsonpatch_path(self):
        return f'{self.job.jsonpatch_path}/inputs/{self.hashid}'

    @property
    def path(self) -> Path:
        return self.job.path / 'inputs' / f'{self.id}'

    @property
    def url(self):
        return url_for(
            'jobs.job',
            job_input_id=self.id,
            _anchor=f'job-{self.job.hashid}-input-{self.hashid}'
        )

    @property
    def user_hashid(self):
        return self.job.user.hashid

    @property
    def user_id(self):
        return self.job.user.id

    def to_json_serializeable(self, backrefs=False, relationships=False):
        json_serializeable = {
            'id': self.hashid,
            **self.file_mixin_to_json_serializeable()
        }
        if backrefs:
            json_serializeable['job'] = \
                self.job.to_json_serializeable(backrefs=True)
        if relationships:
            pass
        return json_serializeable
