from flask import current_app, Flask, render_template
from flask_mail import Message
from threading import Thread
from app import mail


def create_message(
    recipient: str,
    subject: str,
    template: str,
    **context
) -> Message:
    message = Message(
        body=render_template(f'{template}.txt.j2', **context),
        html=render_template(f'{template}.html.j2', **context),
        recipients=[recipient],
        subject=f'[nopaque] {subject}'
    )
    return message


def send(message: Message) -> Thread:
    def _send(app: Flask, message: Message):
        with app.app_context():
            mail.send(message)

    thread = Thread(
        target=_send,
        args=[current_app._get_current_object(), message]
    )
    thread.start()
    return thread
