##############################################################################
# Environment variables used by nopaque.                                     #
##############################################################################


##############################################################################
# Flask                                                                      #
# https://flask.palletsprojects.com/en/1.1.x/config/                         #
##############################################################################
# CHOOSE ONE: False, True
# DEFAULT: False
# FLASK_DEBUG=

# CHOOSE ONE: http, https
# DEFAULT: http
# PREFERRED_URL_SCHEME=

# DEFAULT: hard to guess string
# HINT: Use this bash command `python -c "import uuid; print(uuid.uuid4().hex)"`
# SECRET_KEY=

# DEFAULT: localhost:5000
# EXAMPLES:
# - nopaque.example.com
# - nopaque.example.com:5000
# HINT: If your instance is publicly available on a different Port then 80/443,
#       you will have to add this to the server name
# SERVER_NAME=

# CHOOSE ONE: False, True
# DEFAULT: False
# HINT: Set to true if you redirect http to https
# SESSION_COOKIE_SECURE=


##############################################################################
# Flask-Assets                                                               #
# https://webassets.readthedocs.io/en/latest/                                #
##############################################################################
# CHOOSE ONE: False, True
# DEFAULT: False
# ASSETS_DEBUG=


##############################################################################
# Flask-Hashids                                                              #
# https://github.com/Pevtrick/Flask-Hashids                                  #
##############################################################################
# DEFAULT: 16
# HASHIDS_MIN_LENGTH=

# DEFAULT: hard to guess string
# HINT: Use this bash command `python -c "import uuid; print(uuid.uuid4().hex)"`
# NOTE: In production it is strongly recommended that this is NEVER the same as
#       the `SECRET_KEY`
# HASHIDS_SALT=


##############################################################################
# Flask-Login                                                                #
# https://flask-login.readthedocs.io/en/latest/                              #
##############################################################################
# CHOOSE ONE: False, True
# DEFAULT: False
# HINT: Set to true if you redirect http to https
# REMEMBER_COOKIE_SECURE=


##############################################################################
# Flask-Mail                                                                 #
# https://pythonhosted.org/Flask-Mail/                                       #
##############################################################################
# EXAMPLE: nopaque Admin <nopaque@example.com>
MAIL_DEFAULT_SENDER=

MAIL_PASSWORD=

# EXAMPLE: smtp.example.com
MAIL_SERVER=

# EXAMPLE: 587
MAIL_PORT=

# CHOOSE ONE: False, True
# DEFAULT: False
# MAIL_USE_SSL=

# CHOOSE ONE: False, True
# DEFAULT: False
# MAIL_USE_TLS=

# EXAMPLE: nopaque@example.com
MAIL_USERNAME=


##############################################################################
# Flask-SQLAlchemy                                                           #
# https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/                #
##############################################################################
# NOTES:
# - Use `.` as <nopaque-basedir>
# - Don't use a SQLite database when using Docker Compose
SQLALCHEMY_DATABASE_URI=


##############################################################################
# nopaque                                                                    #
##############################################################################
# An account is registered with this email adress gets automatically assigned
# the administrator role
# EXAMPLE: admin.nopaque@example.com
NOPAQUE_ADMIN=

# DEFAULT: /mnt/nopaque
# NOTES:
# - This must be a network share and it must be available on all
#   Docker Swarm nodes, mounted to the same path.
# - When running with Docker Compose, this gets overwritten in the
#   `docker-compose.yml` file with the value of the `HOST_NOPAQUE_DATA_PATH`
#   environment variable from the `.env` file.
# NOPAQUE_DATA_PATH=

# CHOOSE ONE: False, True
# DEFAULT: True
# NOPAQUE_IS_PRIMARY_INSTANCE=

# transport://[userid:password]@hostname[:port]/[virtual_host]
NOPAQUE_SOCKETIO_MESSAGE_QUEUE_URI=

# DEFAULT: nopaque
# NOTE: When running with Docker Compose, this gets overwritten in the
#       `docker-compose.yml` file
# NOPAQUE_DOCKER_NETWORK_NAME=

# NOTE: Get these from the nopaque development team
NOPAQUE_DOCKER_REGISTRY_USERNAME=
NOPAQUE_DOCKER_REGISTRY_PASSWORD=

# DEFAULT: %Y-%m-%d %H:%M:%S
# NOPAQUE_LOG_DATE_FORMAT=

# DEFAULT: [%(asctime)s] %(levelname)s in %(pathname)s (function: %(funcName)s, line: %(lineno)d): %(message)s
# NOPAQUE_LOG_FORMAT=

# DEFAULT: WARNING
# CHOOSE ONE: CRITICAL, ERROR, WARNING, INFO, DEBUG
# NOPAQUE_LOG_LEVEL=

# CHOOSE ONE: False, True
# DEFAULT: False
# HINT: Set this to True only if you are using a proxy in front of nopaque
# NOPAQUE_PROXY_FIX_ENABLED=

# DEFAULT: 0
# Number of values to trust for X-Forwarded-For
# NOPAQUE_PROXY_FIX_X_FOR=

# DEFAULT: 0
# Number of values to trust for X-Forwarded-Host
# NOPAQUE_PROXY_FIX_X_HOST=

# DEFAULT: 0
# Number of values to trust for X-Forwarded-Port
# NOPAQUE_PROXY_FIX_X_PORT=

# DEFAULT: 0
# Number of values to trust for X-Forwarded-Prefix
# NOPAQUE_PROXY_FIX_X_PREFIX=

# DEFAULT: 0
# Number of values to trust for X-Forwarded-Proto
# NOPAQUE_PROXY_FIX_X_PROTO=

# CHOOSE ONE: False, True
# DEFAULT: False
# NOPAQUE_TRANSKRIBUS_ENABLED=

# READ-COOP account data: https://readcoop.eu/
# NOPAQUE_READCOOP_USERNAME=
# NOPAQUE_READCOOP_PASSWORD=
