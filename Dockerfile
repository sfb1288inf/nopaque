FROM python:3.10.13-slim-bookworm


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


# Set environment variables
ENV LANG="C.UTF-8"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONUNBUFFERED="1"


# Install system dependencies
RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
      build-essential \
      gosu \
      libpq-dev \
 && rm --recursive /var/lib/apt/lists/*


# Create a non-root user
RUN useradd --create-home --no-log-init nopaque \
 && groupadd docker \
 && usermod --append --groups docker nopaque

USER nopaque
WORKDIR /home/nopaque


# Create a Python virtual environment
ENV NOPAQUE_PYTHON3_VENV_PATH="/home/nopaque/.venv"
RUN python3 -m venv "${NOPAQUE_PYTHON3_VENV_PATH}"
ENV PATH="${NOPAQUE_PYTHON3_VENV_PATH}/bin:${PATH}"


# Install Python dependencies
COPY --chown=nopaque:nopaque requirements.freezed.txt requirements.freezed.txt
RUN python3 -m pip install --requirement requirements.freezed.txt \
 && rm requirements.freezed.txt


# Install the application
COPY docker-nopaque-entrypoint.sh /usr/local/bin/
COPY --chown=nopaque:nopaque app app
COPY --chown=nopaque:nopaque migrations migrations
COPY --chown=nopaque:nopaque tests tests
COPY --chown=nopaque:nopaque boot.sh config.py wsgi.py ./


EXPOSE 5000


USER root


ENTRYPOINT ["docker-nopaque-entrypoint.sh"]
