"""Add profile pages columns to users table

Revision ID: 4820fa2e3ee2
Revises: f2656133df2f
Create Date: 2022-11-30 10:03:49.080576

"""
from alembic import op
import sqlalchemy as sa

revision = '4820fa2e3ee2'
down_revision = 'f2656133df2f'
branch_labels = None
depends_on = None

def upgrade():
    op.add_column(
        'users',
        sa.Column('full_name', sa.String(length=64), nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('about_me', sa.String(length=256), nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('location', sa.String(length=64), nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('website', sa.String(length=128), nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('organization', sa.String(length=128), nullable=True)
    )

def downgrade():
    op.drop_column('users', 'organization')
    op.drop_column('users', 'website')
    op.drop_column('users', 'location')
    op.drop_column('users', 'about_me')
    op.drop_column('users', 'full_name')
