"""Add terms_of_use_accepted column to users table

Revision ID: b15b639288d4
Revises: 1f77ce4346c6
Create Date: 2023-04-13 10:19:38.662996

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b15b639288d4'
down_revision = '1f77ce4346c6'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.add_column(sa.Column('terms_of_use_accepted', sa.Boolean(), nullable=True))
    op.execute('UPDATE users SET terms_of_use_accepted = false;')

    # ### end Alembic commands ###


def downgrade():
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.drop_column('terms_of_use_accepted')
