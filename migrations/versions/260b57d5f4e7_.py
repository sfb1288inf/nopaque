"""Remove transkribus_name column from transkribus_htr_models table

Revision ID: 260b57d5f4e7
Revises: 2c4e27331ccb
Create Date: 2022-10-04 13:26:47.186931

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '260b57d5f4e7'
down_revision = '2c4e27331ccb'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('transkribus_htr_models', 'transkribus_name')


def downgrade():
    op.add_column(
        'transkribus_htr_models',
        sa.Column('transkribus_name', sa.String(length=64), autoincrement=False, nullable=True)
    )
