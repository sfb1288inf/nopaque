"""Rename pipeline model tables

Revision ID: 63b2cc26a01f
Revises: 260b57d5f4e7
Create Date: 2022-10-11 14:32:13.227364

"""
from alembic import op
import os
from app.models import User

# revision identifiers, used by Alembic.
revision = '63b2cc26a01f'
down_revision = '260b57d5f4e7'
branch_labels = None
depends_on = None


def upgrade():
    # TODO: Add error handling for sqlalchemy.exc.ProgrammingError
    for user in User.query.all():
        old_tesseract_ocr_pipeline_model_path = os.path.join(user.path, 'tesseract_ocr_models')
        new_tesseract_ocr_pipeline_model_path = os.path.join(user.path, 'tesseract_ocr_pipeline_models')
        os.rename(old_tesseract_ocr_pipeline_model_path, new_tesseract_ocr_pipeline_model_path)
    op.rename_table('tesseract_ocr_models', 'tesseract_ocr_pipeline_models')
    op.rename_table('transkribus_htr_models', 'transkribus_htr_pipeline_models')


def downgrade():
    for user in User.query.all():
        old_tesseract_ocr_pipeline_model_path = os.path.join(user.path, 'tesseract_ocr_models')
        new_tesseract_ocr_pipeline_model_path = os.path.join(user.path, 'tesseract_ocr_pipeline_models')
        os.rename(old_tesseract_ocr_pipeline_model_path, new_tesseract_ocr_pipeline_model_path)
        os.rename(new_tesseract_ocr_pipeline_model_path, old_tesseract_ocr_pipeline_model_path)
    op.rename_table('tesseract_ocr_pipeline_models', 'tesseract_ocr_models')
    op.rename_table('transkribus_htr_pipeline_models', 'transkribus_htr_models')
