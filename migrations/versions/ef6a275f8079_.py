"""Add avatar table and conncect it to users

Revision ID: ef6a275f8079
Revises: 4820fa2e3ee2
Create Date: 2022-12-01 14:23:22.688572

"""
from alembic import op
import sqlalchemy as sa


revision = 'ef6a275f8079'
down_revision = '4820fa2e3ee2'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('avatars',
    sa.Column('creation_date', sa.DateTime(), nullable=True),
    sa.Column('filename', sa.String(length=255), nullable=True),
    sa.Column('mimetype', sa.String(length=255), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('avatars')
