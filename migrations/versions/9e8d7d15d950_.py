"""Initial database setup

Revision ID: 9e8d7d15d950
Revises: 
Create Date: 2022-04-22 09:38:49.527498

"""
from alembic import op
from flask import current_app
import sqlalchemy as sa
import os
import shutil


# revision identifiers, used by Alembic.
revision = '9e8d7d15d950'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    users_dir = os.path.join(current_app.config['NOPAQUE_DATA_DIR'], 'users')
    if os.path.exists(users_dir):
        if not os.path.isdir(users_dir):
            raise OSError(f'Not a directory: {users_dir}')
        if len(os.listdir(users_dir)) > 0:
            raise OSError(f'Directory not empty: {users_dir}')
    else:
        os.mkdir(users_dir)


    op.create_table(
        'roles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('default', sa.Boolean(), nullable=True),
        sa.Column('name', sa.String(length=64), nullable=True),
        sa.Column('permissions', sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name')
    )
    op.create_index(op.f('ix_roles_default'), 'roles', ['default'], unique=False)

    op.create_table(
        'users',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('role_id', sa.Integer(), nullable=True),
        sa.Column('confirmed', sa.Boolean(), nullable=True),
        sa.Column('email', sa.String(length=254), nullable=True),
        sa.Column('last_seen', sa.DateTime(), nullable=True),
        sa.Column('member_since', sa.DateTime(), nullable=True),
        sa.Column('password_hash', sa.String(length=128), nullable=True),
        sa.Column('token', sa.String(length=32), nullable=True),
        sa.Column('token_expiration', sa.DateTime(), nullable=True),
        sa.Column('username', sa.String(length=64), nullable=True),
        sa.Column('setting_dark_mode', sa.Boolean(), nullable=True),
        sa.Column('setting_job_status_mail_notification_level', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['role_id'], ['roles.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_users_email'), 'users', ['email'], unique=True)
    op.create_index(op.f('ix_users_token'), 'users', ['token'], unique=True)
    op.create_index(op.f('ix_users_username'), 'users', ['username'], unique=True)

    op.create_table(
        'corpora',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('description', sa.String(length=255), nullable=True),
        sa.Column('last_edited_date', sa.DateTime(), nullable=True),
        sa.Column('status', sa.Integer(), nullable=True),
        sa.Column('title', sa.String(length=32), nullable=True),
        sa.Column('num_analysis_sessions', sa.Integer(), nullable=True),
        sa.Column('num_tokens', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'jobs',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('description', sa.String(length=255), nullable=True),
        sa.Column('end_date', sa.DateTime(), nullable=True),
        sa.Column('service', sa.String(length=64), nullable=True),
        sa.Column('service_args', sa.String(length=255), nullable=True),
        sa.Column('service_version', sa.String(length=16), nullable=True),
        sa.Column('status', sa.Integer(), nullable=True),
        sa.Column('title', sa.String(length=32), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'tesseract_ocr_models',
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('filename', sa.String(length=255), nullable=True),
        sa.Column('last_edited_date', sa.DateTime(), nullable=True),
        sa.Column('mimetype', sa.String(length=255), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('compatible_service_versions', sa.String(length=255), nullable=True),
        sa.Column('description', sa.String(length=255), nullable=True),
        sa.Column('publisher', sa.String(length=128), nullable=True),
        sa.Column('publisher_url', sa.String(length=512), nullable=True),
        sa.Column('publishing_url', sa.String(length=512), nullable=True),
        sa.Column('publishing_year', sa.Integer(), nullable=True),
        sa.Column('shared', sa.Boolean(), nullable=True),
        sa.Column('title', sa.String(length=64), nullable=True),
        sa.Column('version', sa.String(length=16), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'transkribus_htr_models',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('shared', sa.Boolean(), nullable=True),
        sa.Column('transkribus_model_id', sa.Integer(), nullable=True),
        sa.Column('transkribus_name', sa.String(length=64), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'corpus_files',
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('filename', sa.String(length=255), nullable=True),
        sa.Column('last_edited_date', sa.DateTime(), nullable=True),
        sa.Column('mimetype', sa.String(length=255), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('corpus_id', sa.Integer(), nullable=True),
        sa.Column('address', sa.String(length=255), nullable=True),
        sa.Column('author', sa.String(length=255), nullable=True),
        sa.Column('booktitle', sa.String(length=255), nullable=True),
        sa.Column('chapter', sa.String(length=255), nullable=True),
        sa.Column('editor', sa.String(length=255), nullable=True),
        sa.Column('institution', sa.String(length=255), nullable=True),
        sa.Column('journal', sa.String(length=255), nullable=True),
        sa.Column('pages', sa.String(length=255), nullable=True),
        sa.Column('publisher', sa.String(length=255), nullable=True),
        sa.Column('publishing_year', sa.Integer(), nullable=True),
        sa.Column('school', sa.String(length=255), nullable=True),
        sa.Column('title', sa.String(length=255), nullable=True),
        sa.ForeignKeyConstraint(['corpus_id'], ['corpora.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'job_inputs',
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('filename', sa.String(length=255), nullable=True),
        sa.Column('last_edited_date', sa.DateTime(), nullable=True),
        sa.Column('mimetype', sa.String(length=255), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('job_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['job_id'], ['jobs.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_table(
        'job_results',
        sa.Column('creation_date', sa.DateTime(), nullable=True),
        sa.Column('filename', sa.String(length=255), nullable=True),
        sa.Column('last_edited_date', sa.DateTime(), nullable=True),
        sa.Column('mimetype', sa.String(length=255), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('job_id', sa.Integer(), nullable=True),
        sa.Column('description', sa.String(length=255), nullable=True),
        sa.ForeignKeyConstraint(['job_id'], ['jobs.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    users_dir = os.path.join(current_app.config['NOPAQUE_DATA_DIR'], 'users')
    if os.path.exists(users_dir):
        shutil.rmtree(users_dir)


    op.drop_table('job_results')
    op.drop_table('job_inputs')
    op.drop_table('corpus_files')
    op.drop_table('transkribus_htr_models')
    op.drop_table('tesseract_ocr_models')
    op.drop_table('jobs')
    op.drop_table('corpora')
    op.drop_index(op.f('ix_users_username'), table_name='users')
    op.drop_index(op.f('ix_users_token'), table_name='users')
    op.drop_index(op.f('ix_users_email'), table_name='users')
    op.drop_table('users')
    op.drop_index(op.f('ix_roles_default'), table_name='roles')
    op.drop_table('roles')
