"""Add is_public column to corpora table

Revision ID: 2c4e27331ccb
Revises: 116b4ab3ef9c
Create Date: 2022-09-28 13:48:26.218643

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2c4e27331ccb'
down_revision = '116b4ab3ef9c'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'corpora',
        sa.Column('is_public', sa.Boolean(), nullable=True)
    )
    op.execute('UPDATE corpora SET is_public = false;')


def downgrade():
    op.drop_column('corpora', 'is_public')
