"""Remove last_edited_date column from all tables

Revision ID: 31b9c0259e6b
Revises: 89e9526089bf
Create Date: 2022-11-24 09:53:21.025531

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '31b9c0259e6b'
down_revision = '89e9526089bf'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('corpora', 'last_edited_date')
    op.drop_column('corpus_files', 'last_edited_date')
    op.drop_column('job_inputs', 'last_edited_date')
    op.drop_column('job_results', 'last_edited_date')
    op.drop_column('spacy_nlp_pipeline_models', 'last_edited_date')
    op.drop_column('tesseract_ocr_pipeline_models', 'last_edited_date')


def downgrade():
    op.add_column(
        'tesseract_ocr_pipeline_models',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
    op.add_column(
        'spacy_nlp_pipeline_models',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
    op.add_column(
        'job_results',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
    op.add_column(
        'job_inputs',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
    op.add_column(
        'corpus_files',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
    op.add_column(
        'corpora',
        sa.Column('last_edited_date', sa.DateTime(), nullable=True)
    )
