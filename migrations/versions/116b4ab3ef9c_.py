"""Add tokens table for API authentication

Revision ID: 116b4ab3ef9c
Revises: f9070ff1fa4a
Create Date: 2022-09-02 11:12:01.995451

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '116b4ab3ef9c'
down_revision = 'f9070ff1fa4a'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'tokens',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('access_token', sa.String(length=64), nullable=True),
        sa.Column('access_expiration', sa.DateTime(), nullable=True),
        sa.Column('refresh_token', sa.String(length=64), nullable=True),
        sa.Column('refresh_expiration', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_tokens_access_token'), 'tokens', ['access_token'], unique=False)
    op.create_index(op.f('ix_tokens_refresh_token'), 'tokens', ['refresh_token'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_tokens_refresh_token'), table_name='tokens')
    op.drop_index(op.f('ix_tokens_access_token'), table_name='tokens')
    op.drop_table('tokens')
