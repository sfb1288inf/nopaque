"""Remove transkribus_htr_pipeline_models table

Revision ID: a3b727e3ff71
Revises: 63b2cc26a01f
Create Date: 2022-10-12 13:08:19.065218

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a3b727e3ff71'
down_revision = '63b2cc26a01f'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table('transkribus_htr_pipeline_models')


def downgrade():
    op.create_table(
        'transkribus_htr_pipeline_models',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('user_id', sa.Integer(), autoincrement=False, nullable=True),
        sa.Column('shared', sa.Boolean(), autoincrement=False, nullable=True),
        sa.Column('transkribus_model_id', sa.Integer(), autoincrement=False, nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], name='transkribus_htr_models_user_id_fkey'),
        sa.PrimaryKeyConstraint('id', name='transkribus_htr_models_pkey')
    )
