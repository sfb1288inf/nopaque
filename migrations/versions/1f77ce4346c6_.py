"""empty message

Revision ID: 1f77ce4346c6
Revises: 03c7211f089d
Create Date: 2023-02-22 12:56:30.176665

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1f77ce4346c6'
down_revision = '03c7211f089d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'corpus_follower_roles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=64), nullable=True),
        sa.Column('default', sa.Boolean(), nullable=True),
        sa.Column('permissions', sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name')
    )
    op.create_index(
        op.f('ix_corpus_follower_roles_default'),
        'corpus_follower_roles',
        ['default'],
        unique=False
    )

    op.add_column(
        'corpus_follower_associations',
        sa.Column('role_id', sa.Integer(), nullable=True)
    )
    op.create_foreign_key(
        'fk_corpus_follower_associations_role_id_corpus_follower_roles',
        'corpus_follower_associations',
        'corpus_follower_roles',
        ['role_id'],
        ['id']
    )
    op.drop_column('corpus_follower_associations', 'permissions')


def downgrade():
    op.add_column(
        'corpus_follower_associations',
        sa.Column('permissions', sa.Integer(), nullable=True)
    )
    op.execute('UPDATE corpus_follower_associations SET permissions = 0;')
    op.alter_column(
        'corpus_follower_associations',
        'permissions',
        nullable=False
    )
    op.drop_constraint(
        'fk_corpus_follower_associations_role_id_corpus_follower_roles',
        'corpus_follower_associations',
        type_='foreignkey'
    )
    op.drop_column('corpus_follower_associations', 'role_id')

    op.drop_index(
        op.f('ix_corpus_follower_roles_default'),
        table_name='corpus_follower_roles'
    )
    op.drop_table('corpus_follower_roles')
