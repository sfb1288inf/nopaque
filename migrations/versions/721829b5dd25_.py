"""Add pipeline_name column to spacy_nlp_pipeline_models table

Revision ID: 721829b5dd25
Revises: 31dd42e5ea6f
Create Date: 2022-11-04 13:58:13.008301

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '721829b5dd25'
down_revision = '31dd42e5ea6f'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'spacy_nlp_pipeline_models',
        sa.Column('pipeline_name', sa.String(length=64), nullable=True)
    )


def downgrade():
    op.drop_column('spacy_nlp_pipeline_models', 'pipeline_name')
