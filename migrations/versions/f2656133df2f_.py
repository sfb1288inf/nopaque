"""Rename shared columns to is_public and add description column to corpus_files table

Revision ID: f2656133df2f
Revises: 31b9c0259e6b
Create Date: 2022-11-29 14:17:16.845501

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2656133df2f'
down_revision = '31b9c0259e6b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'corpus_files',
        sa.Column('description', sa.String(length=255), nullable=True)
    )

    op.alter_column(
        'spacy_nlp_pipeline_models',
        'shared',
        new_column_name='is_public'
    )
    op.alter_column(
        'tesseract_ocr_pipeline_models',
        'shared',
        new_column_name='is_public'
    )


def downgrade():
    op.alter_column('tesseract_ocr_pipeline_models', 'is_public', new_column_name='shared')
    op.alter_column('spacy_nlp_pipeline_models', 'is_public', new_column_name='shared')

    op.drop_column('corpus_files', 'description')
