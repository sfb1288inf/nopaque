"""Remove token and token_expiration columns from users table

Revision ID: f9070ff1fa4a
Revises: 9e8d7d15d950
Create Date: 2022-09-01 13:46:47.425268

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'f9070ff1fa4a'
down_revision = '9e8d7d15d950'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_index('ix_users_token', table_name='users')
    op.drop_column('users', 'token')
    op.drop_column('users', 'token_expiration')


def downgrade():
    op.add_column(
        'users',
        sa.Column('token_expiration', sa.DateTime(), autoincrement=False, nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('token', sa.String(length=32), autoincrement=False, nullable=True)
    )
    op.create_index('ix_users_token', 'users', ['token'], unique=False)
