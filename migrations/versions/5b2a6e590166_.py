"""Add is_public and profile_privacy_settings columns to users table

Revision ID: 5b2a6e590166
Revises: ef6a275f8079
Create Date: 2022-12-12 12:39:09.339847

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5b2a6e590166'
down_revision = 'ef6a275f8079'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'users',
        sa.Column('is_public', sa.Boolean(), nullable=True)
    )
    op.add_column(
        'users',
        sa.Column('profile_privacy_settings', sa.Integer(), nullable=True)
    )
    op.execute('UPDATE users SET is_public = false;')
    op.execute('UPDATE users SET profile_privacy_settings = 0;')


def downgrade():
    op.drop_column('users', 'is_public')
    op.drop_column('users', 'profile_privacy_settings')

