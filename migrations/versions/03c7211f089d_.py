"""empty message

Revision ID: 03c7211f089d
Revises: 5fe6a6c7870c
Create Date: 2023-02-21 09:57:22.005883

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '03c7211f089d'
down_revision = '5fe6a6c7870c'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        'corpus_follower_associations',
        'followed_corpus_id',
        new_column_name='corpus_id'
    )
    op.alter_column(
        'corpus_follower_associations',
        'following_user_id',
        new_column_name='follower_id'
    )


def downgrade():
    op.alter_column(
        'corpus_follower_associations',
        'corpus_id',
        new_column_name='followed_corpus_id'
    )
    op.alter_column(
        'corpus_follower_associations',
        'follower_id',
        new_column_name='following_user_id'
    )
