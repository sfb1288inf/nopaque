"""Remove setting_dark_mode column from users table

Revision ID: 89e9526089bf
Revises: 721829b5dd25
Create Date: 2022-11-17 09:47:27.724692

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '89e9526089bf'
down_revision = '721829b5dd25'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('users', 'setting_dark_mode')


def downgrade():
    op.add_column(
        'users',
        sa.Column('setting_dark_mode', sa.Boolean(), nullable=True)
    )
