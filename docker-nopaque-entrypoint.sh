#!/bin/bash

GREEN_COLOR="\033[0;32m"
RED_COLOR="\033[0;31m"
NO_COLOR="\033[0m"
CHECK_MARK="\xE2\x9C\x93"
CROSS_MARK="\xE2\x9D\x8C"


# Check if necessary environment variables are set
if [[ -z "${HOST_DOCKER_GID}" ]]; then
  echo "Environment variable \"HOST_DOCKER_GID\" not set."
  exit 1
fi

if [[ -z "${HOST_UID}" ]]; then
  echo "Environment variable \"HOST_UID\" not set."
  exit 1
fi

if [[ -z "${HOST_GID}" ]]; then
  echo "Environment variable \"HOST_GID\" not set."
  exit 1
fi


# Check if the UID or GID are set to "0" (root). We want an unprivileged user.
if [[ "${HOST_UID}" == "0" ]]; then
    echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
    echo "\"0\" is not allowed for HOST_UID"
    exit 1
fi

if [[ "${HOST_GID}" == "0" ]]; then
    echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
    echo "\"0\" is not allowed for HOST_GID"
    exit 1
fi


echo "Set container UID and GIDs to match the host system..."
##############################################################################
# Update docker GID                                                          #
##############################################################################
DOCKER_GID=$(getent group docker | cut -d: -f3)
if [[ "${DOCKER_GID}" == "${HOST_DOCKER_GID}" ]]; then
    echo -n "- docker GID is already matching..."
    echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
else
    echo -n "- Updating docker GID (${DOCKER_GID} -> ${HOST_DOCKER_GID})... "
    groupmod --gid "${HOST_DOCKER_GID}" docker > /dev/null
    if [[ "${?}" == "0" ]]; then
        echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
    else
        echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
        exit 1
    fi
fi


##############################################################################
# Update nopaque GID                                                         #
##############################################################################
NOPAQUE_GID=$(id -g nopaque)
if [[ "${NOPAQUE_GID}" == "${HOST_GID}" ]]; then
    echo -n "- nopaque GID is already matching..."
    echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
else
    echo -n "- Updating nopaque GID (${NOPAQUE_GID} -> ${HOST_GID})... "
    groupmod --gid "${HOST_GID}" nopaque > /dev/null
    if [[ "${?}" == "0" ]]; then
        echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
    else
        echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
        exit 1
    fi

    echo -n "- Updating nopaque directory group... "
    chown -R :nopaque /home/nopaque
    if [[ "${?}" == "0" ]]; then
        echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
    else
        echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
        exit 1
    fi
fi


##############################################################################
# Update nopaque UID                                                         #
##############################################################################
NOPAQUE_UID=$(id -u nopaque)
if [[ "${NOPAQUE_UID}" == "${HOST_UID}" ]]; then
    echo -n "- nopaque UID is already matching..."
    echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
else
    echo -n "- Updating nopaque UID (${NOPAQUE_UID} -> ${HOST_UID})... "
    usermod --uid "${HOST_UID}" nopaque > /dev/null
    if [[ "${?}" == "0" ]]; then
        echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
    else
        echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
        exit 1
    fi

    echo -n "- Updating nopaque directory owner... "
    chown -R nopaque /home/nopaque
    if [[ "${?}" == "0" ]]; then
        echo -e "${GREEN_COLOR}${CHECK_MARK}${NO_COLOR}"
    else
        echo -e "${RED_COLOR}${CROSS_MARK}${NO_COLOR}"
        exit 1
    fi
fi


exec gosu nopaque ./boot.sh ${@}
