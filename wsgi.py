# First things first: apply monkey patch, so that no code gets executed without
# patched libraries!
import eventlet
eventlet.monkey_patch()


from typing import Any  # noqa
from app import create_app, db, scheduler, socketio  # noqa
from app import models  # noqa


app = create_app()


@app.shell_context_processor
def make_shell_context() -> dict[str, Any]:
    ''' Adds variables to the shell context. '''
    return {
        'db': db,
        'Avatar': models.Avatar,
        'Corpus': models.Corpus,
        'CorpusFile': models.CorpusFile,
        'CorpusFollowerAssociation': models.CorpusFollowerAssociation,
        'CorpusFollowerRole': models.CorpusFollowerRole,
        'Job': models.Job,
        'JobInput': models.JobInput,
        'JobResult': models.JobResult,
        'Role': models.Role,
        'TesseractOCRPipelineModel': models.TesseractOCRPipelineModel,
        'SpaCyNLPPipelineModel': models.SpaCyNLPPipelineModel,
        'User': models.User
    }


def main():
    with app.app_context():
        scheduler.start()
    socketio.run(app, host='0.0.0.0')


if __name__ == '__main__':
    main()
